(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("DPlayer", [], factory);
	else if(typeof exports === 'object')
		exports["DPlayer"] = factory();
	else
		root["DPlayer"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/art-template/lib/compile/runtime.js":
/*!**********************************************************!*\
  !*** ./node_modules/art-template/lib/compile/runtime.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

/*! art-template@runtime | https://github.com/aui/art-template */

var detectNode = __webpack_require__(/*! detect-node */ "./node_modules/detect-node/index.js");
var runtime = Object.create(detectNode ? global : window);
var ESCAPE_REG = /["&'<>]/;

/**
 * 编码模板输出的内容
 * @param  {any}        content
 * @return {string}
 */
runtime.$escape = function (content) {
    return xmlEscape(toString(content));
};

/**
 * 迭代器，支持数组与对象
 * @param {array|Object} data 
 * @param {function}     callback 
 */
runtime.$each = function (data, callback) {
    if (Array.isArray(data)) {
        for (var i = 0, len = data.length; i < len; i++) {
            callback(data[i], i);
        }
    } else {
        for (var _i in data) {
            callback(data[_i], _i);
        }
    }
};

// 将目标转成字符
function toString(value) {
    if (typeof value !== 'string') {
        if (value === undefined || value === null) {
            value = '';
        } else if (typeof value === 'function') {
            value = toString(value.call(value));
        } else {
            value = JSON.stringify(value);
        }
    }

    return value;
};

// 编码 HTML 内容
function xmlEscape(content) {
    var html = '' + content;
    var regexResult = ESCAPE_REG.exec(html);
    if (!regexResult) {
        return content;
    }

    var result = '';
    var i = void 0,
        lastIndex = void 0,
        char = void 0;
    for (i = regexResult.index, lastIndex = 0; i < html.length; i++) {

        switch (html.charCodeAt(i)) {
            case 34:
                char = '&#34;';
                break;
            case 38:
                char = '&#38;';
                break;
            case 39:
                char = '&#39;';
                break;
            case 60:
                char = '&#60;';
                break;
            case 62:
                char = '&#62;';
                break;
            default:
                continue;
        }

        if (lastIndex !== i) {
            result += html.substring(lastIndex, i);
        }

        lastIndex = i + 1;
        result += char;
    }

    if (lastIndex !== i) {
        return result + html.substring(lastIndex, i);
    } else {
        return result;
    }
};

module.exports = runtime;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/art-template/lib/runtime.js":
/*!**************************************************!*\
  !*** ./node_modules/art-template/lib/runtime.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(/*! ./compile/runtime */ "./node_modules/art-template/lib/compile/runtime.js");

/***/ }),

/***/ "./node_modules/axios/index.js":
/*!*************************************!*\
  !*** ./node_modules/axios/index.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(/*! ./lib/axios */ "./node_modules/axios/lib/axios.js");

/***/ }),

/***/ "./node_modules/axios/lib/adapters/xhr.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/adapters/xhr.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var settle = __webpack_require__(/*! ./../core/settle */ "./node_modules/axios/lib/core/settle.js");
var buildURL = __webpack_require__(/*! ./../helpers/buildURL */ "./node_modules/axios/lib/helpers/buildURL.js");
var parseHeaders = __webpack_require__(/*! ./../helpers/parseHeaders */ "./node_modules/axios/lib/helpers/parseHeaders.js");
var isURLSameOrigin = __webpack_require__(/*! ./../helpers/isURLSameOrigin */ "./node_modules/axios/lib/helpers/isURLSameOrigin.js");
var createError = __webpack_require__(/*! ../core/createError */ "./node_modules/axios/lib/core/createError.js");
var btoa = typeof window !== 'undefined' && window.btoa && window.btoa.bind(window) || __webpack_require__(/*! ./../helpers/btoa */ "./node_modules/axios/lib/helpers/btoa.js");

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();
    var loadEvent = 'onreadystatechange';
    var xDomain = false;

    // For IE 8/9 CORS support
    // Only supports POST and GET calls and doesn't returns the response headers.
    // DON'T do this for testing b/c XMLHttpRequest is mocked, not XDomainRequest.
    if ("development" !== 'test' && typeof window !== 'undefined' && window.XDomainRequest && !('withCredentials' in request) && !isURLSameOrigin(config.url)) {
      request = new window.XDomainRequest();
      loadEvent = 'onload';
      xDomain = true;
      request.onprogress = function handleProgress() {};
      request.ontimeout = function handleTimeout() {};
    }

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    // Listen for ready state
    request[loadEvent] = function handleLoad() {
      if (!request || request.readyState !== 4 && !xDomain) {
        return;
      }

      // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request
      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      }

      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        // IE sends 1223 instead of 204 (https://github.com/axios/axios/issues/201)
        status: request.status === 1223 ? 204 : request.status,
        statusText: request.status === 1223 ? 'No Content' : request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED', request));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__(/*! ./../helpers/cookies */ "./node_modules/axios/lib/helpers/cookies.js");

      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ? cookies.read(config.xsrfCookieName) : undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (config.withCredentials) {
      request.withCredentials = true;
    }

    // Add responseType to request if needed
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
        if (config.responseType !== 'json') {
          throw e;
        }
      }
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};

/***/ }),

/***/ "./node_modules/axios/lib/axios.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/axios.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");
var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");
var Axios = __webpack_require__(/*! ./core/Axios */ "./node_modules/axios/lib/core/Axios.js");
var defaults = __webpack_require__(/*! ./defaults */ "./node_modules/axios/lib/defaults.js");

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(utils.merge(defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = __webpack_require__(/*! ./cancel/Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");
axios.CancelToken = __webpack_require__(/*! ./cancel/CancelToken */ "./node_modules/axios/lib/cancel/CancelToken.js");
axios.isCancel = __webpack_require__(/*! ./cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js");

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = __webpack_require__(/*! ./helpers/spread */ "./node_modules/axios/lib/helpers/spread.js");

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports.default = axios;

/***/ }),

/***/ "./node_modules/axios/lib/cancel/Cancel.js":
/*!*************************************************!*\
  !*** ./node_modules/axios/lib/cancel/Cancel.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */

function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;

/***/ }),

/***/ "./node_modules/axios/lib/cancel/CancelToken.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/cancel/CancelToken.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__(/*! ./Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;

/***/ }),

/***/ "./node_modules/axios/lib/cancel/isCancel.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/cancel/isCancel.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};

/***/ }),

/***/ "./node_modules/axios/lib/core/Axios.js":
/*!**********************************************!*\
  !*** ./node_modules/axios/lib/core/Axios.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var defaults = __webpack_require__(/*! ./../defaults */ "./node_modules/axios/lib/defaults.js");
var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var InterceptorManager = __webpack_require__(/*! ./InterceptorManager */ "./node_modules/axios/lib/core/InterceptorManager.js");
var dispatchRequest = __webpack_require__(/*! ./dispatchRequest */ "./node_modules/axios/lib/core/dispatchRequest.js");

/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = utils.merge({
      url: arguments[0]
    }, arguments[1]);
  }

  config = utils.merge(defaults, { method: 'get' }, this.defaults, config);
  config.method = config.method.toLowerCase();

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function (url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function (url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;

/***/ }),

/***/ "./node_modules/axios/lib/core/InterceptorManager.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/core/InterceptorManager.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;

/***/ }),

/***/ "./node_modules/axios/lib/core/createError.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/core/createError.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__(/*! ./enhanceError */ "./node_modules/axios/lib/core/enhanceError.js");

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};

/***/ }),

/***/ "./node_modules/axios/lib/core/dispatchRequest.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/core/dispatchRequest.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var transformData = __webpack_require__(/*! ./transformData */ "./node_modules/axios/lib/core/transformData.js");
var isCancel = __webpack_require__(/*! ../cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js");
var defaults = __webpack_require__(/*! ../defaults */ "./node_modules/axios/lib/defaults.js");
var isAbsoluteURL = __webpack_require__(/*! ./../helpers/isAbsoluteURL */ "./node_modules/axios/lib/helpers/isAbsoluteURL.js");
var combineURLs = __webpack_require__(/*! ./../helpers/combineURLs */ "./node_modules/axios/lib/helpers/combineURLs.js");

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Support baseURL config
  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  }

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData(config.data, config.headers, config.transformRequest);

  // Flatten headers
  config.headers = utils.merge(config.headers.common || {}, config.headers[config.method] || {}, config.headers || {});

  utils.forEach(['delete', 'get', 'head', 'post', 'put', 'patch', 'common'], function cleanHeaderConfig(method) {
    delete config.headers[method];
  });

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData(response.data, response.headers, config.transformResponse);

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData(reason.response.data, reason.response.headers, config.transformResponse);
      }
    }

    return Promise.reject(reason);
  });
};

/***/ }),

/***/ "./node_modules/axios/lib/core/enhanceError.js":
/*!*****************************************************!*\
  !*** ./node_modules/axios/lib/core/enhanceError.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */

module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }
  error.request = request;
  error.response = response;
  return error;
};

/***/ }),

/***/ "./node_modules/axios/lib/core/settle.js":
/*!***********************************************!*\
  !*** ./node_modules/axios/lib/core/settle.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__(/*! ./createError */ "./node_modules/axios/lib/core/createError.js");

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  // Note: status is not exposed by XDomainRequest
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError('Request failed with status code ' + response.status, response.config, null, response.request, response));
  }
};

/***/ }),

/***/ "./node_modules/axios/lib/core/transformData.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/core/transformData.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};

/***/ }),

/***/ "./node_modules/axios/lib/defaults.js":
/*!********************************************!*\
  !*** ./node_modules/axios/lib/defaults.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");
var normalizeHeaderName = __webpack_require__(/*! ./helpers/normalizeHeaderName */ "./node_modules/axios/lib/helpers/normalizeHeaderName.js");

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__(/*! ./adapters/xhr */ "./node_modules/axios/lib/adapters/xhr.js");
  } else if (typeof process !== 'undefined') {
    // For node use HTTP adapter
    adapter = __webpack_require__(/*! ./adapters/http */ "./node_modules/axios/lib/adapters/xhr.js");
  }
  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) || utils.isArrayBuffer(data) || utils.isBuffer(data) || utils.isStream(data) || utils.isFile(data) || utils.isBlob(data)) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (e) {/* Ignore */}
    }
    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/axios/lib/helpers/bind.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/helpers/bind.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/btoa.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/helpers/btoa.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// btoa polyfill for IE<10 courtesy https://github.com/davidchambers/Base64.js

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

function E() {
  this.message = 'String contains an invalid character';
}
E.prototype = new Error();
E.prototype.code = 5;
E.prototype.name = 'InvalidCharacterError';

function btoa(input) {
  var str = String(input);
  var output = '';
  for (
  // initialize result and counter
  var block, charCode, idx = 0, map = chars;
  // if the next str index does not exist:
  //   change the mapping table to "="
  //   check if d has no fractional digits
  str.charAt(idx | 0) || (map = '=', idx % 1);
  // "8 - idx % 1 * 8" generates the sequence 2, 4, 6, 8
  output += map.charAt(63 & block >> 8 - idx % 1 * 8)) {
    charCode = str.charCodeAt(idx += 3 / 4);
    if (charCode > 0xFF) {
      throw new E();
    }
    block = block << 8 | charCode;
  }
  return output;
}

module.exports = btoa;

/***/ }),

/***/ "./node_modules/axios/lib/helpers/buildURL.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/helpers/buildURL.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function encode(val) {
  return encodeURIComponent(val).replace(/%40/gi, '@').replace(/%3A/gi, ':').replace(/%24/g, '$').replace(/%2C/gi, ',').replace(/%20/g, '+').replace(/%5B/gi, '[').replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/combineURLs.js":
/*!*******************************************************!*\
  !*** ./node_modules/axios/lib/helpers/combineURLs.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */

module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '') : baseURL;
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/cookies.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/helpers/cookies.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = utils.isStandardBrowserEnv() ?

// Standard browser envs support document.cookie
function standardBrowserEnv() {
  return {
    write: function write(name, value, expires, path, domain, secure) {
      var cookie = [];
      cookie.push(name + '=' + encodeURIComponent(value));

      if (utils.isNumber(expires)) {
        cookie.push('expires=' + new Date(expires).toGMTString());
      }

      if (utils.isString(path)) {
        cookie.push('path=' + path);
      }

      if (utils.isString(domain)) {
        cookie.push('domain=' + domain);
      }

      if (secure === true) {
        cookie.push('secure');
      }

      document.cookie = cookie.join('; ');
    },

    read: function read(name) {
      var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
      return match ? decodeURIComponent(match[3]) : null;
    },

    remove: function remove(name) {
      this.write(name, '', Date.now() - 86400000);
    }
  };
}() :

// Non standard browser env (web workers, react-native) lack needed support.
function nonStandardBrowserEnv() {
  return {
    write: function write() {},
    read: function read() {
      return null;
    },
    remove: function remove() {}
  };
}();

/***/ }),

/***/ "./node_modules/axios/lib/helpers/isAbsoluteURL.js":
/*!*********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isAbsoluteURL.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */

module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return (/^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url)
  );
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/isURLSameOrigin.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isURLSameOrigin.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = utils.isStandardBrowserEnv() ?

// Standard browser envs have full support of the APIs needed to test
// whether the request URL is of the same origin as current location.
function standardBrowserEnv() {
  var msie = /(msie|trident)/i.test(navigator.userAgent);
  var urlParsingNode = document.createElement('a');
  var originURL;

  /**
  * Parse a URL to discover it's components
  *
  * @param {String} url The URL to be parsed
  * @returns {Object}
  */
  function resolveURL(url) {
    var href = url;

    if (msie) {
      // IE needs attribute set twice to normalize properties
      urlParsingNode.setAttribute('href', href);
      href = urlParsingNode.href;
    }

    urlParsingNode.setAttribute('href', href);

    // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
    return {
      href: urlParsingNode.href,
      protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
      host: urlParsingNode.host,
      search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
      hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
      hostname: urlParsingNode.hostname,
      port: urlParsingNode.port,
      pathname: urlParsingNode.pathname.charAt(0) === '/' ? urlParsingNode.pathname : '/' + urlParsingNode.pathname
    };
  }

  originURL = resolveURL(window.location.href);

  /**
  * Determine if a URL shares the same origin as the current location
  *
  * @param {String} requestURL The URL to test
  * @returns {boolean} True if URL shares the same origin, otherwise false
  */
  return function isURLSameOrigin(requestURL) {
    var parsed = utils.isString(requestURL) ? resolveURL(requestURL) : requestURL;
    return parsed.protocol === originURL.protocol && parsed.host === originURL.host;
  };
}() :

// Non standard browser envs (web workers, react-native) lack needed support.
function nonStandardBrowserEnv() {
  return function isURLSameOrigin() {
    return true;
  };
}();

/***/ }),

/***/ "./node_modules/axios/lib/helpers/normalizeHeaderName.js":
/*!***************************************************************!*\
  !*** ./node_modules/axios/lib/helpers/normalizeHeaderName.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/axios/lib/utils.js");

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/parseHeaders.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/parseHeaders.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

// Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers
var ignoreDuplicateOf = ['age', 'authorization', 'content-length', 'content-type', 'etag', 'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since', 'last-modified', 'location', 'max-forwards', 'proxy-authorization', 'referer', 'retry-after', 'user-agent'];

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) {
    return parsed;
  }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }
      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });

  return parsed;
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/spread.js":
/*!**************************************************!*\
  !*** ./node_modules/axios/lib/helpers/spread.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */

module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};

/***/ }),

/***/ "./node_modules/axios/lib/utils.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/utils.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");
var isBuffer = __webpack_require__(/*! is-buffer */ "./node_modules/is-buffer/index.js");

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return typeof FormData !== 'undefined' && val instanceof FormData;
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if (typeof ArrayBuffer !== 'undefined' && ArrayBuffer.isView) {
    result = ArrayBuffer.isView(val);
  } else {
    result = val && val.buffer && val.buffer instanceof ArrayBuffer;
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && (typeof val === 'undefined' ? 'undefined' : _typeof(val)) === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 */
function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    return false;
  }
  return typeof window !== 'undefined' && typeof document !== 'undefined';
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge() /* obj1, obj2, obj3, ... */{
  var result = {};
  function assignValue(val, key) {
    if (_typeof(result[key]) === 'object' && (typeof val === 'undefined' ? 'undefined' : _typeof(val)) === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  extend: extend,
  trim: trim
};

/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--6-1!./node_modules/postcss-loader/lib/index.js??ref--6-2!./node_modules/balloon-css/balloon.css":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/postcss-loader/lib??ref--6-2!./node_modules/balloon-css/balloon.css ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "button[data-balloon] {\n  overflow: visible; }\n\n[data-balloon] {\n  position: relative;\n  cursor: pointer; }\n  [data-balloon]:after {\n    filter: alpha(opactiy=0);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)\";\n    -moz-opacity: 0;\n    -khtml-opacity: 0;\n    opacity: 0;\n    pointer-events: none;\n    transition: all 0.18s ease-out 0.18s;\n    font-family: sans-serif !important;\n    font-weight: normal !important;\n    font-style: normal !important;\n    text-shadow: none !important;\n    font-size: 12px !important;\n    background: rgba(17, 17, 17, 0.9);\n    border-radius: 4px;\n    color: #fff;\n    content: attr(data-balloon);\n    padding: .5em 1em;\n    position: absolute;\n    white-space: nowrap;\n    z-index: 10; }\n  [data-balloon]:before {\n    background: no-repeat url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http://www.w3.org/2000/svg%22%20width%3D%2236px%22%20height%3D%2212px%22%3E%3Cpath%20fill%3D%22rgba(17, 17, 17, 0.9)%22%20transform%3D%22rotate(0)%22%20d%3D%22M2.658,0.000%20C-13.615,0.000%2050.938,0.000%2034.662,0.000%20C28.662,0.000%2023.035,12.002%2018.660,12.002%20C14.285,12.002%208.594,0.000%202.658,0.000%20Z%22/%3E%3C/svg%3E\");\n    background-size: 100% auto;\n    width: 18px;\n    height: 6px;\n    filter: alpha(opactiy=0);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)\";\n    -moz-opacity: 0;\n    -khtml-opacity: 0;\n    opacity: 0;\n    pointer-events: none;\n    transition: all 0.18s ease-out 0.18s;\n    content: '';\n    position: absolute;\n    z-index: 10; }\n  [data-balloon]:hover:before, [data-balloon]:hover:after, [data-balloon][data-balloon-visible]:before, [data-balloon][data-balloon-visible]:after {\n    filter: alpha(opactiy=100);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)\";\n    -moz-opacity: 1;\n    -khtml-opacity: 1;\n    opacity: 1;\n    pointer-events: auto; }\n  [data-balloon].font-awesome:after {\n    font-family: FontAwesome; }\n  [data-balloon][data-balloon-break]:after {\n    white-space: pre; }\n  [data-balloon][data-balloon-blunt]:before, [data-balloon][data-balloon-blunt]:after {\n    transition: none; }\n  [data-balloon][data-balloon-pos=\"up\"]:after {\n    bottom: 100%;\n    left: 50%;\n    margin-bottom: 11px;\n    -webkit-transform: translate(-50%, 10px);\n    transform: translate(-50%, 10px);\n    -webkit-transform-origin: top;\n    transform-origin: top; }\n  [data-balloon][data-balloon-pos=\"up\"]:before {\n    bottom: 100%;\n    left: 50%;\n    margin-bottom: 5px;\n    -webkit-transform: translate(-50%, 10px);\n    transform: translate(-50%, 10px);\n    -webkit-transform-origin: top;\n    transform-origin: top; }\n  [data-balloon][data-balloon-pos=\"up\"]:hover:after, [data-balloon][data-balloon-pos=\"up\"][data-balloon-visible]:after {\n    -webkit-transform: translate(-50%, 0);\n    transform: translate(-50%, 0); }\n  [data-balloon][data-balloon-pos=\"up\"]:hover:before, [data-balloon][data-balloon-pos=\"up\"][data-balloon-visible]:before {\n    -webkit-transform: translate(-50%, 0);\n    transform: translate(-50%, 0); }\n  [data-balloon][data-balloon-pos=\"up-left\"]:after {\n    bottom: 100%;\n    left: 0;\n    margin-bottom: 11px;\n    -webkit-transform: translate(0, 10px);\n    transform: translate(0, 10px);\n    -webkit-transform-origin: top;\n    transform-origin: top; }\n  [data-balloon][data-balloon-pos=\"up-left\"]:before {\n    bottom: 100%;\n    left: 5px;\n    margin-bottom: 5px;\n    -webkit-transform: translate(0, 10px);\n    transform: translate(0, 10px);\n    -webkit-transform-origin: top;\n    transform-origin: top; }\n  [data-balloon][data-balloon-pos=\"up-left\"]:hover:after, [data-balloon][data-balloon-pos=\"up-left\"][data-balloon-visible]:after {\n    -webkit-transform: translate(0, 0);\n    transform: translate(0, 0); }\n  [data-balloon][data-balloon-pos=\"up-left\"]:hover:before, [data-balloon][data-balloon-pos=\"up-left\"][data-balloon-visible]:before {\n    -webkit-transform: translate(0, 0);\n    transform: translate(0, 0); }\n  [data-balloon][data-balloon-pos=\"up-right\"]:after {\n    bottom: 100%;\n    right: 0;\n    margin-bottom: 11px;\n    -webkit-transform: translate(0, 10px);\n    transform: translate(0, 10px);\n    -webkit-transform-origin: top;\n    transform-origin: top; }\n  [data-balloon][data-balloon-pos=\"up-right\"]:before {\n    bottom: 100%;\n    right: 5px;\n    margin-bottom: 5px;\n    -webkit-transform: translate(0, 10px);\n    transform: translate(0, 10px);\n    -webkit-transform-origin: top;\n    transform-origin: top; }\n  [data-balloon][data-balloon-pos=\"up-right\"]:hover:after, [data-balloon][data-balloon-pos=\"up-right\"][data-balloon-visible]:after {\n    -webkit-transform: translate(0, 0);\n    transform: translate(0, 0); }\n  [data-balloon][data-balloon-pos=\"up-right\"]:hover:before, [data-balloon][data-balloon-pos=\"up-right\"][data-balloon-visible]:before {\n    -webkit-transform: translate(0, 0);\n    transform: translate(0, 0); }\n  [data-balloon][data-balloon-pos='down']:after {\n    left: 50%;\n    margin-top: 11px;\n    top: 100%;\n    -webkit-transform: translate(-50%, -10px);\n    transform: translate(-50%, -10px); }\n  [data-balloon][data-balloon-pos='down']:before {\n    background: no-repeat url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http://www.w3.org/2000/svg%22%20width%3D%2236px%22%20height%3D%2212px%22%3E%3Cpath%20fill%3D%22rgba(17, 17, 17, 0.9)%22%20transform%3D%22rotate(180 18 6)%22%20d%3D%22M2.658,0.000%20C-13.615,0.000%2050.938,0.000%2034.662,0.000%20C28.662,0.000%2023.035,12.002%2018.660,12.002%20C14.285,12.002%208.594,0.000%202.658,0.000%20Z%22/%3E%3C/svg%3E\");\n    background-size: 100% auto;\n    width: 18px;\n    height: 6px;\n    left: 50%;\n    margin-top: 5px;\n    top: 100%;\n    -webkit-transform: translate(-50%, -10px);\n    transform: translate(-50%, -10px); }\n  [data-balloon][data-balloon-pos='down']:hover:after, [data-balloon][data-balloon-pos='down'][data-balloon-visible]:after {\n    -webkit-transform: translate(-50%, 0);\n    transform: translate(-50%, 0); }\n  [data-balloon][data-balloon-pos='down']:hover:before, [data-balloon][data-balloon-pos='down'][data-balloon-visible]:before {\n    -webkit-transform: translate(-50%, 0);\n    transform: translate(-50%, 0); }\n  [data-balloon][data-balloon-pos='down-left']:after {\n    left: 0;\n    margin-top: 11px;\n    top: 100%;\n    -webkit-transform: translate(0, -10px);\n    transform: translate(0, -10px); }\n  [data-balloon][data-balloon-pos='down-left']:before {\n    background: no-repeat url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http://www.w3.org/2000/svg%22%20width%3D%2236px%22%20height%3D%2212px%22%3E%3Cpath%20fill%3D%22rgba(17, 17, 17, 0.9)%22%20transform%3D%22rotate(180 18 6)%22%20d%3D%22M2.658,0.000%20C-13.615,0.000%2050.938,0.000%2034.662,0.000%20C28.662,0.000%2023.035,12.002%2018.660,12.002%20C14.285,12.002%208.594,0.000%202.658,0.000%20Z%22/%3E%3C/svg%3E\");\n    background-size: 100% auto;\n    width: 18px;\n    height: 6px;\n    left: 5px;\n    margin-top: 5px;\n    top: 100%;\n    -webkit-transform: translate(0, -10px);\n    transform: translate(0, -10px); }\n  [data-balloon][data-balloon-pos='down-left']:hover:after, [data-balloon][data-balloon-pos='down-left'][data-balloon-visible]:after {\n    -webkit-transform: translate(0, 0);\n    transform: translate(0, 0); }\n  [data-balloon][data-balloon-pos='down-left']:hover:before, [data-balloon][data-balloon-pos='down-left'][data-balloon-visible]:before {\n    -webkit-transform: translate(0, 0);\n    transform: translate(0, 0); }\n  [data-balloon][data-balloon-pos='down-right']:after {\n    right: 0;\n    margin-top: 11px;\n    top: 100%;\n    -webkit-transform: translate(0, -10px);\n    transform: translate(0, -10px); }\n  [data-balloon][data-balloon-pos='down-right']:before {\n    background: no-repeat url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http://www.w3.org/2000/svg%22%20width%3D%2236px%22%20height%3D%2212px%22%3E%3Cpath%20fill%3D%22rgba(17, 17, 17, 0.9)%22%20transform%3D%22rotate(180 18 6)%22%20d%3D%22M2.658,0.000%20C-13.615,0.000%2050.938,0.000%2034.662,0.000%20C28.662,0.000%2023.035,12.002%2018.660,12.002%20C14.285,12.002%208.594,0.000%202.658,0.000%20Z%22/%3E%3C/svg%3E\");\n    background-size: 100% auto;\n    width: 18px;\n    height: 6px;\n    right: 5px;\n    margin-top: 5px;\n    top: 100%;\n    -webkit-transform: translate(0, -10px);\n    transform: translate(0, -10px); }\n  [data-balloon][data-balloon-pos='down-right']:hover:after, [data-balloon][data-balloon-pos='down-right'][data-balloon-visible]:after {\n    -webkit-transform: translate(0, 0);\n    transform: translate(0, 0); }\n  [data-balloon][data-balloon-pos='down-right']:hover:before, [data-balloon][data-balloon-pos='down-right'][data-balloon-visible]:before {\n    -webkit-transform: translate(0, 0);\n    transform: translate(0, 0); }\n  [data-balloon][data-balloon-pos='left']:after {\n    margin-right: 11px;\n    right: 100%;\n    top: 50%;\n    -webkit-transform: translate(10px, -50%);\n    transform: translate(10px, -50%); }\n  [data-balloon][data-balloon-pos='left']:before {\n    background: no-repeat url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http://www.w3.org/2000/svg%22%20width%3D%2212px%22%20height%3D%2236px%22%3E%3Cpath%20fill%3D%22rgba(17, 17, 17, 0.9)%22%20transform%3D%22rotate(-90 18 18)%22%20d%3D%22M2.658,0.000%20C-13.615,0.000%2050.938,0.000%2034.662,0.000%20C28.662,0.000%2023.035,12.002%2018.660,12.002%20C14.285,12.002%208.594,0.000%202.658,0.000%20Z%22/%3E%3C/svg%3E\");\n    background-size: 100% auto;\n    width: 6px;\n    height: 18px;\n    margin-right: 5px;\n    right: 100%;\n    top: 50%;\n    -webkit-transform: translate(10px, -50%);\n    transform: translate(10px, -50%); }\n  [data-balloon][data-balloon-pos='left']:hover:after, [data-balloon][data-balloon-pos='left'][data-balloon-visible]:after {\n    -webkit-transform: translate(0, -50%);\n    transform: translate(0, -50%); }\n  [data-balloon][data-balloon-pos='left']:hover:before, [data-balloon][data-balloon-pos='left'][data-balloon-visible]:before {\n    -webkit-transform: translate(0, -50%);\n    transform: translate(0, -50%); }\n  [data-balloon][data-balloon-pos='right']:after {\n    left: 100%;\n    margin-left: 11px;\n    top: 50%;\n    -webkit-transform: translate(-10px, -50%);\n    transform: translate(-10px, -50%); }\n  [data-balloon][data-balloon-pos='right']:before {\n    background: no-repeat url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http://www.w3.org/2000/svg%22%20width%3D%2212px%22%20height%3D%2236px%22%3E%3Cpath%20fill%3D%22rgba(17, 17, 17, 0.9)%22%20transform%3D%22rotate(90 6 6)%22%20d%3D%22M2.658,0.000%20C-13.615,0.000%2050.938,0.000%2034.662,0.000%20C28.662,0.000%2023.035,12.002%2018.660,12.002%20C14.285,12.002%208.594,0.000%202.658,0.000%20Z%22/%3E%3C/svg%3E\");\n    background-size: 100% auto;\n    width: 6px;\n    height: 18px;\n    left: 100%;\n    margin-left: 5px;\n    top: 50%;\n    -webkit-transform: translate(-10px, -50%);\n    transform: translate(-10px, -50%); }\n  [data-balloon][data-balloon-pos='right']:hover:after, [data-balloon][data-balloon-pos='right'][data-balloon-visible]:after {\n    -webkit-transform: translate(0, -50%);\n    transform: translate(0, -50%); }\n  [data-balloon][data-balloon-pos='right']:hover:before, [data-balloon][data-balloon-pos='right'][data-balloon-visible]:before {\n    -webkit-transform: translate(0, -50%);\n    transform: translate(0, -50%); }\n  [data-balloon][data-balloon-length='small']:after {\n    white-space: normal;\n    width: 80px; }\n  [data-balloon][data-balloon-length='medium']:after {\n    white-space: normal;\n    width: 150px; }\n  [data-balloon][data-balloon-length='large']:after {\n    white-space: normal;\n    width: 260px; }\n  [data-balloon][data-balloon-length='xlarge']:after {\n    white-space: normal;\n    width: 380px; }\n    @media screen and (max-width: 768px) {\n      [data-balloon][data-balloon-length='xlarge']:after {\n        white-space: normal;\n        width: 90vw; } }\n  [data-balloon][data-balloon-length='fit']:after {\n    white-space: normal;\n    width: 100%; }\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js??ref--6-1!./node_modules/postcss-loader/lib/index.js??ref--6-2!./node_modules/sass-loader/lib/loader.js!./src/css/index.scss":
/*!*****************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/postcss-loader/lib??ref--6-2!./node_modules/sass-loader/lib/loader.js!./src/css/index.scss ***!
  \*****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__(/*! -!../../node_modules/css-loader??ref--6-1!../../node_modules/postcss-loader/lib??ref--6-2!../../node_modules/balloon-css/balloon.css */ "./node_modules/css-loader/index.js??ref--6-1!./node_modules/postcss-loader/lib/index.js??ref--6-2!./node_modules/balloon-css/balloon.css"), "");

// module
exports.push([module.i, "@-webkit-keyframes my-face {\n  2% {\n    -webkit-transform: translate(0, 1.5px) rotate(1.5deg);\n            transform: translate(0, 1.5px) rotate(1.5deg); }\n  4% {\n    -webkit-transform: translate(0, -1.5px) rotate(-0.5deg);\n            transform: translate(0, -1.5px) rotate(-0.5deg); }\n  6% {\n    -webkit-transform: translate(0, 1.5px) rotate(-1.5deg);\n            transform: translate(0, 1.5px) rotate(-1.5deg); }\n  8% {\n    -webkit-transform: translate(0, -1.5px) rotate(-1.5deg);\n            transform: translate(0, -1.5px) rotate(-1.5deg); }\n  10% {\n    -webkit-transform: translate(0, 2.5px) rotate(1.5deg);\n            transform: translate(0, 2.5px) rotate(1.5deg); }\n  12% {\n    -webkit-transform: translate(0, -0.5px) rotate(1.5deg);\n            transform: translate(0, -0.5px) rotate(1.5deg); }\n  14% {\n    -webkit-transform: translate(0, -1.5px) rotate(1.5deg);\n            transform: translate(0, -1.5px) rotate(1.5deg); }\n  16% {\n    -webkit-transform: translate(0, -0.5px) rotate(-1.5deg);\n            transform: translate(0, -0.5px) rotate(-1.5deg); }\n  18% {\n    -webkit-transform: translate(0, 0.5px) rotate(-1.5deg);\n            transform: translate(0, 0.5px) rotate(-1.5deg); }\n  20% {\n    -webkit-transform: translate(0, -1.5px) rotate(2.5deg);\n            transform: translate(0, -1.5px) rotate(2.5deg); }\n  22% {\n    -webkit-transform: translate(0, 0.5px) rotate(-1.5deg);\n            transform: translate(0, 0.5px) rotate(-1.5deg); }\n  24% {\n    -webkit-transform: translate(0, 1.5px) rotate(1.5deg);\n            transform: translate(0, 1.5px) rotate(1.5deg); }\n  26% {\n    -webkit-transform: translate(0, 0.5px) rotate(0.5deg);\n            transform: translate(0, 0.5px) rotate(0.5deg); }\n  28% {\n    -webkit-transform: translate(0, 0.5px) rotate(1.5deg);\n            transform: translate(0, 0.5px) rotate(1.5deg); }\n  30% {\n    -webkit-transform: translate(0, -0.5px) rotate(2.5deg);\n            transform: translate(0, -0.5px) rotate(2.5deg); }\n  32% {\n    -webkit-transform: translate(0, 1.5px) rotate(-0.5deg);\n            transform: translate(0, 1.5px) rotate(-0.5deg); }\n  34% {\n    -webkit-transform: translate(0, 1.5px) rotate(-0.5deg);\n            transform: translate(0, 1.5px) rotate(-0.5deg); }\n  36% {\n    -webkit-transform: translate(0, -1.5px) rotate(2.5deg);\n            transform: translate(0, -1.5px) rotate(2.5deg); }\n  38% {\n    -webkit-transform: translate(0, 1.5px) rotate(-1.5deg);\n            transform: translate(0, 1.5px) rotate(-1.5deg); }\n  40% {\n    -webkit-transform: translate(0, -0.5px) rotate(2.5deg);\n            transform: translate(0, -0.5px) rotate(2.5deg); }\n  42% {\n    -webkit-transform: translate(0, 2.5px) rotate(-1.5deg);\n            transform: translate(0, 2.5px) rotate(-1.5deg); }\n  44% {\n    -webkit-transform: translate(0, 1.5px) rotate(0.5deg);\n            transform: translate(0, 1.5px) rotate(0.5deg); }\n  46% {\n    -webkit-transform: translate(0, -1.5px) rotate(2.5deg);\n            transform: translate(0, -1.5px) rotate(2.5deg); }\n  48% {\n    -webkit-transform: translate(0, -0.5px) rotate(0.5deg);\n            transform: translate(0, -0.5px) rotate(0.5deg); }\n  50% {\n    -webkit-transform: translate(0, 0.5px) rotate(0.5deg);\n            transform: translate(0, 0.5px) rotate(0.5deg); }\n  52% {\n    -webkit-transform: translate(0, 2.5px) rotate(2.5deg);\n            transform: translate(0, 2.5px) rotate(2.5deg); }\n  54% {\n    -webkit-transform: translate(0, -1.5px) rotate(1.5deg);\n            transform: translate(0, -1.5px) rotate(1.5deg); }\n  56% {\n    -webkit-transform: translate(0, 2.5px) rotate(2.5deg);\n            transform: translate(0, 2.5px) rotate(2.5deg); }\n  58% {\n    -webkit-transform: translate(0, 0.5px) rotate(2.5deg);\n            transform: translate(0, 0.5px) rotate(2.5deg); }\n  60% {\n    -webkit-transform: translate(0, 2.5px) rotate(2.5deg);\n            transform: translate(0, 2.5px) rotate(2.5deg); }\n  62% {\n    -webkit-transform: translate(0, -0.5px) rotate(2.5deg);\n            transform: translate(0, -0.5px) rotate(2.5deg); }\n  64% {\n    -webkit-transform: translate(0, -0.5px) rotate(1.5deg);\n            transform: translate(0, -0.5px) rotate(1.5deg); }\n  66% {\n    -webkit-transform: translate(0, 1.5px) rotate(-0.5deg);\n            transform: translate(0, 1.5px) rotate(-0.5deg); }\n  68% {\n    -webkit-transform: translate(0, -1.5px) rotate(-0.5deg);\n            transform: translate(0, -1.5px) rotate(-0.5deg); }\n  70% {\n    -webkit-transform: translate(0, 1.5px) rotate(0.5deg);\n            transform: translate(0, 1.5px) rotate(0.5deg); }\n  72% {\n    -webkit-transform: translate(0, 2.5px) rotate(1.5deg);\n            transform: translate(0, 2.5px) rotate(1.5deg); }\n  74% {\n    -webkit-transform: translate(0, -0.5px) rotate(0.5deg);\n            transform: translate(0, -0.5px) rotate(0.5deg); }\n  76% {\n    -webkit-transform: translate(0, -0.5px) rotate(2.5deg);\n            transform: translate(0, -0.5px) rotate(2.5deg); }\n  78% {\n    -webkit-transform: translate(0, -0.5px) rotate(1.5deg);\n            transform: translate(0, -0.5px) rotate(1.5deg); }\n  80% {\n    -webkit-transform: translate(0, 1.5px) rotate(1.5deg);\n            transform: translate(0, 1.5px) rotate(1.5deg); }\n  82% {\n    -webkit-transform: translate(0, -0.5px) rotate(0.5deg);\n            transform: translate(0, -0.5px) rotate(0.5deg); }\n  84% {\n    -webkit-transform: translate(0, 1.5px) rotate(2.5deg);\n            transform: translate(0, 1.5px) rotate(2.5deg); }\n  86% {\n    -webkit-transform: translate(0, -1.5px) rotate(-1.5deg);\n            transform: translate(0, -1.5px) rotate(-1.5deg); }\n  88% {\n    -webkit-transform: translate(0, -0.5px) rotate(2.5deg);\n            transform: translate(0, -0.5px) rotate(2.5deg); }\n  90% {\n    -webkit-transform: translate(0, 2.5px) rotate(-0.5deg);\n            transform: translate(0, 2.5px) rotate(-0.5deg); }\n  92% {\n    -webkit-transform: translate(0, 0.5px) rotate(-0.5deg);\n            transform: translate(0, 0.5px) rotate(-0.5deg); }\n  94% {\n    -webkit-transform: translate(0, 2.5px) rotate(0.5deg);\n            transform: translate(0, 2.5px) rotate(0.5deg); }\n  96% {\n    -webkit-transform: translate(0, -0.5px) rotate(1.5deg);\n            transform: translate(0, -0.5px) rotate(1.5deg); }\n  98% {\n    -webkit-transform: translate(0, -1.5px) rotate(-0.5deg);\n            transform: translate(0, -1.5px) rotate(-0.5deg); }\n  0%,\n  100% {\n    -webkit-transform: translate(0, 0) rotate(0deg);\n            transform: translate(0, 0) rotate(0deg); } }\n@keyframes my-face {\n  2% {\n    -webkit-transform: translate(0, 1.5px) rotate(1.5deg);\n            transform: translate(0, 1.5px) rotate(1.5deg); }\n  4% {\n    -webkit-transform: translate(0, -1.5px) rotate(-0.5deg);\n            transform: translate(0, -1.5px) rotate(-0.5deg); }\n  6% {\n    -webkit-transform: translate(0, 1.5px) rotate(-1.5deg);\n            transform: translate(0, 1.5px) rotate(-1.5deg); }\n  8% {\n    -webkit-transform: translate(0, -1.5px) rotate(-1.5deg);\n            transform: translate(0, -1.5px) rotate(-1.5deg); }\n  10% {\n    -webkit-transform: translate(0, 2.5px) rotate(1.5deg);\n            transform: translate(0, 2.5px) rotate(1.5deg); }\n  12% {\n    -webkit-transform: translate(0, -0.5px) rotate(1.5deg);\n            transform: translate(0, -0.5px) rotate(1.5deg); }\n  14% {\n    -webkit-transform: translate(0, -1.5px) rotate(1.5deg);\n            transform: translate(0, -1.5px) rotate(1.5deg); }\n  16% {\n    -webkit-transform: translate(0, -0.5px) rotate(-1.5deg);\n            transform: translate(0, -0.5px) rotate(-1.5deg); }\n  18% {\n    -webkit-transform: translate(0, 0.5px) rotate(-1.5deg);\n            transform: translate(0, 0.5px) rotate(-1.5deg); }\n  20% {\n    -webkit-transform: translate(0, -1.5px) rotate(2.5deg);\n            transform: translate(0, -1.5px) rotate(2.5deg); }\n  22% {\n    -webkit-transform: translate(0, 0.5px) rotate(-1.5deg);\n            transform: translate(0, 0.5px) rotate(-1.5deg); }\n  24% {\n    -webkit-transform: translate(0, 1.5px) rotate(1.5deg);\n            transform: translate(0, 1.5px) rotate(1.5deg); }\n  26% {\n    -webkit-transform: translate(0, 0.5px) rotate(0.5deg);\n            transform: translate(0, 0.5px) rotate(0.5deg); }\n  28% {\n    -webkit-transform: translate(0, 0.5px) rotate(1.5deg);\n            transform: translate(0, 0.5px) rotate(1.5deg); }\n  30% {\n    -webkit-transform: translate(0, -0.5px) rotate(2.5deg);\n            transform: translate(0, -0.5px) rotate(2.5deg); }\n  32% {\n    -webkit-transform: translate(0, 1.5px) rotate(-0.5deg);\n            transform: translate(0, 1.5px) rotate(-0.5deg); }\n  34% {\n    -webkit-transform: translate(0, 1.5px) rotate(-0.5deg);\n            transform: translate(0, 1.5px) rotate(-0.5deg); }\n  36% {\n    -webkit-transform: translate(0, -1.5px) rotate(2.5deg);\n            transform: translate(0, -1.5px) rotate(2.5deg); }\n  38% {\n    -webkit-transform: translate(0, 1.5px) rotate(-1.5deg);\n            transform: translate(0, 1.5px) rotate(-1.5deg); }\n  40% {\n    -webkit-transform: translate(0, -0.5px) rotate(2.5deg);\n            transform: translate(0, -0.5px) rotate(2.5deg); }\n  42% {\n    -webkit-transform: translate(0, 2.5px) rotate(-1.5deg);\n            transform: translate(0, 2.5px) rotate(-1.5deg); }\n  44% {\n    -webkit-transform: translate(0, 1.5px) rotate(0.5deg);\n            transform: translate(0, 1.5px) rotate(0.5deg); }\n  46% {\n    -webkit-transform: translate(0, -1.5px) rotate(2.5deg);\n            transform: translate(0, -1.5px) rotate(2.5deg); }\n  48% {\n    -webkit-transform: translate(0, -0.5px) rotate(0.5deg);\n            transform: translate(0, -0.5px) rotate(0.5deg); }\n  50% {\n    -webkit-transform: translate(0, 0.5px) rotate(0.5deg);\n            transform: translate(0, 0.5px) rotate(0.5deg); }\n  52% {\n    -webkit-transform: translate(0, 2.5px) rotate(2.5deg);\n            transform: translate(0, 2.5px) rotate(2.5deg); }\n  54% {\n    -webkit-transform: translate(0, -1.5px) rotate(1.5deg);\n            transform: translate(0, -1.5px) rotate(1.5deg); }\n  56% {\n    -webkit-transform: translate(0, 2.5px) rotate(2.5deg);\n            transform: translate(0, 2.5px) rotate(2.5deg); }\n  58% {\n    -webkit-transform: translate(0, 0.5px) rotate(2.5deg);\n            transform: translate(0, 0.5px) rotate(2.5deg); }\n  60% {\n    -webkit-transform: translate(0, 2.5px) rotate(2.5deg);\n            transform: translate(0, 2.5px) rotate(2.5deg); }\n  62% {\n    -webkit-transform: translate(0, -0.5px) rotate(2.5deg);\n            transform: translate(0, -0.5px) rotate(2.5deg); }\n  64% {\n    -webkit-transform: translate(0, -0.5px) rotate(1.5deg);\n            transform: translate(0, -0.5px) rotate(1.5deg); }\n  66% {\n    -webkit-transform: translate(0, 1.5px) rotate(-0.5deg);\n            transform: translate(0, 1.5px) rotate(-0.5deg); }\n  68% {\n    -webkit-transform: translate(0, -1.5px) rotate(-0.5deg);\n            transform: translate(0, -1.5px) rotate(-0.5deg); }\n  70% {\n    -webkit-transform: translate(0, 1.5px) rotate(0.5deg);\n            transform: translate(0, 1.5px) rotate(0.5deg); }\n  72% {\n    -webkit-transform: translate(0, 2.5px) rotate(1.5deg);\n            transform: translate(0, 2.5px) rotate(1.5deg); }\n  74% {\n    -webkit-transform: translate(0, -0.5px) rotate(0.5deg);\n            transform: translate(0, -0.5px) rotate(0.5deg); }\n  76% {\n    -webkit-transform: translate(0, -0.5px) rotate(2.5deg);\n            transform: translate(0, -0.5px) rotate(2.5deg); }\n  78% {\n    -webkit-transform: translate(0, -0.5px) rotate(1.5deg);\n            transform: translate(0, -0.5px) rotate(1.5deg); }\n  80% {\n    -webkit-transform: translate(0, 1.5px) rotate(1.5deg);\n            transform: translate(0, 1.5px) rotate(1.5deg); }\n  82% {\n    -webkit-transform: translate(0, -0.5px) rotate(0.5deg);\n            transform: translate(0, -0.5px) rotate(0.5deg); }\n  84% {\n    -webkit-transform: translate(0, 1.5px) rotate(2.5deg);\n            transform: translate(0, 1.5px) rotate(2.5deg); }\n  86% {\n    -webkit-transform: translate(0, -1.5px) rotate(-1.5deg);\n            transform: translate(0, -1.5px) rotate(-1.5deg); }\n  88% {\n    -webkit-transform: translate(0, -0.5px) rotate(2.5deg);\n            transform: translate(0, -0.5px) rotate(2.5deg); }\n  90% {\n    -webkit-transform: translate(0, 2.5px) rotate(-0.5deg);\n            transform: translate(0, 2.5px) rotate(-0.5deg); }\n  92% {\n    -webkit-transform: translate(0, 0.5px) rotate(-0.5deg);\n            transform: translate(0, 0.5px) rotate(-0.5deg); }\n  94% {\n    -webkit-transform: translate(0, 2.5px) rotate(0.5deg);\n            transform: translate(0, 2.5px) rotate(0.5deg); }\n  96% {\n    -webkit-transform: translate(0, -0.5px) rotate(1.5deg);\n            transform: translate(0, -0.5px) rotate(1.5deg); }\n  98% {\n    -webkit-transform: translate(0, -1.5px) rotate(-0.5deg);\n            transform: translate(0, -1.5px) rotate(-0.5deg); }\n  0%,\n  100% {\n    -webkit-transform: translate(0, 0) rotate(0deg);\n            transform: translate(0, 0) rotate(0deg); } }\n\n.dplayer {\n  position: relative;\n  overflow: hidden;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  line-height: 1; }\n  .dplayer * {\n    box-sizing: content-box; }\n  .dplayer svg {\n    width: 100%;\n    height: 100%; }\n    .dplayer svg path,\n    .dplayer svg circle {\n      fill: #fff; }\n  .dplayer:-webkit-full-screen {\n    width: 100%;\n    height: 100%;\n    background: #000;\n    position: fixed;\n    z-index: 100000;\n    left: 0;\n    top: 0;\n    margin: 0;\n    padding: 0;\n    -webkit-transform: translate(0, 0);\n            transform: translate(0, 0); }\n    .dplayer:-webkit-full-screen .dplayer-danmaku .dplayer-danmaku-top.dplayer-danmaku-move,\n    .dplayer:-webkit-full-screen .dplayer-danmaku .dplayer-danmaku-bottom.dplayer-danmaku-move {\n      -webkit-animation: danmaku-center 6s linear;\n              animation: danmaku-center 6s linear;\n      -webkit-animation-play-state: inherit;\n              animation-play-state: inherit; }\n    .dplayer:-webkit-full-screen .dplayer-danmaku .dplayer-danmaku-right.dplayer-danmaku-move {\n      -webkit-animation: danmaku 8s linear;\n              animation: danmaku 8s linear;\n      -webkit-animation-play-state: inherit;\n              animation-play-state: inherit; }\n  .dplayer.dplayer-no-danmaku .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-box .dplayer-setting-showdan,\n  .dplayer.dplayer-no-danmaku .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-box .dplayer-setting-danmaku,\n  .dplayer.dplayer-no-danmaku .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-box .dplayer-setting-danunlimit {\n    display: none; }\n  .dplayer.dplayer-no-danmaku .dplayer-controller .dplayer-icons .dplayer-comment {\n    display: none; }\n  .dplayer.dplayer-no-danmaku .dplayer-danmaku {\n    display: none; }\n  .dplayer.dplayer-live .dplayer-time {\n    display: none; }\n  .dplayer.dplayer-live .dplayer-bar-wrap {\n    display: none; }\n  .dplayer.dplayer-live .dplayer-setting-speed {\n    display: none; }\n  .dplayer.dplayer-live .dplayer-setting-loop {\n    display: none; }\n  .dplayer.dplayer-live.dplayer-no-danmaku .dplayer-setting {\n    display: none; }\n  .dplayer.dplayer-arrow .dplayer-danmaku {\n    font-size: 18px; }\n  .dplayer.dplayer-arrow .dplayer-icon {\n    margin: 0 -3px; }\n  .dplayer.dplayer-playing .dplayer-danmaku .dplayer-danmaku-move {\n    -webkit-animation-play-state: running;\n            animation-play-state: running; }\n  @media (min-width: 900px) {\n    .dplayer.dplayer-playing .dplayer-controller-mask {\n      opacity: 0; }\n    .dplayer.dplayer-playing .dplayer-controller {\n      opacity: 0; }\n    .dplayer.dplayer-playing:hover .dplayer-controller-mask {\n      opacity: 1; }\n    .dplayer.dplayer-playing:hover .dplayer-controller {\n      opacity: 1; } }\n  .dplayer.dplayer-loading .dplayer-bezel .diplayer-loading-icon {\n    display: block; }\n  .dplayer.dplayer-loading .dplayer-danmaku,\n  .dplayer.dplayer-loading .dplayer-danmaku-move, .dplayer.dplayer-paused .dplayer-danmaku,\n  .dplayer.dplayer-paused .dplayer-danmaku-move {\n    -webkit-animation-play-state: paused;\n            animation-play-state: paused; }\n  .dplayer.dplayer-hide-controller {\n    cursor: none; }\n    .dplayer.dplayer-hide-controller .dplayer-controller-mask {\n      opacity: 0;\n      -webkit-transform: translateY(100%);\n              transform: translateY(100%); }\n    .dplayer.dplayer-hide-controller .dplayer-controller {\n      opacity: 0;\n      -webkit-transform: translateY(100%);\n              transform: translateY(100%); }\n  .dplayer.dplayer-show-controller .dplayer-controller-mask {\n    opacity: 1; }\n  .dplayer.dplayer-show-controller .dplayer-controller {\n    opacity: 1; }\n  .dplayer.dplayer-fulled {\n    position: fixed;\n    z-index: 100000;\n    left: 0;\n    top: 0;\n    width: 100%;\n    height: 100%; }\n  .dplayer.dplayer-mobile .dplayer-controller .dplayer-icons .dplayer-volume,\n  .dplayer.dplayer-mobile .dplayer-controller .dplayer-icons .dplayer-camera-icon {\n    display: none; }\n  .dplayer.dplayer-mobile .dplayer-controller .dplayer-icons .dplayer-full .dplayer-full-in-icon {\n    position: static;\n    display: inline-block; }\n  .dplayer.dplayer-mobile .dplayer-bar-time {\n    display: none; }\n\n.dplayer-web-fullscreen-fix {\n  position: fixed;\n  top: 0;\n  left: 0;\n  margin: 0;\n  padding: 0; }\n\n[data-balloon]:before {\n  display: none; }\n\n[data-balloon]:after {\n  padding: 0.3em 0.7em;\n  background: rgba(17, 17, 17, 0.7); }\n\n[data-balloon][data-balloon-pos=\"up\"]:after {\n  margin-bottom: 0; }\n\n.dplayer-bezel {\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  font-size: 22px;\n  color: #fff;\n  pointer-events: none; }\n  .dplayer-bezel .dplayer-bezel-icon {\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    margin: -26px 0 0 -26px;\n    height: 52px;\n    width: 52px;\n    padding: 12px;\n    box-sizing: border-box;\n    background: rgba(0, 0, 0, 0.5);\n    border-radius: 50%;\n    opacity: 0;\n    pointer-events: none; }\n    .dplayer-bezel .dplayer-bezel-icon.dplayer-bezel-transition {\n      -webkit-animation: bezel-hide .5s linear;\n              animation: bezel-hide .5s linear; }\n\n@-webkit-keyframes bezel-hide {\n  from {\n    opacity: 1;\n    -webkit-transform: scale(1);\n            transform: scale(1); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale(2);\n            transform: scale(2); } }\n\n@keyframes bezel-hide {\n  from {\n    opacity: 1;\n    -webkit-transform: scale(1);\n            transform: scale(1); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale(2);\n            transform: scale(2); } }\n  .dplayer-bezel .dplayer-danloading {\n    position: absolute;\n    top: 50%;\n    margin-top: -7px;\n    width: 100%;\n    text-align: center;\n    font-size: 14px;\n    line-height: 14px;\n    -webkit-animation: my-face 5s infinite ease-in-out;\n            animation: my-face 5s infinite ease-in-out; }\n  .dplayer-bezel .diplayer-loading-icon {\n    display: none;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    margin: -18px 0 0 -18px;\n    height: 36px;\n    width: 36px;\n    pointer-events: none; }\n    .dplayer-bezel .diplayer-loading-icon .diplayer-loading-hide {\n      display: none; }\n    .dplayer-bezel .diplayer-loading-icon .diplayer-loading-dot {\n      -webkit-animation: diplayer-loading-dot-fade .8s ease infinite;\n              animation: diplayer-loading-dot-fade .8s ease infinite;\n      opacity: 0;\n      -webkit-transform-origin: 4px 4px;\n              transform-origin: 4px 4px; }\n      .dplayer-bezel .diplayer-loading-icon .diplayer-loading-dot.diplayer-loading-dot-7 {\n        -webkit-animation-delay: 0.7s;\n                animation-delay: 0.7s; }\n      .dplayer-bezel .diplayer-loading-icon .diplayer-loading-dot.diplayer-loading-dot-6 {\n        -webkit-animation-delay: 0.6s;\n                animation-delay: 0.6s; }\n      .dplayer-bezel .diplayer-loading-icon .diplayer-loading-dot.diplayer-loading-dot-5 {\n        -webkit-animation-delay: 0.5s;\n                animation-delay: 0.5s; }\n      .dplayer-bezel .diplayer-loading-icon .diplayer-loading-dot.diplayer-loading-dot-4 {\n        -webkit-animation-delay: 0.4s;\n                animation-delay: 0.4s; }\n      .dplayer-bezel .diplayer-loading-icon .diplayer-loading-dot.diplayer-loading-dot-3 {\n        -webkit-animation-delay: 0.3s;\n                animation-delay: 0.3s; }\n      .dplayer-bezel .diplayer-loading-icon .diplayer-loading-dot.diplayer-loading-dot-2 {\n        -webkit-animation-delay: 0.2s;\n                animation-delay: 0.2s; }\n      .dplayer-bezel .diplayer-loading-icon .diplayer-loading-dot.diplayer-loading-dot-1 {\n        -webkit-animation-delay: 0.1s;\n                animation-delay: 0.1s; }\n\n@-webkit-keyframes diplayer-loading-dot-fade {\n  0% {\n    opacity: .7;\n    -webkit-transform: scale(1.2, 1.2);\n            transform: scale(1.2, 1.2); }\n  50% {\n    opacity: .25;\n    -webkit-transform: scale(0.9, 0.9);\n            transform: scale(0.9, 0.9); }\n  to {\n    opacity: .25;\n    -webkit-transform: scale(0.85, 0.85);\n            transform: scale(0.85, 0.85); } }\n\n@keyframes diplayer-loading-dot-fade {\n  0% {\n    opacity: .7;\n    -webkit-transform: scale(1.2, 1.2);\n            transform: scale(1.2, 1.2); }\n  50% {\n    opacity: .25;\n    -webkit-transform: scale(0.9, 0.9);\n            transform: scale(0.9, 0.9); }\n  to {\n    opacity: .25;\n    -webkit-transform: scale(0.85, 0.85);\n            transform: scale(0.85, 0.85); } }\n\n.dplayer-controller-mask {\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAADGCAYAAAAT+OqFAAAAdklEQVQoz42QQQ7AIAgEF/T/D+kbq/RWAlnQyyazA4aoAB4FsBSA/bFjuF1EOL7VbrIrBuusmrt4ZZORfb6ehbWdnRHEIiITaEUKa5EJqUakRSaEYBJSCY2dEstQY7AuxahwXFrvZmWl2rh4JZ07z9dLtesfNj5q0FU3A5ObbwAAAABJRU5ErkJggg==) repeat-x bottom;\n  height: 98px;\n  width: 100%;\n  position: absolute;\n  bottom: 0;\n  transition: all 0.3s ease; }\n\n.dplayer-controller {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  height: 41px;\n  padding: 0 20px;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  transition: all 0.3s ease; }\n  .dplayer-controller.dplayer-controller-comment .dplayer-icons {\n    display: none; }\n  .dplayer-controller.dplayer-controller-comment .dplayer-icons.dplayer-comment-box {\n    display: block; }\n  .dplayer-controller .dplayer-bar-wrap {\n    padding: 5px 0;\n    cursor: pointer;\n    position: absolute;\n    bottom: 33px;\n    width: calc(100% - 40px);\n    height: 3px; }\n    .dplayer-controller .dplayer-bar-wrap:hover .dplayer-bar .dplayer-played .dplayer-thumb {\n      -webkit-transform: scale(1);\n              transform: scale(1); }\n    .dplayer-controller .dplayer-bar-wrap:hover .dplayer-highlight {\n      display: block;\n      width: 8px;\n      -webkit-transform: translateX(-4px);\n              transform: translateX(-4px);\n      top: 4px;\n      height: 40%; }\n    .dplayer-controller .dplayer-bar-wrap .dplayer-highlight {\n      z-index: 12;\n      position: absolute;\n      top: 5px;\n      width: 6px;\n      height: 20%;\n      border-radius: 6px;\n      background-color: #fff;\n      text-align: center;\n      -webkit-transform: translateX(-3px);\n              transform: translateX(-3px);\n      transition: all .2s ease-in-out; }\n      .dplayer-controller .dplayer-bar-wrap .dplayer-highlight:hover .dplayer-highlight-text {\n        display: block; }\n      .dplayer-controller .dplayer-bar-wrap .dplayer-highlight:hover ~ .dplayer-bar-preview {\n        opacity: 0; }\n      .dplayer-controller .dplayer-bar-wrap .dplayer-highlight:hover ~ .dplayer-bar-time {\n        opacity: 0; }\n      .dplayer-controller .dplayer-bar-wrap .dplayer-highlight .dplayer-highlight-text {\n        display: none;\n        position: absolute;\n        left: 50%;\n        top: -24px;\n        padding: 5px 8px;\n        background-color: rgba(0, 0, 0, 0.62);\n        color: #fff;\n        border-radius: 4px;\n        font-size: 12px;\n        white-space: nowrap;\n        -webkit-transform: translateX(-50%);\n                transform: translateX(-50%); }\n    .dplayer-controller .dplayer-bar-wrap .dplayer-bar-preview {\n      position: absolute;\n      background: #fff;\n      pointer-events: none;\n      display: none;\n      background-size: 16000px 100%; }\n    .dplayer-controller .dplayer-bar-wrap .dplayer-bar-preview-canvas {\n      position: absolute;\n      width: 100%;\n      height: 100%;\n      z-index: 1;\n      pointer-events: none; }\n    .dplayer-controller .dplayer-bar-wrap .dplayer-bar-time {\n      position: absolute;\n      left: 0px;\n      top: -20px;\n      border-radius: 4px;\n      padding: 5px 7px;\n      background-color: rgba(0, 0, 0, 0.62);\n      color: #fff;\n      font-size: 12px;\n      text-align: center;\n      opacity: 1;\n      transition: opacity .1s ease-in-out;\n      word-wrap: normal;\n      word-break: normal;\n      z-index: 2;\n      pointer-events: none; }\n      .dplayer-controller .dplayer-bar-wrap .dplayer-bar-time.hidden {\n        opacity: 0; }\n    .dplayer-controller .dplayer-bar-wrap .dplayer-bar {\n      position: relative;\n      height: 3px;\n      width: 100%;\n      background: rgba(255, 255, 255, 0.2);\n      cursor: pointer; }\n      .dplayer-controller .dplayer-bar-wrap .dplayer-bar .dplayer-loaded {\n        position: absolute;\n        left: 0;\n        top: 0;\n        bottom: 0;\n        background: rgba(255, 255, 255, 0.4);\n        height: 3px;\n        transition: all 0.5s ease;\n        will-change: width; }\n      .dplayer-controller .dplayer-bar-wrap .dplayer-bar .dplayer-played {\n        position: absolute;\n        left: 0;\n        top: 0;\n        bottom: 0;\n        height: 3px;\n        will-change: width; }\n        .dplayer-controller .dplayer-bar-wrap .dplayer-bar .dplayer-played .dplayer-thumb {\n          position: absolute;\n          top: 0;\n          right: 5px;\n          margin-top: -4px;\n          margin-right: -10px;\n          height: 11px;\n          width: 11px;\n          border-radius: 50%;\n          cursor: pointer;\n          transition: all .3s ease-in-out;\n          -webkit-transform: scale(0);\n                  transform: scale(0); }\n  .dplayer-controller .dplayer-icons {\n    height: 38px;\n    position: absolute;\n    bottom: 0; }\n    .dplayer-controller .dplayer-icons.dplayer-comment-box {\n      display: none;\n      position: absolute;\n      transition: all .3s ease-in-out;\n      z-index: 2;\n      height: 38px;\n      bottom: 0;\n      left: 20px;\n      right: 20px;\n      color: #fff; }\n      .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-icon {\n        padding: 7px; }\n      .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-icon {\n        position: absolute;\n        left: 0;\n        top: 0; }\n      .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-send-icon {\n        position: absolute;\n        right: 0;\n        top: 0; }\n      .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box {\n        position: absolute;\n        background: rgba(28, 28, 28, 0.9);\n        bottom: 41px;\n        left: 0;\n        box-shadow: 0 0 25px rgba(0, 0, 0, 0.3);\n        border-radius: 4px;\n        padding: 10px 10px 16px;\n        font-size: 14px;\n        width: 204px;\n        transition: all .3s ease-in-out;\n        -webkit-transform: scale(0);\n                transform: scale(0); }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box.dplayer-comment-setting-open {\n          -webkit-transform: scale(1);\n                  transform: scale(1); }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box input[type=radio] {\n          display: none; }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box label {\n          cursor: pointer; }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-title {\n          font-size: 13px;\n          color: #fff;\n          line-height: 30px; }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-type {\n          font-size: 0; }\n          .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-type .dplayer-comment-setting-title {\n            margin-bottom: 6px; }\n          .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-type label:nth-child(2) span {\n            border-radius: 4px 0 0 4px; }\n          .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-type label:nth-child(4) span {\n            border-radius: 0 4px 4px 0; }\n          .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-type span {\n            width: 33%;\n            padding: 4px 6px;\n            line-height: 16px;\n            display: inline-block;\n            font-size: 12px;\n            color: #fff;\n            border: 1px solid #fff;\n            margin-right: -1px;\n            box-sizing: border-box;\n            text-align: center;\n            cursor: pointer; }\n          .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-type input:checked + span {\n            background: #E4E4E6;\n            color: #1c1c1c; }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-color {\n          font-size: 0; }\n          .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-color label {\n            font-size: 0;\n            padding: 6px;\n            display: inline-block; }\n          .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-color span {\n            width: 22px;\n            height: 22px;\n            display: inline-block;\n            border-radius: 50%;\n            box-sizing: border-box;\n            cursor: pointer; }\n            .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-setting-box .dplayer-comment-setting-color span:hover {\n              -webkit-animation: my-face 5s infinite ease-in-out;\n                      animation: my-face 5s infinite ease-in-out; }\n      .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-input {\n        outline: none;\n        border: none;\n        padding: 8px 31px;\n        font-size: 14px;\n        line-height: 18px;\n        text-align: center;\n        border-radius: 4px;\n        background: none;\n        margin: 0;\n        height: 100%;\n        box-sizing: border-box;\n        width: 100%;\n        color: #fff; }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-input::-webkit-input-placeholder {\n          color: #fff;\n          opacity: 0.8; }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-input:-ms-input-placeholder {\n          color: #fff;\n          opacity: 0.8; }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-input::-ms-input-placeholder {\n          color: #fff;\n          opacity: 0.8; }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-input::placeholder {\n          color: #fff;\n          opacity: 0.8; }\n        .dplayer-controller .dplayer-icons.dplayer-comment-box .dplayer-comment-input::-ms-clear {\n          display: none; }\n    .dplayer-controller .dplayer-icons.dplayer-icons-left .dplayer-icon {\n      padding: 7px; }\n    .dplayer-controller .dplayer-icons.dplayer-icons-right {\n      right: 20px; }\n      .dplayer-controller .dplayer-icons.dplayer-icons-right .dplayer-icon {\n        padding: 8px; }\n    .dplayer-controller .dplayer-icons .dplayer-time,\n    .dplayer-controller .dplayer-icons .dplayer-live-badge {\n      line-height: 38px;\n      color: #eee;\n      text-shadow: 0 0 2px rgba(0, 0, 0, 0.5);\n      vertical-align: middle;\n      font-size: 13px;\n      cursor: default; }\n    .dplayer-controller .dplayer-icons .dplayer-live-dot {\n      display: inline-block;\n      width: 6px;\n      height: 6px;\n      vertical-align: 4%;\n      margin-right: 5px;\n      content: '';\n      border-radius: 6px; }\n    .dplayer-controller .dplayer-icons .dplayer-icon {\n      width: 40px;\n      height: 100%;\n      border: none;\n      background-color: transparent;\n      outline: none;\n      cursor: pointer;\n      vertical-align: middle;\n      box-sizing: border-box;\n      display: inline-block; }\n      .dplayer-controller .dplayer-icons .dplayer-icon .dplayer-icon-content {\n        transition: all .2s ease-in-out;\n        opacity: .8; }\n      .dplayer-controller .dplayer-icons .dplayer-icon:hover .dplayer-icon-content {\n        opacity: 1; }\n      .dplayer-controller .dplayer-icons .dplayer-icon.dplayer-quality-icon {\n        color: #fff;\n        width: auto;\n        line-height: 22px;\n        font-size: 14px; }\n      .dplayer-controller .dplayer-icons .dplayer-icon.dplayer-comment-icon {\n        padding: 10px 9px 9px; }\n      .dplayer-controller .dplayer-icons .dplayer-icon.dplayer-setting-icon {\n        padding-top: 8.5px; }\n      .dplayer-controller .dplayer-icons .dplayer-icon.dplayer-volume-icon {\n        width: 43px; }\n    .dplayer-controller .dplayer-icons .dplayer-volume {\n      position: relative;\n      display: inline-block;\n      cursor: pointer;\n      height: 100%; }\n      .dplayer-controller .dplayer-icons .dplayer-volume:hover .dplayer-volume-bar-wrap .dplayer-volume-bar {\n        width: 45px; }\n      .dplayer-controller .dplayer-icons .dplayer-volume:hover .dplayer-volume-bar-wrap .dplayer-volume-bar .dplayer-volume-bar-inner .dplayer-thumb {\n        -webkit-transform: scale(1);\n                transform: scale(1); }\n      .dplayer-controller .dplayer-icons .dplayer-volume.dplayer-volume-active .dplayer-volume-bar-wrap .dplayer-volume-bar {\n        width: 45px; }\n      .dplayer-controller .dplayer-icons .dplayer-volume.dplayer-volume-active .dplayer-volume-bar-wrap .dplayer-volume-bar .dplayer-volume-bar-inner .dplayer-thumb {\n        -webkit-transform: scale(1);\n                transform: scale(1); }\n      .dplayer-controller .dplayer-icons .dplayer-volume .dplayer-volume-bar-wrap {\n        display: inline-block;\n        margin: 0 10px 0 -5px;\n        vertical-align: middle;\n        height: 100%; }\n        .dplayer-controller .dplayer-icons .dplayer-volume .dplayer-volume-bar-wrap .dplayer-volume-bar {\n          position: relative;\n          top: 17px;\n          width: 0;\n          height: 3px;\n          background: #aaa;\n          transition: all 0.3s ease-in-out; }\n          .dplayer-controller .dplayer-icons .dplayer-volume .dplayer-volume-bar-wrap .dplayer-volume-bar .dplayer-volume-bar-inner {\n            position: absolute;\n            bottom: 0;\n            left: 0;\n            height: 100%;\n            transition: all 0.1s ease;\n            will-change: width; }\n            .dplayer-controller .dplayer-icons .dplayer-volume .dplayer-volume-bar-wrap .dplayer-volume-bar .dplayer-volume-bar-inner .dplayer-thumb {\n              position: absolute;\n              top: 0;\n              right: 5px;\n              margin-top: -4px;\n              margin-right: -10px;\n              height: 11px;\n              width: 11px;\n              border-radius: 50%;\n              cursor: pointer;\n              transition: all .3s ease-in-out;\n              -webkit-transform: scale(0);\n                      transform: scale(0); }\n    .dplayer-controller .dplayer-icons .dplayer-subtitle-btn {\n      display: inline-block;\n      height: 100%; }\n    .dplayer-controller .dplayer-icons .dplayer-setting {\n      display: inline-block;\n      height: 100%; }\n      .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-box {\n        position: absolute;\n        right: 0;\n        bottom: 50px;\n        -webkit-transform: scale(0);\n                transform: scale(0);\n        width: 150px;\n        border-radius: 2px;\n        background: rgba(28, 28, 28, 0.9);\n        padding: 7px 0;\n        transition: all .3s ease-in-out;\n        overflow: hidden;\n        z-index: 2; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-box > div {\n          display: none; }\n          .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-box > div.dplayer-setting-origin-panel {\n            display: block; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-box.dplayer-setting-box-open {\n          -webkit-transform: scale(1);\n                  transform: scale(1); }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-box.dplayer-setting-box-narrow {\n          width: 70px;\n          height: 180px;\n          text-align: center; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-box.dplayer-setting-box-speed .dplayer-setting-origin-panel {\n          display: none; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-box.dplayer-setting-box-speed .dplayer-setting-speed-panel {\n          display: block; }\n      .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-item,\n      .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-speed-item {\n        height: 30px;\n        padding: 5px 10px;\n        box-sizing: border-box;\n        cursor: pointer;\n        position: relative; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-item:hover,\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-speed-item:hover {\n          background-color: rgba(255, 255, 255, 0.1); }\n      .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-danmaku {\n        padding: 5px 0; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-danmaku .dplayer-label {\n          padding: 0 10px;\n          display: inline; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-danmaku:hover .dplayer-label {\n          display: none; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-danmaku:hover .dplayer-danmaku-bar-wrap {\n          display: inline-block; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-danmaku.dplayer-setting-danmaku-active .dplayer-label {\n          display: none; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-danmaku.dplayer-setting-danmaku-active .dplayer-danmaku-bar-wrap {\n          display: inline-block; }\n        .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-danmaku .dplayer-danmaku-bar-wrap {\n          padding: 0 10px;\n          box-sizing: border-box;\n          display: none;\n          vertical-align: middle;\n          height: 100%;\n          width: 100%; }\n          .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-danmaku .dplayer-danmaku-bar-wrap .dplayer-danmaku-bar {\n            position: relative;\n            top: 8.5px;\n            width: 100%;\n            height: 3px;\n            background: #fff;\n            transition: all 0.3s ease-in-out; }\n            .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-danmaku .dplayer-danmaku-bar-wrap .dplayer-danmaku-bar .dplayer-danmaku-bar-inner {\n              position: absolute;\n              bottom: 0;\n              left: 0;\n              height: 100%;\n              transition: all 0.1s ease;\n              background: #aaa;\n              will-change: width; }\n              .dplayer-controller .dplayer-icons .dplayer-setting .dplayer-setting-danmaku .dplayer-danmaku-bar-wrap .dplayer-danmaku-bar .dplayer-danmaku-bar-inner .dplayer-thumb {\n                position: absolute;\n                top: 0;\n                right: 5px;\n                margin-top: -4px;\n                margin-right: -10px;\n                height: 11px;\n                width: 11px;\n                border-radius: 50%;\n                cursor: pointer;\n                transition: all .3s ease-in-out;\n                background: #aaa; }\n    .dplayer-controller .dplayer-icons .dplayer-full {\n      display: inline-block;\n      height: 100%;\n      position: relative; }\n      .dplayer-controller .dplayer-icons .dplayer-full:hover .dplayer-full-in-icon {\n        display: block; }\n      .dplayer-controller .dplayer-icons .dplayer-full .dplayer-full-in-icon {\n        position: absolute;\n        top: -30px;\n        z-index: 1;\n        display: none; }\n    .dplayer-controller .dplayer-icons .dplayer-quality {\n      position: relative;\n      display: inline-block;\n      height: 100%;\n      z-index: 2; }\n      .dplayer-controller .dplayer-icons .dplayer-quality:hover .dplayer-quality-list {\n        display: block; }\n      .dplayer-controller .dplayer-icons .dplayer-quality:hover .dplayer-quality-mask {\n        display: block; }\n      .dplayer-controller .dplayer-icons .dplayer-quality .dplayer-quality-mask {\n        display: none;\n        position: absolute;\n        bottom: 38px;\n        left: -18px;\n        width: 80px;\n        padding-bottom: 12px; }\n      .dplayer-controller .dplayer-icons .dplayer-quality .dplayer-quality-list {\n        display: none;\n        font-size: 12px;\n        width: 80px;\n        border-radius: 2px;\n        background: rgba(28, 28, 28, 0.9);\n        padding: 5px 0;\n        transition: all .3s ease-in-out;\n        overflow: hidden;\n        color: #fff;\n        text-align: center; }\n      .dplayer-controller .dplayer-icons .dplayer-quality .dplayer-quality-item {\n        height: 25px;\n        box-sizing: border-box;\n        cursor: pointer;\n        line-height: 25px; }\n        .dplayer-controller .dplayer-icons .dplayer-quality .dplayer-quality-item:hover {\n          background-color: rgba(255, 255, 255, 0.1); }\n    .dplayer-controller .dplayer-icons .dplayer-comment {\n      display: inline-block;\n      height: 100%; }\n    .dplayer-controller .dplayer-icons .dplayer-label {\n      color: #eee;\n      font-size: 13px;\n      display: inline-block;\n      vertical-align: middle;\n      white-space: nowrap; }\n    .dplayer-controller .dplayer-icons .dplayer-toggle {\n      width: 32px;\n      height: 20px;\n      text-align: center;\n      font-size: 0;\n      vertical-align: middle;\n      position: absolute;\n      top: 5px;\n      right: 10px; }\n      .dplayer-controller .dplayer-icons .dplayer-toggle input {\n        max-height: 0;\n        max-width: 0;\n        display: none; }\n      .dplayer-controller .dplayer-icons .dplayer-toggle input + label {\n        display: inline-block;\n        position: relative;\n        box-shadow: #dfdfdf 0 0 0 0 inset;\n        border: 1px solid #dfdfdf;\n        height: 20px;\n        width: 32px;\n        border-radius: 10px;\n        box-sizing: border-box;\n        cursor: pointer;\n        transition: .2s ease-in-out; }\n      .dplayer-controller .dplayer-icons .dplayer-toggle input + label:before {\n        content: \"\";\n        position: absolute;\n        display: block;\n        height: 18px;\n        width: 18px;\n        top: 0;\n        left: 0;\n        border-radius: 15px;\n        transition: .2s ease-in-out; }\n      .dplayer-controller .dplayer-icons .dplayer-toggle input + label:after {\n        content: \"\";\n        position: absolute;\n        display: block;\n        left: 0;\n        top: 0;\n        border-radius: 15px;\n        background: #fff;\n        transition: .2s ease-in-out;\n        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);\n        height: 18px;\n        width: 18px; }\n      .dplayer-controller .dplayer-icons .dplayer-toggle input:checked + label {\n        border-color: rgba(255, 255, 255, 0.5); }\n      .dplayer-controller .dplayer-icons .dplayer-toggle input:checked + label:before {\n        width: 30px;\n        background: rgba(255, 255, 255, 0.5); }\n      .dplayer-controller .dplayer-icons .dplayer-toggle input:checked + label:after {\n        left: 12px; }\n\n.dplayer-danmaku {\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  font-size: 22px;\n  color: #fff; }\n  .dplayer-danmaku .dplayer-danmaku-item {\n    display: inline-block;\n    pointer-events: none;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none;\n    cursor: default;\n    white-space: nowrap;\n    text-shadow: 0.5px 0.5px 0.5px rgba(0, 0, 0, 0.5); }\n    .dplayer-danmaku .dplayer-danmaku-item--demo {\n      position: absolute;\n      visibility: hidden; }\n  .dplayer-danmaku .dplayer-danmaku-right {\n    position: absolute;\n    right: 0;\n    -webkit-transform: translateX(100%);\n            transform: translateX(100%); }\n    .dplayer-danmaku .dplayer-danmaku-right.dplayer-danmaku-move {\n      will-change: transform;\n      -webkit-animation: danmaku 5s linear;\n              animation: danmaku 5s linear;\n      -webkit-animation-play-state: paused;\n              animation-play-state: paused; }\n\n@-webkit-keyframes danmaku {\n  from {\n    -webkit-transform: translateX(100%);\n            transform: translateX(100%); } }\n\n@keyframes danmaku {\n  from {\n    -webkit-transform: translateX(100%);\n            transform: translateX(100%); } }\n  .dplayer-danmaku .dplayer-danmaku-top,\n  .dplayer-danmaku .dplayer-danmaku-bottom {\n    position: absolute;\n    width: 100%;\n    text-align: center;\n    visibility: hidden; }\n    .dplayer-danmaku .dplayer-danmaku-top.dplayer-danmaku-move,\n    .dplayer-danmaku .dplayer-danmaku-bottom.dplayer-danmaku-move {\n      will-change: visibility;\n      -webkit-animation: danmaku-center 4s linear;\n              animation: danmaku-center 4s linear;\n      -webkit-animation-play-state: paused;\n              animation-play-state: paused; }\n\n@-webkit-keyframes danmaku-center {\n  from {\n    visibility: visible; }\n  to {\n    visibility: visible; } }\n\n@keyframes danmaku-center {\n  from {\n    visibility: visible; }\n  to {\n    visibility: visible; } }\n\n.dplayer-logo {\n  pointer-events: none;\n  position: absolute;\n  left: 20px;\n  top: 20px;\n  max-width: 50px;\n  max-height: 50px; }\n  .dplayer-logo img {\n    max-width: 100%;\n    max-height: 100%;\n    background: none; }\n\n.dplayer-menu {\n  position: absolute;\n  width: 170px;\n  border-radius: 2px;\n  background: rgba(28, 28, 28, 0.85);\n  padding: 5px 0;\n  overflow: hidden;\n  z-index: 3;\n  display: none; }\n  .dplayer-menu.dplayer-menu-show {\n    display: block; }\n  .dplayer-menu .dplayer-menu-item {\n    height: 30px;\n    box-sizing: border-box;\n    cursor: pointer; }\n    .dplayer-menu .dplayer-menu-item:hover {\n      background-color: rgba(255, 255, 255, 0.1); }\n    .dplayer-menu .dplayer-menu-item a {\n      display: inline-block;\n      padding: 0 10px;\n      line-height: 30px;\n      color: #eee;\n      font-size: 13px;\n      display: inline-block;\n      vertical-align: middle;\n      width: 100%;\n      box-sizing: border-box;\n      white-space: nowrap;\n      text-overflow: ellipsis;\n      overflow: hidden; }\n      .dplayer-menu .dplayer-menu-item a:hover {\n        text-decoration: none; }\n\n.dplayer-notice {\n  opacity: 0;\n  position: absolute;\n  bottom: 60px;\n  left: 20px;\n  font-size: 14px;\n  border-radius: 2px;\n  background: rgba(28, 28, 28, 0.9);\n  padding: 7px 20px;\n  transition: all .3s ease-in-out;\n  overflow: hidden;\n  color: #fff;\n  pointer-events: none; }\n\n.dplayer-subtitle {\n  position: absolute;\n  bottom: 40px;\n  width: 90%;\n  left: 5%;\n  text-align: center;\n  color: #fff;\n  text-shadow: 0.5px 0.5px 0.5px rgba(0, 0, 0, 0.5);\n  font-size: 20px; }\n  .dplayer-subtitle.dplayer-subtitle-hide {\n    display: none; }\n\n.dplayer-mask {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  z-index: 1;\n  display: none; }\n  .dplayer-mask.dplayer-mask-show {\n    display: block; }\n\n.dplayer-video-wrap {\n  position: relative;\n  background: #000;\n  font-size: 0;\n  width: 100%;\n  height: 100%; }\n  .dplayer-video-wrap .dplayer-video {\n    width: 100%;\n    height: 100%;\n    display: none; }\n  .dplayer-video-wrap .dplayer-video-current {\n    display: block; }\n  .dplayer-video-wrap .dplayer-video-prepare {\n    display: none; }\n\n.dplayer-info-panel {\n  position: absolute;\n  top: 10px;\n  left: 10px;\n  width: 400px;\n  background: rgba(28, 28, 28, 0.8);\n  padding: 10px;\n  color: #fff;\n  font-size: 12px;\n  border-radius: 2px; }\n  .dplayer-info-panel-hide {\n    display: none; }\n  .dplayer-info-panel .dplayer-info-panel-close {\n    cursor: pointer;\n    position: absolute;\n    right: 10px;\n    top: 10px; }\n  .dplayer-info-panel .dplayer-info-panel-item > span {\n    display: inline-block;\n    vertical-align: middle;\n    line-height: 15px;\n    white-space: nowrap;\n    text-overflow: ellipsis;\n    overflow: hidden; }\n  .dplayer-info-panel .dplayer-info-panel-item-title {\n    width: 100px;\n    text-align: right;\n    margin-right: 10px; }\n  .dplayer-info-panel .dplayer-info-panel-item-data {\n    width: 260px; }\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/css-base.js":
/*!*************************************************!*\
  !*** ./node_modules/css-loader/lib/css-base.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function (useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if (item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function (modules, mediaQuery) {
		if (typeof modules === "string") modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for (var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if (typeof id === "number") alreadyImportedModules[id] = true;
		}
		for (i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if (typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if (mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if (mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}

/***/ }),

/***/ "./node_modules/detect-node/index.js":
/*!*******************************************!*\
  !*** ./node_modules/detect-node/index.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

module.exports = false;

// Only Node.JS has a process variable that is of [[Class]] process
try {
  module.exports = Object.prototype.toString.call(global.process) === '[object process]';
} catch (e) {}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/is-buffer/index.js":
/*!*****************************************!*\
  !*** ./node_modules/is-buffer/index.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */

// The _isBuffer check is for Safari 5-7 support, because it's missing
// Object.prototype.constructor. Remove this eventually
module.exports = function (obj) {
  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer);
};

function isBuffer(obj) {
  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj);
}

// For Node v0.10 support. Remove this eventually.
function isSlowBuffer(obj) {
  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0));
}

/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout() {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
})();
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch (e) {
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch (e) {
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }
}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e) {
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e) {
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }
}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while (len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) {
    return [];
};

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () {
    return '/';
};
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function () {
    return 0;
};

/***/ }),

/***/ "./node_modules/promise-polyfill/src/finally.js":
/*!******************************************************!*\
  !*** ./node_modules/promise-polyfill/src/finally.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (callback) {
  var constructor = this.constructor;
  return this.then(function (value) {
    return constructor.resolve(callback()).then(function () {
      return value;
    });
  }, function (reason) {
    return constructor.resolve(callback()).then(function () {
      return constructor.reject(reason);
    });
  });
};

/***/ }),

/***/ "./node_modules/promise-polyfill/src/index.js":
/*!****************************************************!*\
  !*** ./node_modules/promise-polyfill/src/index.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(setImmediate) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _finally = __webpack_require__(/*! ./finally */ "./node_modules/promise-polyfill/src/finally.js");

var _finally2 = _interopRequireDefault(_finally);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Store setTimeout reference so promise-polyfill will be unaffected by
// other code modifying setTimeout (like sinon.useFakeTimers())
var setTimeoutFunc = setTimeout;

function noop() {}

// Polyfill for Function.prototype.bind
function bind(fn, thisArg) {
  return function () {
    fn.apply(thisArg, arguments);
  };
}

function Promise(fn) {
  if (!(this instanceof Promise)) throw new TypeError('Promises must be constructed via new');
  if (typeof fn !== 'function') throw new TypeError('not a function');
  this._state = 0;
  this._handled = false;
  this._value = undefined;
  this._deferreds = [];

  doResolve(fn, this);
}

function handle(self, deferred) {
  while (self._state === 3) {
    self = self._value;
  }
  if (self._state === 0) {
    self._deferreds.push(deferred);
    return;
  }
  self._handled = true;
  Promise._immediateFn(function () {
    var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
    if (cb === null) {
      (self._state === 1 ? resolve : reject)(deferred.promise, self._value);
      return;
    }
    var ret;
    try {
      ret = cb(self._value);
    } catch (e) {
      reject(deferred.promise, e);
      return;
    }
    resolve(deferred.promise, ret);
  });
}

function resolve(self, newValue) {
  try {
    // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
    if (newValue === self) throw new TypeError('A promise cannot be resolved with itself.');
    if (newValue && ((typeof newValue === 'undefined' ? 'undefined' : _typeof(newValue)) === 'object' || typeof newValue === 'function')) {
      var then = newValue.then;
      if (newValue instanceof Promise) {
        self._state = 3;
        self._value = newValue;
        finale(self);
        return;
      } else if (typeof then === 'function') {
        doResolve(bind(then, newValue), self);
        return;
      }
    }
    self._state = 1;
    self._value = newValue;
    finale(self);
  } catch (e) {
    reject(self, e);
  }
}

function reject(self, newValue) {
  self._state = 2;
  self._value = newValue;
  finale(self);
}

function finale(self) {
  if (self._state === 2 && self._deferreds.length === 0) {
    Promise._immediateFn(function () {
      if (!self._handled) {
        Promise._unhandledRejectionFn(self._value);
      }
    });
  }

  for (var i = 0, len = self._deferreds.length; i < len; i++) {
    handle(self, self._deferreds[i]);
  }
  self._deferreds = null;
}

function Handler(onFulfilled, onRejected, promise) {
  this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
  this.onRejected = typeof onRejected === 'function' ? onRejected : null;
  this.promise = promise;
}

/**
 * Take a potentially misbehaving resolver function and make sure
 * onFulfilled and onRejected are only called once.
 *
 * Makes no guarantees about asynchrony.
 */
function doResolve(fn, self) {
  var done = false;
  try {
    fn(function (value) {
      if (done) return;
      done = true;
      resolve(self, value);
    }, function (reason) {
      if (done) return;
      done = true;
      reject(self, reason);
    });
  } catch (ex) {
    if (done) return;
    done = true;
    reject(self, ex);
  }
}

Promise.prototype['catch'] = function (onRejected) {
  return this.then(null, onRejected);
};

Promise.prototype.then = function (onFulfilled, onRejected) {
  var prom = new this.constructor(noop);

  handle(this, new Handler(onFulfilled, onRejected, prom));
  return prom;
};

Promise.prototype['finally'] = _finally2.default;

Promise.all = function (arr) {
  return new Promise(function (resolve, reject) {
    if (!arr || typeof arr.length === 'undefined') throw new TypeError('Promise.all accepts an array');
    var args = Array.prototype.slice.call(arr);
    if (args.length === 0) return resolve([]);
    var remaining = args.length;

    function res(i, val) {
      try {
        if (val && ((typeof val === 'undefined' ? 'undefined' : _typeof(val)) === 'object' || typeof val === 'function')) {
          var then = val.then;
          if (typeof then === 'function') {
            then.call(val, function (val) {
              res(i, val);
            }, reject);
            return;
          }
        }
        args[i] = val;
        if (--remaining === 0) {
          resolve(args);
        }
      } catch (ex) {
        reject(ex);
      }
    }

    for (var i = 0; i < args.length; i++) {
      res(i, args[i]);
    }
  });
};

Promise.resolve = function (value) {
  if (value && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value.constructor === Promise) {
    return value;
  }

  return new Promise(function (resolve) {
    resolve(value);
  });
};

Promise.reject = function (value) {
  return new Promise(function (resolve, reject) {
    reject(value);
  });
};

Promise.race = function (values) {
  return new Promise(function (resolve, reject) {
    for (var i = 0, len = values.length; i < len; i++) {
      values[i].then(resolve, reject);
    }
  });
};

// Use polyfill for setImmediate for performance gains
Promise._immediateFn = typeof setImmediate === 'function' && function (fn) {
  setImmediate(fn);
} || function (fn) {
  setTimeoutFunc(fn, 0);
};

Promise._unhandledRejectionFn = function _unhandledRejectionFn(err) {
  if (typeof console !== 'undefined' && console) {
    console.warn('Possible Unhandled Promise Rejection:', err); // eslint-disable-line no-console
  }
};

exports.default = Promise;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../timers-browserify/main.js */ "./node_modules/timers-browserify/main.js").setImmediate))

/***/ }),

/***/ "./node_modules/setimmediate/setImmediate.js":
/*!***************************************************!*\
  !*** ./node_modules/setimmediate/setImmediate.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global, process) {

(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
        // Callback can either be a function or a string
        if (typeof callback !== "function") {
            callback = new Function("" + callback);
        }
        // Copy function arguments
        var args = new Array(arguments.length - 1);
        for (var i = 0; i < args.length; i++) {
            args[i] = arguments[i + 1];
        }
        // Store and register the task
        var task = { callback: callback, args: args };
        tasksByHandle[nextHandle] = task;
        registerImmediate(nextHandle);
        return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
            case 0:
                callback();
                break;
            case 1:
                callback(args[0]);
                break;
            case 2:
                callback(args[0], args[1]);
                break;
            case 3:
                callback(args[0], args[1], args[2]);
                break;
            default:
                callback.apply(undefined, args);
                break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function registerImmediate(handle) {
            process.nextTick(function () {
                runIfPresent(handle);
            });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function () {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function onGlobalMessage(event) {
            if (event.source === global && typeof event.data === "string" && event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function registerImmediate(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function (event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function registerImmediate(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function registerImmediate(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function registerImmediate(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();
    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();
    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();
    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();
    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
})(typeof self === "undefined" ? typeof global === "undefined" ? undefined : global : self);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js"), __webpack_require__(/*! ./../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target) {
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertInto + " " + options.insertAt.before);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
	// get current location
	var location = typeof window !== "undefined" && window.location;

	if (!location) {
		throw new Error("fixUrls requires window.location");
	}

	// blank or null?
	if (!css || typeof css !== "string") {
		return css;
	}

	var baseUrl = location.protocol + "//" + location.host;
	var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
 This regular expression is just a way to recursively match brackets within
 a string.
 	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
    (  = Start a capturing group
      (?:  = Start a non-capturing group
          [^)(]  = Match anything that isn't a parentheses
          |  = OR
          \(  = Match a start parentheses
              (?:  = Start another non-capturing groups
                  [^)(]+  = Match anything that isn't a parentheses
                  |  = OR
                  \(  = Match a start parentheses
                      [^)(]*  = Match anything that isn't a parentheses
                  \)  = Match a end parentheses
              )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
  \)  = Match a close parens
 	 /gi  = Get all matches, not the first.  Be case insensitive.
  */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function (fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl.trim().replace(/^"(.*)"$/, function (o, $1) {
			return $1;
		}).replace(/^'(.*)'$/, function (o, $1) {
			return $1;
		});

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
			return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
			//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};

/***/ }),

/***/ "./node_modules/timers-browserify/main.js":
/*!************************************************!*\
  !*** ./node_modules/timers-browserify/main.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function () {
  return new Timeout(apply.call(setTimeout, window, arguments), clearTimeout);
};
exports.setInterval = function () {
  return new Timeout(apply.call(setInterval, window, arguments), clearInterval);
};
exports.clearTimeout = exports.clearInterval = function (timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function () {};
Timeout.prototype.close = function () {
  this._clearFn.call(window, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function (item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function (item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function (item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout) item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__(/*! setimmediate */ "./node_modules/setimmediate/setImmediate.js");
exports.setImmediate = setImmediate;
exports.clearImmediate = clearImmediate;

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var g;

// This works in non-strict mode
g = function () {
	return this;
}();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1, eval)("this");
} catch (e) {
	// This works if the window reference is available
	if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;

/***/ }),

/***/ "./src/assets/camera.svg":
/*!*******************************!*\
  !*** ./src/assets/camera.svg ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 32 32\"><path d=\"M16 23c-3.309 0-6-2.691-6-6s2.691-6 6-6 6 2.691 6 6-2.691 6-6 6zM16 13c-2.206 0-4 1.794-4 4s1.794 4 4 4c2.206 0 4-1.794 4-4s-1.794-4-4-4zM27 28h-22c-1.654 0-3-1.346-3-3v-16c0-1.654 1.346-3 3-3h3c0.552 0 1 0.448 1 1s-0.448 1-1 1h-3c-0.551 0-1 0.449-1 1v16c0 0.552 0.449 1 1 1h22c0.552 0 1-0.448 1-1v-16c0-0.551-0.448-1-1-1h-11c-0.552 0-1-0.448-1-1s0.448-1 1-1h11c1.654 0 3 1.346 3 3v16c0 1.654-1.346 3-3 3zM24 10.5c0 0.828 0.672 1.5 1.5 1.5s1.5-0.672 1.5-1.5c0-0.828-0.672-1.5-1.5-1.5s-1.5 0.672-1.5 1.5zM15 4c0 0.552-0.448 1-1 1h-4c-0.552 0-1-0.448-1-1v0c0-0.552 0.448-1 1-1h4c0.552 0 1 0.448 1 1v0z\"></path></svg>"

/***/ }),

/***/ "./src/assets/comment-off.svg":
/*!************************************!*\
  !*** ./src/assets/comment-off.svg ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 32 32\"><path d=\"M27.090 0.131h-22.731c-2.354 0-4.262 1.839-4.262 4.109v16.401c0 2.269 1.908 4.109 4.262 4.109h4.262v-2.706h8.469l-8.853 8.135 1.579 1.451 7.487-6.88h9.787c2.353 0 4.262-1.84 4.262-4.109v-16.401c0-2.27-1.909-4.109-4.262-4.109v0zM28.511 19.304c0 1.512-1.272 2.738-2.841 2.738h-8.425l-0.076-0.070-0.076 0.070h-11.311c-1.569 0-2.841-1.226-2.841-2.738v-13.696c0-1.513 1.272-2.739 2.841-2.739h19.889c1.569 0 2.841-0.142 2.841 1.37v15.064z\"></path></svg>"

/***/ }),

/***/ "./src/assets/comment.svg":
/*!********************************!*\
  !*** ./src/assets/comment.svg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 32 32\"><path d=\"M27.128 0.38h-22.553c-2.336 0-4.229 1.825-4.229 4.076v16.273c0 2.251 1.893 4.076 4.229 4.076h4.229v-2.685h8.403l-8.784 8.072 1.566 1.44 7.429-6.827h9.71c2.335 0 4.229-1.825 4.229-4.076v-16.273c0-2.252-1.894-4.076-4.229-4.076zM28.538 19.403c0 1.5-1.262 2.717-2.819 2.717h-8.36l-0.076-0.070-0.076 0.070h-11.223c-1.557 0-2.819-1.217-2.819-2.717v-13.589c0-1.501 1.262-2.718 2.819-2.718h19.734c1.557 0 2.819-0.141 2.819 1.359v14.947zM9.206 10.557c-1.222 0-2.215 0.911-2.215 2.036s0.992 2.035 2.215 2.035c1.224 0 2.216-0.911 2.216-2.035s-0.992-2.036-2.216-2.036zM22.496 10.557c-1.224 0-2.215 0.911-2.215 2.036s0.991 2.035 2.215 2.035c1.224 0 2.215-0.911 2.215-2.035s-0.991-2.036-2.215-2.036zM15.852 10.557c-1.224 0-2.215 0.911-2.215 2.036s0.991 2.035 2.215 2.035c1.222 0 2.215-0.911 2.215-2.035s-0.992-2.036-2.215-2.036z\"></path></svg>"

/***/ }),

/***/ "./src/assets/full-web.svg":
/*!*********************************!*\
  !*** ./src/assets/full-web.svg ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 32 33\"><path d=\"M24.965 24.38h-18.132c-1.366 0-2.478-1.113-2.478-2.478v-11.806c0-1.364 1.111-2.478 2.478-2.478h18.132c1.366 0 2.478 1.113 2.478 2.478v11.806c0 1.364-1.11 2.478-2.478 2.478zM6.833 10.097v11.806h18.134l-0.002-11.806h-18.132zM2.478 28.928h5.952c0.684 0 1.238-0.554 1.238-1.239 0-0.684-0.554-1.238-1.238-1.238h-5.952v-5.802c0-0.684-0.554-1.239-1.238-1.239s-1.239 0.556-1.239 1.239v5.802c0 1.365 1.111 2.478 2.478 2.478zM30.761 19.412c-0.684 0-1.238 0.554-1.238 1.238v5.801h-5.951c-0.686 0-1.239 0.554-1.239 1.238 0 0.686 0.554 1.239 1.239 1.239h5.951c1.366 0 2.478-1.111 2.478-2.478v-5.801c0-0.683-0.554-1.238-1.239-1.238zM0 5.55v5.802c0 0.683 0.554 1.238 1.238 1.238s1.238-0.555 1.238-1.238v-5.802h5.952c0.684 0 1.238-0.554 1.238-1.238s-0.554-1.238-1.238-1.238h-5.951c-1.366-0.001-2.478 1.111-2.478 2.476zM32 11.35v-5.801c0-1.365-1.11-2.478-2.478-2.478h-5.951c-0.686 0-1.239 0.554-1.239 1.238s0.554 1.238 1.239 1.238h5.951v5.801c0 0.683 0.554 1.237 1.238 1.237 0.686 0.002 1.239-0.553 1.239-1.236z\"></path></svg>"

/***/ }),

/***/ "./src/assets/full.svg":
/*!*****************************!*\
  !*** ./src/assets/full.svg ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 32 33\"><path d=\"M6.667 28h-5.333c-0.8 0-1.333-0.533-1.333-1.333v-5.333c0-0.8 0.533-1.333 1.333-1.333s1.333 0.533 1.333 1.333v4h4c0.8 0 1.333 0.533 1.333 1.333s-0.533 1.333-1.333 1.333zM30.667 28h-5.333c-0.8 0-1.333-0.533-1.333-1.333s0.533-1.333 1.333-1.333h4v-4c0-0.8 0.533-1.333 1.333-1.333s1.333 0.533 1.333 1.333v5.333c0 0.8-0.533 1.333-1.333 1.333zM30.667 12c-0.8 0-1.333-0.533-1.333-1.333v-4h-4c-0.8 0-1.333-0.533-1.333-1.333s0.533-1.333 1.333-1.333h5.333c0.8 0 1.333 0.533 1.333 1.333v5.333c0 0.8-0.533 1.333-1.333 1.333zM1.333 12c-0.8 0-1.333-0.533-1.333-1.333v-5.333c0-0.8 0.533-1.333 1.333-1.333h5.333c0.8 0 1.333 0.533 1.333 1.333s-0.533 1.333-1.333 1.333h-4v4c0 0.8-0.533 1.333-1.333 1.333z\"></path></svg>"

/***/ }),

/***/ "./src/assets/loading.svg":
/*!********************************!*\
  !*** ./src/assets/loading.svg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg version=\"1.1\" viewBox=\"0 0 22 22\"><svg x=\"7\" y=\"1\"><circle class=\"diplayer-loading-dot diplayer-loading-dot-0\" cx=\"4\" cy=\"4\" r=\"2\"></circle></svg><svg x=\"11\" y=\"3\"><circle class=\"diplayer-loading-dot diplayer-loading-dot-1\" cx=\"4\" cy=\"4\" r=\"2\"></circle></svg><svg x=\"13\" y=\"7\"><circle class=\"diplayer-loading-dot diplayer-loading-dot-2\" cx=\"4\" cy=\"4\" r=\"2\"></circle></svg><svg x=\"11\" y=\"11\"><circle class=\"diplayer-loading-dot diplayer-loading-dot-3\" cx=\"4\" cy=\"4\" r=\"2\"></circle></svg><svg x=\"7\" y=\"13\"><circle class=\"diplayer-loading-dot diplayer-loading-dot-4\" cx=\"4\" cy=\"4\" r=\"2\"></circle></svg><svg x=\"3\" y=\"11\"><circle class=\"diplayer-loading-dot diplayer-loading-dot-5\" cx=\"4\" cy=\"4\" r=\"2\"></circle></svg><svg x=\"1\" y=\"7\"><circle class=\"diplayer-loading-dot diplayer-loading-dot-6\" cx=\"4\" cy=\"4\" r=\"2\"></circle></svg><svg x=\"3\" y=\"3\"><circle class=\"diplayer-loading-dot diplayer-loading-dot-7\" cx=\"4\" cy=\"4\" r=\"2\"></circle></svg></svg>"

/***/ }),

/***/ "./src/assets/pallette.svg":
/*!*********************************!*\
  !*** ./src/assets/pallette.svg ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 32 32\"><path d=\"M19.357 2.88c1.749 0 3.366 0.316 4.851 0.946 1.485 0.632 2.768 1.474 3.845 2.533s1.922 2.279 2.532 3.661c0.611 1.383 0.915 2.829 0.915 4.334 0 1.425-0.304 2.847-0.915 4.271-0.611 1.425-1.587 2.767-2.928 4.028-0.855 0.813-1.811 1.607-2.869 2.38s-2.136 1.465-3.233 2.075c-1.099 0.61-2.198 1.098-3.296 1.465-1.098 0.366-2.115 0.549-3.051 0.549-1.343 0-2.441-0.438-3.296-1.311-0.854-0.876-1.281-2.41-1.281-4.608 0-0.366 0.020-0.773 0.060-1.221s0.062-0.895 0.062-1.343c0-0.773-0.183-1.353-0.55-1.738-0.366-0.387-0.793-0.58-1.281-0.58-0.652 0-1.21 0.295-1.678 0.886s-0.926 1.23-1.373 1.921c-0.447 0.693-0.905 1.334-1.372 1.923s-1.028 0.886-1.679 0.886c-0.529 0-1.048-0.427-1.556-1.282s-0.763-2.259-0.763-4.212c0-2.197 0.529-4.241 1.587-6.133s2.462-3.529 4.21-4.912c1.75-1.383 3.762-2.471 6.041-3.264 2.277-0.796 4.617-1.212 7.018-1.253zM7.334 15.817c0.569 0 1.047-0.204 1.434-0.611s0.579-0.875 0.579-1.404c0-0.569-0.193-1.047-0.579-1.434s-0.864-0.579-1.434-0.579c-0.529 0-0.987 0.193-1.373 0.579s-0.58 0.864-0.58 1.434c0 0.53 0.194 0.998 0.58 1.404 0.388 0.407 0.845 0.611 1.373 0.611zM12.216 11.79c0.691 0 1.292-0.254 1.8-0.763s0.762-1.107 0.762-1.8c0-0.732-0.255-1.343-0.762-1.831-0.509-0.489-1.109-0.732-1.8-0.732-0.732 0-1.342 0.244-1.831 0.732-0.488 0.488-0.732 1.098-0.732 1.831 0 0.693 0.244 1.292 0.732 1.8s1.099 0.763 1.831 0.763zM16.366 25.947c0.692 0 1.282-0.214 1.77-0.64s0.732-0.987 0.732-1.678-0.244-1.261-0.732-1.709c-0.489-0.448-1.078-0.671-1.77-0.671-0.65 0-1.21 0.223-1.678 0.671s-0.702 1.018-0.702 1.709c0 0.692 0.234 1.25 0.702 1.678s1.027 0.64 1.678 0.64zM19.113 9.592c0.651 0 1.129-0.203 1.433-0.611 0.305-0.406 0.459-0.874 0.459-1.404 0-0.488-0.154-0.947-0.459-1.373-0.304-0.427-0.782-0.641-1.433-0.641-0.529 0-1.008 0.193-1.434 0.58s-0.64 0.865-0.64 1.434c0 0.571 0.213 1.049 0.64 1.434 0.427 0.389 0.905 0.581 1.434 0.581zM24.848 12.826c0.57 0 1.067-0.213 1.495-0.64 0.427-0.427 0.64-0.947 0.64-1.556 0-0.57-0.214-1.068-0.64-1.495-0.428-0.427-0.927-0.64-1.495-0.64-0.611 0-1.129 0.213-1.555 0.64-0.428 0.427-0.642 0.926-0.642 1.495 0 0.611 0.213 1.129 0.642 1.556s0.947 0.64 1.555 0.64z\"></path></svg>"

/***/ }),

/***/ "./src/assets/pause.svg":
/*!******************************!*\
  !*** ./src/assets/pause.svg ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 17 32\"><path d=\"M14.080 4.8q2.88 0 2.88 2.048v18.24q0 2.112-2.88 2.112t-2.88-2.112v-18.24q0-2.048 2.88-2.048zM2.88 4.8q2.88 0 2.88 2.048v18.24q0 2.112-2.88 2.112t-2.88-2.112v-18.24q0-2.048 2.88-2.048z\"></path></svg>"

/***/ }),

/***/ "./src/assets/play.svg":
/*!*****************************!*\
  !*** ./src/assets/play.svg ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 16 32\"><path d=\"M15.552 15.168q0.448 0.32 0.448 0.832 0 0.448-0.448 0.768l-13.696 8.512q-0.768 0.512-1.312 0.192t-0.544-1.28v-16.448q0-0.96 0.544-1.28t1.312 0.192z\"></path></svg>"

/***/ }),

/***/ "./src/assets/right.svg":
/*!******************************!*\
  !*** ./src/assets/right.svg ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 32 32\"><path d=\"M22 16l-10.105-10.6-1.895 1.987 8.211 8.613-8.211 8.612 1.895 1.988 8.211-8.613z\"></path></svg>"

/***/ }),

/***/ "./src/assets/send.svg":
/*!*****************************!*\
  !*** ./src/assets/send.svg ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 32 32\"><path d=\"M13.725 30l3.9-5.325-3.9-1.125v6.45zM0 17.5l11.050 3.35 13.6-11.55-10.55 12.425 11.8 3.65 6.1-23.375-32 15.5z\"></path></svg>"

/***/ }),

/***/ "./src/assets/setting.svg":
/*!********************************!*\
  !*** ./src/assets/setting.svg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 32 28\"><path d=\"M28.633 17.104c0.035 0.21 0.026 0.463-0.026 0.76s-0.14 0.598-0.262 0.904c-0.122 0.306-0.271 0.581-0.445 0.825s-0.367 0.419-0.576 0.524c-0.209 0.105-0.393 0.157-0.55 0.157s-0.332-0.035-0.524-0.105c-0.175-0.052-0.393-0.1-0.655-0.144s-0.528-0.052-0.799-0.026c-0.271 0.026-0.541 0.083-0.812 0.17s-0.502 0.236-0.694 0.445c-0.419 0.437-0.664 0.934-0.734 1.493s0.009 1.092 0.236 1.598c0.175 0.349 0.148 0.699-0.079 1.048-0.105 0.14-0.271 0.284-0.498 0.432s-0.476 0.284-0.747 0.406-0.555 0.218-0.851 0.288c-0.297 0.070-0.559 0.105-0.786 0.105-0.157 0-0.306-0.061-0.445-0.183s-0.236-0.253-0.288-0.393h-0.026c-0.192-0.541-0.52-1.009-0.982-1.402s-1-0.589-1.611-0.589c-0.594 0-1.131 0.197-1.611 0.589s-0.816 0.851-1.009 1.375c-0.087 0.21-0.218 0.362-0.393 0.458s-0.367 0.144-0.576 0.144c-0.244 0-0.52-0.044-0.825-0.131s-0.611-0.197-0.917-0.327c-0.306-0.131-0.581-0.284-0.825-0.458s-0.428-0.349-0.55-0.524c-0.087-0.122-0.135-0.266-0.144-0.432s0.057-0.397 0.197-0.694c0.192-0.402 0.266-0.86 0.223-1.375s-0.266-0.991-0.668-1.428c-0.244-0.262-0.541-0.432-0.891-0.511s-0.681-0.109-0.995-0.092c-0.367 0.017-0.742 0.087-1.127 0.21-0.244 0.070-0.489 0.052-0.734-0.052-0.192-0.070-0.371-0.231-0.537-0.485s-0.314-0.533-0.445-0.838c-0.131-0.306-0.231-0.62-0.301-0.943s-0.087-0.59-0.052-0.799c0.052-0.384 0.227-0.629 0.524-0.734 0.524-0.21 0.995-0.555 1.415-1.035s0.629-1.017 0.629-1.611c0-0.611-0.21-1.144-0.629-1.598s-0.891-0.786-1.415-0.996c-0.157-0.052-0.288-0.179-0.393-0.38s-0.157-0.406-0.157-0.616c0-0.227 0.035-0.48 0.105-0.76s0.162-0.55 0.275-0.812 0.244-0.502 0.393-0.72c0.148-0.218 0.31-0.38 0.485-0.485 0.14-0.087 0.275-0.122 0.406-0.105s0.275 0.052 0.432 0.105c0.524 0.21 1.070 0.275 1.637 0.197s1.070-0.327 1.506-0.747c0.21-0.209 0.362-0.467 0.458-0.773s0.157-0.607 0.183-0.904c0.026-0.297 0.026-0.568 0-0.812s-0.048-0.419-0.065-0.524c-0.035-0.105-0.066-0.227-0.092-0.367s-0.013-0.262 0.039-0.367c0.105-0.244 0.293-0.458 0.563-0.642s0.563-0.336 0.878-0.458c0.314-0.122 0.62-0.214 0.917-0.275s0.533-0.092 0.707-0.092c0.227 0 0.406 0.074 0.537 0.223s0.223 0.301 0.275 0.458c0.192 0.471 0.507 0.886 0.943 1.244s0.952 0.537 1.546 0.537c0.611 0 1.153-0.17 1.624-0.511s0.803-0.773 0.996-1.297c0.070-0.14 0.179-0.284 0.327-0.432s0.301-0.223 0.458-0.223c0.244 0 0.511 0.035 0.799 0.105s0.572 0.166 0.851 0.288c0.279 0.122 0.537 0.279 0.773 0.472s0.423 0.402 0.563 0.629c0.087 0.14 0.113 0.293 0.079 0.458s-0.070 0.284-0.105 0.354c-0.227 0.506-0.297 1.039-0.21 1.598s0.341 1.048 0.76 1.467c0.419 0.419 0.934 0.651 1.546 0.694s1.179-0.057 1.703-0.301c0.14-0.087 0.31-0.122 0.511-0.105s0.371 0.096 0.511 0.236c0.262 0.244 0.493 0.616 0.694 1.113s0.336 1 0.406 1.506c0.035 0.297-0.013 0.528-0.144 0.694s-0.266 0.275-0.406 0.327c-0.542 0.192-1.004 0.528-1.388 1.009s-0.576 1.026-0.576 1.637c0 0.594 0.162 1.113 0.485 1.559s0.747 0.764 1.27 0.956c0.122 0.070 0.227 0.14 0.314 0.21 0.192 0.157 0.323 0.358 0.393 0.602v0zM16.451 19.462c0.786 0 1.528-0.149 2.227-0.445s1.305-0.707 1.821-1.231c0.515-0.524 0.921-1.131 1.218-1.821s0.445-1.428 0.445-2.214c0-0.786-0.148-1.524-0.445-2.214s-0.703-1.292-1.218-1.808c-0.515-0.515-1.122-0.921-1.821-1.218s-1.441-0.445-2.227-0.445c-0.786 0-1.524 0.148-2.214 0.445s-1.292 0.703-1.808 1.218c-0.515 0.515-0.921 1.118-1.218 1.808s-0.445 1.428-0.445 2.214c0 0.786 0.149 1.524 0.445 2.214s0.703 1.297 1.218 1.821c0.515 0.524 1.118 0.934 1.808 1.231s1.428 0.445 2.214 0.445v0z\"></path></svg>"

/***/ }),

/***/ "./src/assets/subtitle.svg":
/*!*********************************!*\
  !*** ./src/assets/subtitle.svg ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 32 32\"><path d=\"M26.667 5.333h-21.333c-0 0-0.001 0-0.001 0-1.472 0-2.666 1.194-2.666 2.666 0 0 0 0.001 0 0.001v-0 16c0 0 0 0.001 0 0.001 0 1.472 1.194 2.666 2.666 2.666 0 0 0.001 0 0.001 0h21.333c0 0 0.001 0 0.001 0 1.472 0 2.666-1.194 2.666-2.666 0-0 0-0.001 0-0.001v0-16c0-0 0-0.001 0-0.001 0-1.472-1.194-2.666-2.666-2.666-0 0-0.001 0-0.001 0h0zM5.333 16h5.333v2.667h-5.333v-2.667zM18.667 24h-13.333v-2.667h13.333v2.667zM26.667 24h-5.333v-2.667h5.333v2.667zM26.667 18.667h-13.333v-2.667h13.333v2.667z\"></path></svg>"

/***/ }),

/***/ "./src/assets/volume-down.svg":
/*!************************************!*\
  !*** ./src/assets/volume-down.svg ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 21 32\"><path d=\"M13.728 6.272v19.456q0 0.448-0.352 0.8t-0.8 0.32-0.8-0.32l-5.952-5.952h-4.672q-0.48 0-0.8-0.352t-0.352-0.8v-6.848q0-0.48 0.352-0.8t0.8-0.352h4.672l5.952-5.952q0.32-0.32 0.8-0.32t0.8 0.32 0.352 0.8zM20.576 16q0 1.344-0.768 2.528t-2.016 1.664q-0.16 0.096-0.448 0.096-0.448 0-0.8-0.32t-0.32-0.832q0-0.384 0.192-0.64t0.544-0.448 0.608-0.384 0.512-0.64 0.192-1.024-0.192-1.024-0.512-0.64-0.608-0.384-0.544-0.448-0.192-0.64q0-0.48 0.32-0.832t0.8-0.32q0.288 0 0.448 0.096 1.248 0.48 2.016 1.664t0.768 2.528z\"></path></svg>"

/***/ }),

/***/ "./src/assets/volume-off.svg":
/*!***********************************!*\
  !*** ./src/assets/volume-off.svg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 21 32\"><path d=\"M13.728 6.272v19.456q0 0.448-0.352 0.8t-0.8 0.32-0.8-0.32l-5.952-5.952h-4.672q-0.48 0-0.8-0.352t-0.352-0.8v-6.848q0-0.48 0.352-0.8t0.8-0.352h4.672l5.952-5.952q0.32-0.32 0.8-0.32t0.8 0.32 0.352 0.8z\"></path></svg>"

/***/ }),

/***/ "./src/assets/volume-up.svg":
/*!**********************************!*\
  !*** ./src/assets/volume-up.svg ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 21 32\"><path d=\"M13.728 6.272v19.456q0 0.448-0.352 0.8t-0.8 0.32-0.8-0.32l-5.952-5.952h-4.672q-0.48 0-0.8-0.352t-0.352-0.8v-6.848q0-0.48 0.352-0.8t0.8-0.352h4.672l5.952-5.952q0.32-0.32 0.8-0.32t0.8 0.32 0.352 0.8zM20.576 16q0 1.344-0.768 2.528t-2.016 1.664q-0.16 0.096-0.448 0.096-0.448 0-0.8-0.32t-0.32-0.832q0-0.384 0.192-0.64t0.544-0.448 0.608-0.384 0.512-0.64 0.192-1.024-0.192-1.024-0.512-0.64-0.608-0.384-0.544-0.448-0.192-0.64q0-0.48 0.32-0.832t0.8-0.32q0.288 0 0.448 0.096 1.248 0.48 2.016 1.664t0.768 2.528zM25.152 16q0 2.72-1.536 5.056t-4 3.36q-0.256 0.096-0.448 0.096-0.48 0-0.832-0.352t-0.32-0.8q0-0.704 0.672-1.056 1.024-0.512 1.376-0.8 1.312-0.96 2.048-2.4t0.736-3.104-0.736-3.104-2.048-2.4q-0.352-0.288-1.376-0.8-0.672-0.352-0.672-1.056 0-0.448 0.32-0.8t0.8-0.352q0.224 0 0.48 0.096 2.496 1.056 4 3.36t1.536 5.056z\"></path></svg>"

/***/ }),

/***/ "./src/css/index.scss":
/*!****************************!*\
  !*** ./src/css/index.scss ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--6-1!../../node_modules/postcss-loader/lib??ref--6-2!../../node_modules/sass-loader/lib/loader.js!./index.scss */ "./node_modules/css-loader/index.js??ref--6-1!./node_modules/postcss-loader/lib/index.js??ref--6-2!./node_modules/sass-loader/lib/loader.js!./src/css/index.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/js/api.js":
/*!***********************!*\
  !*** ./src/js/api.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    send: function send(options) {
        _axios2.default.post(options.url, options.data).then(function (response) {
            var data = response.data;
            if (!data || data.code !== 0) {
                options.error && options.error(data && data.msg);
                return;
            }
            options.success && options.success(data);
        }).catch(function (e) {
            console.error(e);
            options.error && options.error();
        });
    },

    read: function read(options) {
        _axios2.default.get(options.url).then(function (response) {
            var data = response.data;
            if (!data || data.code !== 0) {
                options.error && options.error(data && data.msg);
                return;
            }
            options.success && options.success(data.data.map(function (item) {
                return {
                    time: item[0],
                    type: item[1],
                    color: item[2],
                    author: item[3],
                    text: item[4]
                };
            }));
        }).catch(function (e) {
            console.error(e);
            options.error && options.error();
        });
    }
};

/***/ }),

/***/ "./src/js/bar.js":
/*!***********************!*\
  !*** ./src/js/bar.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Bar = function () {
    function Bar(template) {
        _classCallCheck(this, Bar);

        this.elements = {};
        this.elements.volume = template.volumeBar;
        this.elements.played = template.playedBar;
        this.elements.loaded = template.loadedBar;
        this.elements.danmaku = template.danmakuOpacityBar;
    }

    /**
     * Update progress
     *
     * @param {String} type - Point out which bar it is
     * @param {Number} percentage
     * @param {String} direction - Point out the direction of this bar, Should be height or width
     */


    _createClass(Bar, [{
        key: 'set',
        value: function set(type, percentage, direction) {
            percentage = Math.max(percentage, 0);
            percentage = Math.min(percentage, 1);
            this.elements[type].style[direction] = percentage * 100 + '%';
        }
    }, {
        key: 'get',
        value: function get(type) {
            return parseFloat(this.elements[type].style.width) / 100;
        }
    }]);

    return Bar;
}();

exports.default = Bar;

/***/ }),

/***/ "./src/js/bezel.js":
/*!*************************!*\
  !*** ./src/js/bezel.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Bezel = function () {
    function Bezel(container) {
        var _this = this;

        _classCallCheck(this, Bezel);

        this.container = container;

        this.container.addEventListener('animationend', function () {
            _this.container.classList.remove('dplayer-bezel-transition');
        });
    }

    _createClass(Bezel, [{
        key: 'switch',
        value: function _switch(icon) {
            this.container.innerHTML = icon;
            this.container.classList.add('dplayer-bezel-transition');
        }
    }]);

    return Bezel;
}();

exports.default = Bezel;

/***/ }),

/***/ "./src/js/comment.js":
/*!***************************!*\
  !*** ./src/js/comment.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = __webpack_require__(/*! ./utils */ "./src/js/utils.js");

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Comment = function () {
    function Comment(player) {
        var _this = this;

        _classCallCheck(this, Comment);

        this.player = player;

        this.player.template.mask.addEventListener('click', function () {
            _this.hide();
        });
        this.player.template.commentButton.addEventListener('click', function () {
            _this.show();
        });
        this.player.template.commentSettingButton.addEventListener('click', function () {
            _this.toggleSetting();
        });

        this.player.template.commentColorSettingBox.addEventListener('click', function () {
            var sele = _this.player.template.commentColorSettingBox.querySelector('input:checked+span');
            if (sele) {
                var color = _this.player.template.commentColorSettingBox.querySelector('input:checked').value;
                _this.player.template.commentSettingFill.style.fill = color;
                _this.player.template.commentInput.style.color = color;
                _this.player.template.commentSendFill.style.fill = color;
            }
        });

        this.player.template.commentInput.addEventListener('click', function () {
            _this.hideSetting();
        });
        this.player.template.commentInput.addEventListener('keydown', function (e) {
            var event = e || window.event;
            if (event.keyCode === 13) {
                _this.send();
            }
        });

        this.player.template.commentSendButton.addEventListener('click', function () {
            _this.send();
        });
    }

    _createClass(Comment, [{
        key: 'show',
        value: function show() {
            this.player.controller.disableAutoHide = true;
            this.player.template.controller.classList.add('dplayer-controller-comment');
            this.player.template.mask.classList.add('dplayer-mask-show');
            this.player.container.classList.add('dplayer-show-controller');
            this.player.template.commentInput.focus();
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.player.template.controller.classList.remove('dplayer-controller-comment');
            this.player.template.mask.classList.remove('dplayer-mask-show');
            this.player.container.classList.remove('dplayer-show-controller');
            this.player.controller.disableAutoHide = false;
            this.hideSetting();
        }
    }, {
        key: 'showSetting',
        value: function showSetting() {
            this.player.template.commentSettingBox.classList.add('dplayer-comment-setting-open');
        }
    }, {
        key: 'hideSetting',
        value: function hideSetting() {
            this.player.template.commentSettingBox.classList.remove('dplayer-comment-setting-open');
        }
    }, {
        key: 'toggleSetting',
        value: function toggleSetting() {
            if (this.player.template.commentSettingBox.classList.contains('dplayer-comment-setting-open')) {
                this.hideSetting();
            } else {
                this.showSetting();
            }
        }
    }, {
        key: 'send',
        value: function send() {
            var _this2 = this;

            this.player.template.commentInput.blur();

            // text can't be empty
            if (!this.player.template.commentInput.value.replace(/^\s+|\s+$/g, '')) {
                this.player.notice(this.player.tran('Please input danmaku content!'));
                return;
            }

            this.player.danmaku.send({
                text: this.player.template.commentInput.value,
                color: _utils2.default.color2Number(this.player.container.querySelector('.dplayer-comment-setting-color input:checked').value),
                type: parseInt(this.player.container.querySelector('.dplayer-comment-setting-type input:checked').value)
            }, function () {
                _this2.player.template.commentInput.value = '';
                _this2.hide();
            });
        }
    }]);

    return Comment;
}();

exports.default = Comment;

/***/ }),

/***/ "./src/js/contextmenu.js":
/*!*******************************!*\
  !*** ./src/js/contextmenu.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ContextMenu = function () {
    function ContextMenu(player) {
        var _this = this;

        _classCallCheck(this, ContextMenu);

        this.player = player;
        this.shown = false;

        Array.prototype.slice.call(this.player.template.menuItem).forEach(function (item, index) {
            if (_this.player.options.contextmenu[index].click) {
                item.addEventListener('click', function () {
                    _this.player.options.contextmenu[index].click(_this.player);
                    _this.hide();
                });
            }
        });

        this.player.container.addEventListener('contextmenu', function (e) {
            if (_this.shown) {
                _this.hide();
                return;
            }

            var event = e || window.event;
            event.preventDefault();

            var clientRect = _this.player.container.getBoundingClientRect();
            _this.show(event.clientX - clientRect.left, event.clientY - clientRect.top);

            _this.player.template.mask.addEventListener('click', function () {
                _this.hide();
            });
        });
    }

    _createClass(ContextMenu, [{
        key: 'show',
        value: function show(x, y) {
            this.player.template.menu.classList.add('dplayer-menu-show');

            var clientRect = this.player.container.getBoundingClientRect();
            if (x + this.player.template.menu.offsetWidth >= clientRect.width) {
                this.player.template.menu.style.right = clientRect.width - x + 'px';
                this.player.template.menu.style.left = 'initial';
            } else {
                this.player.template.menu.style.left = x + 'px';
                this.player.template.menu.style.right = 'initial';
            }
            if (y + this.player.template.menu.offsetHeight >= clientRect.height) {
                this.player.template.menu.style.bottom = clientRect.height - y + 'px';
                this.player.template.menu.style.top = 'initial';
            } else {
                this.player.template.menu.style.top = y + 'px';
                this.player.template.menu.style.bottom = 'initial';
            }

            this.player.template.mask.classList.add('dplayer-mask-show');

            this.shown = true;
            this.player.events.trigger('contextmenu_show');
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.player.template.mask.classList.remove('dplayer-mask-show');
            this.player.template.menu.classList.remove('dplayer-menu-show');

            this.shown = false;
            this.player.events.trigger('contextmenu_hide');
        }
    }]);

    return ContextMenu;
}();

exports.default = ContextMenu;

/***/ }),

/***/ "./src/js/controller.js":
/*!******************************!*\
  !*** ./src/js/controller.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = __webpack_require__(/*! ./utils */ "./src/js/utils.js");

var _utils2 = _interopRequireDefault(_utils);

var _thumbnails = __webpack_require__(/*! ./thumbnails */ "./src/js/thumbnails.js");

var _thumbnails2 = _interopRequireDefault(_thumbnails);

var _icons = __webpack_require__(/*! ./icons */ "./src/js/icons.js");

var _icons2 = _interopRequireDefault(_icons);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Controller = function () {
    function Controller(player) {
        var _this = this;

        _classCallCheck(this, Controller);

        this.player = player;

        this.autoHideTimer = 0;
        if (!_utils2.default.isMobile) {
            this.player.container.addEventListener('mousemove', function () {
                _this.setAutoHide();
            });
            this.player.container.addEventListener('click', function () {
                _this.setAutoHide();
            });
            this.player.on('play', function () {
                _this.setAutoHide();
            });
            this.player.on('pause', function () {
                _this.setAutoHide();
            });
        }

        this.initPlayButton();
        this.initThumbnails();
        this.initPlayedBar();
        this.initFullButton();
        this.initQualityButton();
        this.initScreenshotButton();
        this.initSubtitleButton();
        this.initHighlights();
        if (!_utils2.default.isMobile) {
            this.initVolumeButton();
        }
    }

    _createClass(Controller, [{
        key: 'initPlayButton',
        value: function initPlayButton() {
            var _this2 = this;

            this.player.template.playButton.addEventListener('click', function () {
                _this2.player.toggle();
            });

            if (!_utils2.default.isMobile) {
                this.player.template.videoWrap.addEventListener('click', function () {
                    _this2.player.toggle();
                });
                this.player.template.controllerMask.addEventListener('click', function () {
                    _this2.player.toggle();
                });
            } else {
                this.player.template.videoWrap.addEventListener('click', function () {
                    _this2.toggle();
                });
                this.player.template.controllerMask.addEventListener('click', function () {
                    _this2.toggle();
                });
            }
        }
    }, {
        key: 'initHighlights',
        value: function initHighlights() {
            var _this3 = this;

            this.player.on('durationchange', function () {
                if (_this3.player.video.duration !== 1 && _this3.player.video.duration !== Infinity) {
                    if (_this3.player.options.highlight) {
                        var highlights = document.querySelectorAll('.dplayer-highlight');
                        [].slice.call(highlights, 0).forEach(function (item) {
                            _this3.player.template.playedBarWrap.removeChild(item);
                        });
                        for (var i = 0; i < _this3.player.options.highlight.length; i++) {
                            if (!_this3.player.options.highlight[i].text || !_this3.player.options.highlight[i].time) {
                                continue;
                            }
                            var p = document.createElement('div');
                            p.classList.add('dplayer-highlight');
                            p.style.left = _this3.player.options.highlight[i].time / _this3.player.video.duration * 100 + '%';
                            p.innerHTML = '<span class="dplayer-highlight-text">' + _this3.player.options.highlight[i].text + '</span>';
                            _this3.player.template.playedBarWrap.insertBefore(p, _this3.player.template.playedBarTime);
                        }
                    }
                }
            });
        }
    }, {
        key: 'initThumbnails',
        value: function initThumbnails() {
            var _this4 = this;

            if (this.player.options.video.thumbnails) {
                this.thumbnails = new _thumbnails2.default({
                    container: this.player.template.barPreview,
                    barWidth: this.player.template.barWrap.offsetWidth,
                    url: this.player.options.video.thumbnails,
                    events: this.player.events
                });

                this.player.on('loadedmetadata', function () {
                    _this4.thumbnails.resize(160, _this4.player.video.videoHeight / _this4.player.video.videoWidth * 160);
                });
            }
        }
    }, {
        key: 'initPlayedBar',
        value: function initPlayedBar() {
            var _this5 = this;

            var thumbMove = function thumbMove(e) {
                var percentage = ((e.clientX || e.changedTouches[0].clientX) - _utils2.default.getBoundingClientRectViewLeft(_this5.player.template.playedBarWrap)) / _this5.player.template.playedBarWrap.clientWidth;
                percentage = Math.max(percentage, 0);
                percentage = Math.min(percentage, 1);
                _this5.player.bar.set('played', percentage, 'width');
                _this5.player.template.ptime.innerHTML = _utils2.default.secondToTime(percentage * _this5.player.video.duration);
            };

            var thumbUp = function thumbUp(e) {
                document.removeEventListener(_utils2.default.nameMap.dragEnd, thumbUp);
                document.removeEventListener(_utils2.default.nameMap.dragMove, thumbMove);
                var percentage = ((e.clientX || e.changedTouches[0].clientX) - _utils2.default.getBoundingClientRectViewLeft(_this5.player.template.playedBarWrap)) / _this5.player.template.playedBarWrap.clientWidth;
                percentage = Math.max(percentage, 0);
                percentage = Math.min(percentage, 1);
                _this5.player.bar.set('played', percentage, 'width');
                _this5.player.seek(_this5.player.bar.get('played') * _this5.player.video.duration);
                _this5.player.timer.enable('progress');
            };

            this.player.template.playedBarWrap.addEventListener(_utils2.default.nameMap.dragStart, function () {
                _this5.player.timer.disable('progress');
                document.addEventListener(_utils2.default.nameMap.dragMove, thumbMove);
                document.addEventListener(_utils2.default.nameMap.dragEnd, thumbUp);
            });

            this.player.template.playedBarWrap.addEventListener(_utils2.default.nameMap.dragMove, function (e) {
                if (_this5.player.video.duration) {
                    var px = _utils2.default.cumulativeOffset(_this5.player.template.playedBarWrap).left;
                    var tx = (e.clientX || e.changedTouches[0].clientX) - px;
                    if (tx < 0 || tx > _this5.player.template.playedBarWrap.offsetWidth) {
                        return;
                    }
                    var time = _this5.player.video.duration * (tx / _this5.player.template.playedBarWrap.offsetWidth);
                    if (_utils2.default.isMobile) {
                        _this5.thumbnails && _this5.thumbnails.show();
                    }
                    _this5.thumbnails && _this5.thumbnails.move(tx);
                    _this5.player.template.playedBarTime.style.left = tx - (time >= 3600 ? 25 : 20) + 'px';
                    _this5.player.template.playedBarTime.innerText = _utils2.default.secondToTime(time);
                    _this5.player.template.playedBarTime.classList.remove('hidden');
                }
            });

            this.player.template.playedBarWrap.addEventListener(_utils2.default.nameMap.dragEnd, function () {
                if (_utils2.default.isMobile) {
                    _this5.thumbnails && _this5.thumbnails.hide();
                }
            });

            if (!_utils2.default.isMobile) {
                this.player.template.playedBarWrap.addEventListener('mouseenter', function () {
                    if (_this5.player.video.duration) {
                        _this5.thumbnails && _this5.thumbnails.show();
                        _this5.player.template.playedBarTime.classList.remove('hidden');
                    }
                });

                this.player.template.playedBarWrap.addEventListener('mouseleave', function () {
                    if (_this5.player.video.duration) {
                        _this5.thumbnails && _this5.thumbnails.hide();
                        _this5.player.template.playedBarTime.classList.add('hidden');
                    }
                });
            }
        }
    }, {
        key: 'initFullButton',
        value: function initFullButton() {
            var _this6 = this;

            this.player.template.browserFullButton.addEventListener('click', function () {
                _this6.player.fullScreen.toggle('browser');
            });

            this.player.template.webFullButton.addEventListener('click', function () {
                _this6.player.fullScreen.toggle('web');
            });
        }
    }, {
        key: 'initVolumeButton',
        value: function initVolumeButton() {
            var _this7 = this;

            var vWidth = 35;

            var volumeMove = function volumeMove(event) {
                var e = event || window.event;
                var percentage = ((e.clientX || e.changedTouches[0].clientX) - _utils2.default.getBoundingClientRectViewLeft(_this7.player.template.volumeBarWrap) - 5.5) / vWidth;
                _this7.player.volume(percentage);
            };
            var volumeUp = function volumeUp() {
                document.removeEventListener(_utils2.default.nameMap.dragEnd, volumeUp);
                document.removeEventListener(_utils2.default.nameMap.dragMove, volumeMove);
                _this7.player.template.volumeButton.classList.remove('dplayer-volume-active');
            };

            this.player.template.volumeBarWrapWrap.addEventListener('click', function (event) {
                var e = event || window.event;
                var percentage = ((e.clientX || e.changedTouches[0].clientX) - _utils2.default.getBoundingClientRectViewLeft(_this7.player.template.volumeBarWrap) - 5.5) / vWidth;
                _this7.player.volume(percentage);
            });
            this.player.template.volumeBarWrapWrap.addEventListener(_utils2.default.nameMap.dragStart, function () {
                document.addEventListener(_utils2.default.nameMap.dragMove, volumeMove);
                document.addEventListener(_utils2.default.nameMap.dragEnd, volumeUp);
                _this7.player.template.volumeButton.classList.add('dplayer-volume-active');
            });
            this.player.template.volumeButtonIcon.addEventListener('click', function () {
                if (_this7.player.video.muted) {
                    _this7.player.video.muted = false;
                    _this7.player.switchVolumeIcon();
                    _this7.player.bar.set('volume', _this7.player.volume(), 'width');
                } else {
                    _this7.player.video.muted = true;
                    _this7.player.template.volumeIcon.innerHTML = _icons2.default.volumeOff;
                    _this7.player.bar.set('volume', 0, 'width');
                }
            });
        }
    }, {
        key: 'initQualityButton',
        value: function initQualityButton() {
            var _this8 = this;

            if (this.player.options.video.quality) {
                this.player.template.qualityList.addEventListener('click', function (e) {
                    if (e.target.classList.contains('dplayer-quality-item')) {
                        _this8.player.switchQuality(e.target.dataset.index);
                    }
                });
            }
        }
    }, {
        key: 'initScreenshotButton',
        value: function initScreenshotButton() {
            var _this9 = this;

            if (this.player.options.screenshot) {
                this.player.template.camareButton.addEventListener('click', function () {
                    var canvas = document.createElement('canvas');
                    canvas.width = _this9.player.video.videoWidth;
                    canvas.height = _this9.player.video.videoHeight;
                    canvas.getContext('2d').drawImage(_this9.player.video, 0, 0, canvas.width, canvas.height);

                    var dataURL = void 0;
                    canvas.toBlob(function (blob) {
                        dataURL = URL.createObjectURL(blob);
                        var link = document.createElement('a');
                        link.href = dataURL;
                        link.download = 'DPlayer.png';
                        link.style.display = 'none';
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                        URL.revokeObjectURL(dataURL);
                    });

                    _this9.player.events.trigger('screenshot', dataURL);
                });
            }
        }
    }, {
        key: 'initSubtitleButton',
        value: function initSubtitleButton() {
            var _this10 = this;

            if (this.player.options.subtitle) {
                this.player.events.on('subtitle_show', function () {
                    _this10.player.template.subtitleButton.dataset.balloon = _this10.player.tran('Hide subtitle');
                    _this10.player.template.subtitleButtonInner.style.opacity = '';
                    _this10.player.user.set('subtitle', 1);
                });
                this.player.events.on('subtitle_hide', function () {
                    _this10.player.template.subtitleButton.dataset.balloon = _this10.player.tran('Show subtitle');
                    _this10.player.template.subtitleButtonInner.style.opacity = '0.4';
                    _this10.player.user.set('subtitle', 0);
                });

                this.player.template.subtitleButton.addEventListener('click', function () {
                    _this10.player.subtitle.toggle();
                });
            }
        }
    }, {
        key: 'setAutoHide',
        value: function setAutoHide() {
            var _this11 = this;

            this.show();
            clearTimeout(this.autoHideTimer);
            this.autoHideTimer = setTimeout(function () {
                if (_this11.player.video.played.length && !_this11.player.paused && !_this11.disableAutoHide) {
                    _this11.hide();
                }
            }, 3000);
        }
    }, {
        key: 'show',
        value: function show() {
            this.player.container.classList.remove('dplayer-hide-controller');
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.player.container.classList.add('dplayer-hide-controller');
            this.player.setting.hide();
            this.player.comment && this.player.comment.hide();
        }
    }, {
        key: 'isShow',
        value: function isShow() {
            return !this.player.container.classList.contains('dplayer-hide-controller');
        }
    }, {
        key: 'toggle',
        value: function toggle() {
            if (this.isShow()) {
                this.hide();
            } else {
                this.show();
            }
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            clearTimeout(this.autoHideTimer);
        }
    }]);

    return Controller;
}();

exports.default = Controller;

/***/ }),

/***/ "./src/js/danmaku.js":
/*!***************************!*\
  !*** ./src/js/danmaku.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = __webpack_require__(/*! ./utils */ "./src/js/utils.js");

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Danmaku = function () {
    function Danmaku(options) {
        _classCallCheck(this, Danmaku);

        this.options = options;
        this.container = this.options.container;
        this.danTunnel = {
            right: {},
            top: {},
            bottom: {}
        };
        this.danIndex = 0;
        this.dan = [];
        this.showing = true;
        this._opacity = this.options.opacity;
        this.events = this.options.events;
        this.unlimited = this.options.unlimited;
        this._measure('');

        this.load();
    }

    _createClass(Danmaku, [{
        key: 'load',
        value: function load() {
            var _this = this;

            var apiurl = void 0;
            if (this.options.api.maximum) {
                apiurl = this.options.api.address + 'v3/?id=' + this.options.api.id + '&max=' + this.options.api.maximum;
            } else {
                apiurl = this.options.api.address + 'v3/?id=' + this.options.api.id;
            }
            var endpoints = (this.options.api.addition || []).slice(0);
            endpoints.push(apiurl);
            this.events && this.events.trigger('danmaku_load_start', endpoints);

            this._readAllEndpoints(endpoints, function (results) {
                _this.dan = [].concat.apply([], results).sort(function (a, b) {
                    return a.time - b.time;
                });
                window.requestAnimationFrame(function () {
                    _this.frame();
                });

                _this.options.callback();

                _this.events && _this.events.trigger('danmaku_load_end');
            });
        }
    }, {
        key: 'reload',
        value: function reload(newAPI) {
            this.options.api = newAPI;
            this.dan = [];
            this.clear();
            this.load();
        }

        /**
        * Asynchronously read danmaku from all API endpoints
        */

    }, {
        key: '_readAllEndpoints',
        value: function _readAllEndpoints(endpoints, callback) {
            var _this2 = this;

            var results = [];
            var readCount = 0;

            var _loop = function _loop(i) {
                _this2.options.apiBackend.read({
                    url: endpoints[i],
                    success: function success(data) {
                        results[i] = data;

                        ++readCount;
                        if (readCount === endpoints.length) {
                            callback(results);
                        }
                    },
                    error: function error(msg) {
                        _this2.options.error(msg || _this2.options.tran('Danmaku load failed'));
                        results[i] = [];

                        ++readCount;
                        if (readCount === endpoints.length) {
                            callback(results);
                        }
                    }
                });
            };

            for (var i = 0; i < endpoints.length; ++i) {
                _loop(i);
            }
        }
    }, {
        key: 'send',
        value: function send(dan, callback) {
            var _this3 = this;

            var danmakuData = {
                token: this.options.api.token,
                id: this.options.api.id,
                author: this.options.api.user,
                time: this.options.time(),
                text: dan.text,
                color: dan.color,
                type: dan.type
            };
            this.options.apiBackend.send({
                url: this.options.api.address + 'v3/',
                data: danmakuData,
                success: callback,
                error: function error(msg) {
                    _this3.options.error(msg || _this3.options.tran('Danmaku send failed'));
                }
            });

            this.dan.splice(this.danIndex, 0, danmakuData);
            this.danIndex++;
            var danmaku = {
                text: this.htmlEncode(danmakuData.text),
                color: danmakuData.color,
                type: danmakuData.type,
                border: '2px solid ' + this.options.borderColor
            };
            this.draw(danmaku);

            this.events && this.events.trigger('danmaku_send', danmakuData);
        }
    }, {
        key: 'frame',
        value: function frame() {
            var _this4 = this;

            if (this.dan.length && !this.paused && this.showing) {
                var item = this.dan[this.danIndex];
                var dan = [];
                while (item && this.options.time() > parseFloat(item.time)) {
                    dan.push(item);
                    item = this.dan[++this.danIndex];
                }
                this.draw(dan);
            }
            window.requestAnimationFrame(function () {
                _this4.frame();
            });
        }
    }, {
        key: 'opacity',
        value: function opacity(percentage) {
            if (percentage !== undefined) {
                var items = this.container.getElementsByClassName('dplayer-danmaku-item');
                for (var i = 0; i < items.length; i++) {
                    items[i].style.opacity = percentage;
                }
                this._opacity = percentage;

                this.events && this.events.trigger('danmaku_opacity', this._opacity);
            }
            return this._opacity;
        }

        /**
         * Push a danmaku into DPlayer
         *
         * @param {Object Array} dan - {text, color, type}
         * text - danmaku content
         * color - danmaku color, default: `#fff`
         * type - danmaku type, `right` `top` `bottom`, default: `right`
         */

    }, {
        key: 'draw',
        value: function draw(dan) {
            var _this5 = this;

            if (this.showing) {
                var itemHeight = this.options.height;
                var danWidth = this.container.offsetWidth;
                var danHeight = this.container.offsetHeight;
                var itemY = parseInt(danHeight / itemHeight);

                var danItemRight = function danItemRight(ele) {
                    var eleWidth = ele.offsetWidth || parseInt(ele.style.width);
                    var eleRight = ele.getBoundingClientRect().right || _this5.container.getBoundingClientRect().right + eleWidth;
                    return _this5.container.getBoundingClientRect().right - eleRight;
                };

                var danSpeed = function danSpeed(width) {
                    return (danWidth + width) / 5;
                };

                var getTunnel = function getTunnel(ele, type, width) {
                    var tmp = danWidth / danSpeed(width);

                    var _loop2 = function _loop2(i) {
                        var item = _this5.danTunnel[type][i + ''];
                        if (item && item.length) {
                            if (type !== 'right') {
                                return 'continue';
                            }
                            for (var j = 0; j < item.length; j++) {
                                var danRight = danItemRight(item[j]) - 10;
                                if (danRight <= danWidth - tmp * danSpeed(parseInt(item[j].style.width)) || danRight <= 0) {
                                    break;
                                }
                                if (j === item.length - 1) {
                                    _this5.danTunnel[type][i + ''].push(ele);
                                    ele.addEventListener('animationend', function () {
                                        _this5.danTunnel[type][i + ''].splice(0, 1);
                                    });
                                    return {
                                        v: i % itemY
                                    };
                                }
                            }
                        } else {
                            _this5.danTunnel[type][i + ''] = [ele];
                            ele.addEventListener('animationend', function () {
                                _this5.danTunnel[type][i + ''].splice(0, 1);
                            });
                            return {
                                v: i % itemY
                            };
                        }
                    };

                    for (var i = 0; _this5.unlimited || i < itemY; i++) {
                        var _ret2 = _loop2(i);

                        switch (_ret2) {
                            case 'continue':
                                continue;

                            default:
                                if ((typeof _ret2 === 'undefined' ? 'undefined' : _typeof(_ret2)) === "object") return _ret2.v;
                        }
                    }
                    return -1;
                };

                if (Object.prototype.toString.call(dan) !== '[object Array]') {
                    dan = [dan];
                }

                var docFragment = document.createDocumentFragment();

                var _loop3 = function _loop3(i) {
                    dan[i].type = _utils2.default.number2Type(dan[i].type);
                    if (!dan[i].color) {
                        dan[i].color = 16777215;
                    }
                    var item = document.createElement('div');
                    item.classList.add('dplayer-danmaku-item');
                    item.classList.add('dplayer-danmaku-' + dan[i].type);
                    if (dan[i].border) {
                        item.innerHTML = '<span style="border:' + dan[i].border + '">' + dan[i].text + '</span>';
                    } else {
                        item.innerHTML = dan[i].text;
                    }
                    item.style.opacity = _this5._opacity;
                    item.style.color = _utils2.default.number2Color(dan[i].color);
                    item.addEventListener('animationend', function () {
                        _this5.container.removeChild(item);
                    });

                    var itemWidth = _this5._measure(dan[i].text);
                    var tunnel = void 0;

                    // adjust
                    switch (dan[i].type) {
                        case 'right':
                            tunnel = getTunnel(item, dan[i].type, itemWidth);
                            if (tunnel >= 0) {
                                item.style.width = itemWidth + 1 + 'px';
                                item.style.top = itemHeight * tunnel + 'px';
                                item.style.transform = 'translateX(-' + danWidth + 'px)';
                            }
                            break;
                        case 'top':
                            tunnel = getTunnel(item, dan[i].type);
                            if (tunnel >= 0) {
                                item.style.top = itemHeight * tunnel + 'px';
                            }
                            break;
                        case 'bottom':
                            tunnel = getTunnel(item, dan[i].type);
                            if (tunnel >= 0) {
                                item.style.bottom = itemHeight * tunnel + 'px';
                            }
                            break;
                        default:
                            console.error('Can\'t handled danmaku type: ' + dan[i].type);
                    }

                    if (tunnel >= 0) {
                        // move
                        item.classList.add('dplayer-danmaku-move');

                        // insert
                        docFragment.appendChild(item);
                    }
                };

                for (var i = 0; i < dan.length; i++) {
                    _loop3(i);
                }

                this.container.appendChild(docFragment);

                return docFragment;
            }
        }
    }, {
        key: 'play',
        value: function play() {
            this.paused = false;
        }
    }, {
        key: 'pause',
        value: function pause() {
            this.paused = true;
        }
    }, {
        key: '_measure',
        value: function _measure(text) {
            if (!this.context) {
                var measureStyle = getComputedStyle(this.container.getElementsByClassName('dplayer-danmaku-item')[0], null);
                this.context = document.createElement('canvas').getContext('2d');
                this.context.font = measureStyle.getPropertyValue('font');
            }
            return this.context.measureText(text).width;
        }
    }, {
        key: 'seek',
        value: function seek() {
            this.clear();
            for (var i = 0; i < this.dan.length; i++) {
                if (this.dan[i].time >= this.options.time()) {
                    this.danIndex = i;
                    break;
                }
                this.danIndex = this.dan.length;
            }
        }
    }, {
        key: 'clear',
        value: function clear() {
            this.danTunnel = {
                right: {},
                top: {},
                bottom: {}
            };
            this.danIndex = 0;
            this.options.container.innerHTML = '';

            this.events && this.events.trigger('danmaku_clear');
        }
    }, {
        key: 'htmlEncode',
        value: function htmlEncode(str) {
            return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#x27;').replace(/\//g, '&#x2f;');
        }
    }, {
        key: 'resize',
        value: function resize() {
            var danWidth = this.container.offsetWidth;
            var items = this.container.getElementsByClassName('dplayer-danmaku-item');
            for (var i = 0; i < items.length; i++) {
                items[i].style.transform = 'translateX(-' + danWidth + 'px)';
            }
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.showing = false;
            this.pause();
            this.clear();

            this.events && this.events.trigger('danmaku_hide');
        }
    }, {
        key: 'show',
        value: function show() {
            this.seek();
            this.showing = true;
            this.play();

            this.events && this.events.trigger('danmaku_show');
        }
    }, {
        key: 'unlimit',
        value: function unlimit(boolean) {
            this.unlimited = boolean;
        }
    }]);

    return Danmaku;
}();

exports.default = Danmaku;

/***/ }),

/***/ "./src/js/events.js":
/*!**************************!*\
  !*** ./src/js/events.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Events = function () {
    function Events() {
        _classCallCheck(this, Events);

        this.events = {};

        this.videoEvents = ['abort', 'canplay', 'canplaythrough', 'durationchange', 'emptied', 'ended', 'error', 'loadeddata', 'loadedmetadata', 'loadstart', 'mozaudioavailable', 'pause', 'play', 'playing', 'progress', 'ratechange', 'seeked', 'seeking', 'stalled', 'suspend', 'timeupdate', 'volumechange', 'waiting'];
        this.playerEvents = ['screenshot', 'thumbnails_show', 'thumbnails_hide', 'danmaku_show', 'danmaku_hide', 'danmaku_clear', 'danmaku_loaded', 'danmaku_send', 'danmaku_opacity', 'contextmenu_show', 'contextmenu_hide', 'notice_show', 'notice_hide', 'quality_start', 'quality_end', 'destroy', 'resize', 'fullscreen', 'fullscreen_cancel', 'webfullscreen', 'webfullscreen_cancel', 'subtitle_show', 'subtitle_hide', 'subtitle_change'];
    }

    _createClass(Events, [{
        key: 'on',
        value: function on(name, callback) {
            if (this.type(name) && typeof callback === 'function') {
                if (!this.events[name]) {
                    this.events[name] = [];
                }
                this.events[name].push(callback);
            }
        }
    }, {
        key: 'trigger',
        value: function trigger(name, info) {
            if (this.events[name] && this.events[name].length) {
                for (var i = 0; i < this.events[name].length; i++) {
                    this.events[name][i](info);
                }
            }
        }
    }, {
        key: 'type',
        value: function type(name) {
            if (this.playerEvents.indexOf(name) !== -1) {
                return 'player';
            } else if (this.videoEvents.indexOf(name) !== -1) {
                return 'video';
            }

            console.error('Unknown event name: ' + name);
            return null;
        }
    }]);

    return Events;
}();

exports.default = Events;

/***/ }),

/***/ "./src/js/fullscreen.js":
/*!******************************!*\
  !*** ./src/js/fullscreen.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = __webpack_require__(/*! ./utils */ "./src/js/utils.js");

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FullScreen = function () {
    function FullScreen(player) {
        var _this = this;

        _classCallCheck(this, FullScreen);

        this.player = player;

        this.player.events.on('webfullscreen', function () {
            _this.player.resize();
        });
        this.player.events.on('webfullscreen_cancel', function () {
            _this.player.resize();
            _utils2.default.setScrollPosition(_this.lastScrollPosition);
        });

        var fullscreenchange = function fullscreenchange() {
            _this.player.resize();
            if (_this.isFullScreen('browser')) {
                _this.player.events.trigger('fullscreen');
            } else {
                _utils2.default.setScrollPosition(_this.lastScrollPosition);
                _this.player.events.trigger('fullscreen_cancel');
            }
        };
        var docfullscreenchange = function docfullscreenchange() {
            var fullEle = document.fullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
            if (fullEle && fullEle !== _this.player.container) {
                return;
            }
            _this.player.resize();
            if (fullEle) {
                _this.player.events.trigger('fullscreen');
            } else {
                _utils2.default.setScrollPosition(_this.lastScrollPosition);
                _this.player.events.trigger('fullscreen_cancel');
            }
        };
        if (/Firefox/.test(navigator.userAgent)) {
            document.addEventListener('mozfullscreenchange', docfullscreenchange);
            document.addEventListener('fullscreenchange', docfullscreenchange);
        } else {
            this.player.container.addEventListener('fullscreenchange', fullscreenchange);
            this.player.container.addEventListener('webkitfullscreenchange', fullscreenchange);
            document.addEventListener('msfullscreenchange', docfullscreenchange);
            document.addEventListener('MSFullscreenChange', docfullscreenchange);
        }
    }

    _createClass(FullScreen, [{
        key: 'isFullScreen',
        value: function isFullScreen() {
            var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'browser';

            switch (type) {
                case 'browser':
                    return document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement;
                case 'web':
                    return this.player.container.classList.contains('dplayer-fulled');
            }
        }
    }, {
        key: 'request',
        value: function request() {
            var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'browser';

            var anotherType = type === 'browser' ? 'web' : 'browser';
            var anotherTypeOn = this.isFullScreen(anotherType);
            if (!anotherTypeOn) {
                this.lastScrollPosition = _utils2.default.getScrollPosition();
            }

            switch (type) {
                case 'browser':
                    if (this.player.container.requestFullscreen) {
                        this.player.container.requestFullscreen();
                    } else if (this.player.container.mozRequestFullScreen) {
                        this.player.container.mozRequestFullScreen();
                    } else if (this.player.container.webkitRequestFullscreen) {
                        this.player.container.webkitRequestFullscreen();
                    } else if (this.player.video.webkitEnterFullscreen) {
                        // Safari for iOS
                        this.player.video.webkitEnterFullscreen();
                    } else if (this.player.video.webkitEnterFullScreen) {
                        this.player.video.webkitEnterFullScreen();
                    } else if (this.player.container.msRequestFullscreen) {
                        this.player.container.msRequestFullscreen();
                    }
                    break;
                case 'web':
                    this.player.container.classList.add('dplayer-fulled');
                    document.body.classList.add('dplayer-web-fullscreen-fix');
                    this.player.events.trigger('webfullscreen');
                    break;
            }

            if (anotherTypeOn) {
                this.cancel(anotherType);
            }
        }
    }, {
        key: 'cancel',
        value: function cancel() {
            var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'browser';

            switch (type) {
                case 'browser':
                    if (document.cancelFullScreen) {
                        document.cancelFullScreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitCancelFullScreen) {
                        document.webkitCancelFullScreen();
                    } else if (document.webkitCancelFullscreen) {
                        document.webkitCancelFullscreen();
                    } else if (document.msCancelFullScreen) {
                        document.msCancelFullScreen();
                    } else if (document.msExitFullscreen) {
                        document.msExitFullscreen();
                    }
                    break;
                case 'web':
                    this.player.container.classList.remove('dplayer-fulled');
                    document.body.classList.remove('dplayer-web-fullscreen-fix');
                    this.player.events.trigger('webfullscreen_cancel');
                    break;
            }
        }
    }, {
        key: 'toggle',
        value: function toggle() {
            var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'browser';

            if (this.isFullScreen(type)) {
                this.cancel(type);
            } else {
                this.request(type);
            }
        }
    }]);

    return FullScreen;
}();

exports.default = FullScreen;

/***/ }),

/***/ "./src/js/hotkey.js":
/*!**************************!*\
  !*** ./src/js/hotkey.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var HotKey = function HotKey(player) {
    _classCallCheck(this, HotKey);

    if (player.options.hotkey) {
        document.addEventListener('keydown', function (e) {
            if (player.focus) {
                var tag = document.activeElement.tagName.toUpperCase();
                var editable = document.activeElement.getAttribute('contenteditable');
                if (tag !== 'INPUT' && tag !== 'TEXTAREA' && editable !== '' && editable !== 'true') {
                    var event = e || window.event;
                    var percentage = void 0;
                    switch (event.keyCode) {
                        case 32:
                            event.preventDefault();
                            player.toggle();
                            break;
                        case 37:
                            event.preventDefault();
                            player.seek(player.video.currentTime - 5);
                            player.controller.setAutoHide();
                            break;
                        case 39:
                            event.preventDefault();
                            player.seek(player.video.currentTime + 5);
                            player.controller.setAutoHide();
                            break;
                        case 38:
                            event.preventDefault();
                            percentage = player.volume() + 0.1;
                            player.volume(percentage);
                            break;
                        case 40:
                            event.preventDefault();
                            percentage = player.volume() - 0.1;
                            player.volume(percentage);
                            break;
                    }
                }
            }
        });
    }

    document.addEventListener('keydown', function (e) {
        var event = e || window.event;
        switch (event.keyCode) {
            case 27:
                if (player.fullScreen.isFullScreen('web')) {
                    player.fullScreen.cancel('web');
                }
                break;
        }
    });
};

exports.default = HotKey;

/***/ }),

/***/ "./src/js/i18n.js":
/*!************************!*\
  !*** ./src/js/i18n.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/*
W3C def language codes is :
    language-code = primary-code ( "-" subcode )
        primary-code    ISO 639-1   ( the names of language with 2 code )
        subcode         ISO 3166    ( the names of countries )

NOTE: use lowercase to prevent case typo from user!
Use this as shown below..... */

function i18n(lang) {
    var _this = this;

    this.lang = lang;
    this.tran = function (text) {
        if (tranTxt[_this.lang] && tranTxt[_this.lang][text]) {
            return tranTxt[_this.lang][text];
        } else {
            return text;
        }
    };
}

// add translation text here
var tranTxt = {
    'zh-cn': {
        'Danmaku is loading': '弹幕加载中',
        'Top': '顶部',
        'Bottom': '底部',
        'Rolling': '滚动',
        'Input danmaku, hit Enter': '输入弹幕，回车发送',
        'About author': '关于作者',
        'DPlayer feedback': '播放器意见反馈',
        'About DPlayer': '关于 DPlayer 播放器',
        'Loop': '洗脑循环',
        'Speed': '速度',
        'Opacity for danmaku': '弹幕透明度',
        'Normal': '正常',
        'Please input danmaku content!': '要输入弹幕内容啊喂！',
        'Set danmaku color': '设置弹幕颜色',
        'Set danmaku type': '设置弹幕类型',
        'Show danmaku': '显示弹幕',
        'Video load failed': '视频加载失败',
        'Danmaku load failed': '弹幕加载失败',
        'Danmaku send failed': '弹幕发送失败',
        'Switching to': '正在切换至',
        'Switched to': '已经切换至',
        'quality': '画质',
        'FF': '快进',
        'REW': '快退',
        'Unlimited danmaku': '海量弹幕',
        'Send danmaku': '发送弹幕',
        'Setting': '设置',
        'Full screen': '全屏',
        'Web full screen': '页面全屏',
        'Send': '发送',
        'Screenshot': '截图',
        's': '秒',
        'Show subtitle': '显示字幕',
        'Hide subtitle': '隐藏字幕',
        'Volume': '音量',
        'Live': '直播',
        'Video info': '视频统计信息'
    },
    'zh-tw': {
        'Danmaku is loading': '彈幕載入中',
        'Top': '頂部',
        'Bottom': '底部',
        'Rolling': '滾動',
        'Input danmaku, hit Enter': '輸入彈幕，Enter 發送',
        'About author': '關於作者',
        'DPlayer feedback': '播放器意見回饋',
        'About DPlayer': '關於 DPlayer 播放器',
        'Loop': '循環播放',
        'Speed': '速度',
        'Opacity for danmaku': '彈幕透明度',
        'Normal': '正常',
        'Please input danmaku content!': '請輸入彈幕內容啊！',
        'Set danmaku color': '設定彈幕顏色',
        'Set danmaku type': '設定彈幕類型',
        'Show danmaku': '顯示彈幕',
        'Video load failed': '影片載入失敗',
        'Danmaku load failed': '彈幕載入失敗',
        'Danmaku send failed': '彈幕發送失敗',
        'Switching to': '正在切換至',
        'Switched to': '已經切換至',
        'quality': '畫質',
        'FF': '快進',
        'REW': '快退',
        'Unlimited danmaku': '巨量彈幕',
        'Send danmaku': '發送彈幕',
        'Setting': '設定',
        'Full screen': '全螢幕',
        'Web full screen': '頁面全螢幕',
        'Send': '發送',
        'Screenshot': '截圖',
        's': '秒',
        'Show subtitle': '顯示字幕',
        'Hide subtitle': '隱藏字幕',
        'Volume': '音量',
        'Live': '直播',
        'Video info': '影片統計訊息'
    }
};

exports.default = i18n;

/***/ }),

/***/ "./src/js/icons.js":
/*!*************************!*\
  !*** ./src/js/icons.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _play = __webpack_require__(/*! ../assets/play.svg */ "./src/assets/play.svg");

var _play2 = _interopRequireDefault(_play);

var _pause = __webpack_require__(/*! ../assets/pause.svg */ "./src/assets/pause.svg");

var _pause2 = _interopRequireDefault(_pause);

var _volumeUp = __webpack_require__(/*! ../assets/volume-up.svg */ "./src/assets/volume-up.svg");

var _volumeUp2 = _interopRequireDefault(_volumeUp);

var _volumeDown = __webpack_require__(/*! ../assets/volume-down.svg */ "./src/assets/volume-down.svg");

var _volumeDown2 = _interopRequireDefault(_volumeDown);

var _volumeOff = __webpack_require__(/*! ../assets/volume-off.svg */ "./src/assets/volume-off.svg");

var _volumeOff2 = _interopRequireDefault(_volumeOff);

var _full = __webpack_require__(/*! ../assets/full.svg */ "./src/assets/full.svg");

var _full2 = _interopRequireDefault(_full);

var _fullWeb = __webpack_require__(/*! ../assets/full-web.svg */ "./src/assets/full-web.svg");

var _fullWeb2 = _interopRequireDefault(_fullWeb);

var _setting = __webpack_require__(/*! ../assets/setting.svg */ "./src/assets/setting.svg");

var _setting2 = _interopRequireDefault(_setting);

var _right = __webpack_require__(/*! ../assets/right.svg */ "./src/assets/right.svg");

var _right2 = _interopRequireDefault(_right);

var _comment = __webpack_require__(/*! ../assets/comment.svg */ "./src/assets/comment.svg");

var _comment2 = _interopRequireDefault(_comment);

var _commentOff = __webpack_require__(/*! ../assets/comment-off.svg */ "./src/assets/comment-off.svg");

var _commentOff2 = _interopRequireDefault(_commentOff);

var _send = __webpack_require__(/*! ../assets/send.svg */ "./src/assets/send.svg");

var _send2 = _interopRequireDefault(_send);

var _pallette = __webpack_require__(/*! ../assets/pallette.svg */ "./src/assets/pallette.svg");

var _pallette2 = _interopRequireDefault(_pallette);

var _camera = __webpack_require__(/*! ../assets/camera.svg */ "./src/assets/camera.svg");

var _camera2 = _interopRequireDefault(_camera);

var _subtitle = __webpack_require__(/*! ../assets/subtitle.svg */ "./src/assets/subtitle.svg");

var _subtitle2 = _interopRequireDefault(_subtitle);

var _loading = __webpack_require__(/*! ../assets/loading.svg */ "./src/assets/loading.svg");

var _loading2 = _interopRequireDefault(_loading);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Icons = {
    play: _play2.default,
    pause: _pause2.default,
    volumeUp: _volumeUp2.default,
    volumeDown: _volumeDown2.default,
    volumeOff: _volumeOff2.default,
    full: _full2.default,
    fullWeb: _fullWeb2.default,
    setting: _setting2.default,
    right: _right2.default,
    comment: _comment2.default,
    commentOff: _commentOff2.default,
    send: _send2.default,
    pallette: _pallette2.default,
    camera: _camera2.default,
    subtitle: _subtitle2.default,
    loading: _loading2.default
};

exports.default = Icons;

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

__webpack_require__(/*! ../css/index.scss */ "./src/css/index.scss");

var _player = __webpack_require__(/*! ./player */ "./src/js/player.js");

var _player2 = _interopRequireDefault(_player);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* global DPLAYER_VERSION GIT_HASH */
console.log('\n' + ' %c DPlayer v' + "1.25.0" + ' ' + "e50b933" + ' %c http://dplayer.js.org ' + '\n' + '\n', 'color: #fadfa3; background: #030307; padding:5px 0;', 'background: #fadfa3; padding:5px 0;');

exports.default = _player2.default;

/***/ }),

/***/ "./src/js/info-panel.js":
/*!******************************!*\
  !*** ./src/js/info-panel.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* global DPLAYER_VERSION GIT_HASH */

var InfoPanel = function () {
    function InfoPanel(player) {
        var _this = this;

        _classCallCheck(this, InfoPanel);

        this.container = player.template.infoPanel;
        this.template = player.template;
        this.video = player.video;
        this.player = player;

        this.template.infoPanelClose.addEventListener('click', function () {
            _this.hide();
        });
    }

    _createClass(InfoPanel, [{
        key: 'show',
        value: function show() {
            this.beginTime = Date.now();
            this.update();
            this.player.timer.enable('info');
            this.player.timer.enable('fps');
            this.container.classList.remove('dplayer-info-panel-hide');
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.player.timer.disable('info');
            this.player.timer.disable('fps');
            this.container.classList.add('dplayer-info-panel-hide');
        }
    }, {
        key: 'triggle',
        value: function triggle() {
            if (this.container.classList.contains('dplayer-info-panel-hide')) {
                this.show();
            } else {
                this.hide();
            }
        }
    }, {
        key: 'update',
        value: function update() {
            this.template.infoVersion.innerHTML = 'v' + "1.25.0" + ' ' + "e50b933";
            this.template.infoType.innerHTML = this.player.type;
            this.template.infoUrl.innerHTML = this.player.options.video.url;
            this.template.infoResolution.innerHTML = this.player.video.videoWidth + ' x ' + this.player.video.videoHeight;
            this.template.infoDuration.innerHTML = this.player.video.duration;
            if (this.player.options.danmaku) {
                this.template.infoDanmakuId.innerHTML = this.player.options.danmaku.id;
                this.template.infoDanmakuApi.innerHTML = this.player.options.danmaku.api;
                this.template.infoDanmakuAmount.innerHTML = this.player.danmaku.dan.length;
            }
        }
    }, {
        key: 'fps',
        value: function fps(value) {
            this.template.infoFPS.innerHTML = '' + value.toFixed(1);
        }
    }]);

    return InfoPanel;
}();

exports.default = InfoPanel;

/***/ }),

/***/ "./src/js/options.js":
/*!***************************!*\
  !*** ./src/js/options.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; /* global DPLAYER_VERSION */


var _api = __webpack_require__(/*! ./api.js */ "./src/js/api.js");

var _api2 = _interopRequireDefault(_api);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (options) {

    // default options
    var defaultOption = {
        container: options.element || document.getElementsByClassName('dplayer')[0],
        live: false,
        autoplay: false,
        theme: '#b7daff',
        loop: false,
        lang: (navigator.language || navigator.browserLanguage).toLowerCase(),
        screenshot: false,
        hotkey: true,
        preload: 'metadata',
        volume: 0.7,
        apiBackend: _api2.default,
        video: {},
        contextmenu: [],
        mutex: true
    };
    for (var defaultKey in defaultOption) {
        if (defaultOption.hasOwnProperty(defaultKey) && !options.hasOwnProperty(defaultKey)) {
            options[defaultKey] = defaultOption[defaultKey];
        }
    }
    if (options.video) {
        !options.video.type && (options.video.type = 'auto');
    }
    if (_typeof(options.danmaku) === 'object' && options.danmaku) {
        !options.danmaku.user && (options.danmaku.user = 'DIYgod');
    }
    if (options.subtitle) {
        !options.subtitle.type && (options.subtitle.type = 'webvtt');
        !options.subtitle.fontSize && (options.subtitle.fontSize = '20px');
        !options.subtitle.bottom && (options.subtitle.bottom = '40px');
        !options.subtitle.color && (options.subtitle.color = '#fff');
    }

    if (options.video.quality) {
        options.video.url = options.video.quality[options.video.defaultQuality].url;
    }

    if (options.lang) {
        options.lang = options.lang.toLowerCase();
    }

    options.contextmenu = options.contextmenu.concat([{
        text: 'DPlayer v' + "1.25.0",
        link: 'https://github.com/MoePlayer/DPlayer'
    }]);

    return options;
};

/***/ }),

/***/ "./src/js/player.js":
/*!**************************!*\
  !*** ./src/js/player.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _promisePolyfill = __webpack_require__(/*! promise-polyfill */ "./node_modules/promise-polyfill/src/index.js");

var _promisePolyfill2 = _interopRequireDefault(_promisePolyfill);

var _utils = __webpack_require__(/*! ./utils */ "./src/js/utils.js");

var _utils2 = _interopRequireDefault(_utils);

var _options = __webpack_require__(/*! ./options */ "./src/js/options.js");

var _options2 = _interopRequireDefault(_options);

var _i18n = __webpack_require__(/*! ./i18n */ "./src/js/i18n.js");

var _i18n2 = _interopRequireDefault(_i18n);

var _template = __webpack_require__(/*! ./template */ "./src/js/template.js");

var _template2 = _interopRequireDefault(_template);

var _icons = __webpack_require__(/*! ./icons */ "./src/js/icons.js");

var _icons2 = _interopRequireDefault(_icons);

var _danmaku = __webpack_require__(/*! ./danmaku */ "./src/js/danmaku.js");

var _danmaku2 = _interopRequireDefault(_danmaku);

var _events = __webpack_require__(/*! ./events */ "./src/js/events.js");

var _events2 = _interopRequireDefault(_events);

var _fullscreen = __webpack_require__(/*! ./fullscreen */ "./src/js/fullscreen.js");

var _fullscreen2 = _interopRequireDefault(_fullscreen);

var _user = __webpack_require__(/*! ./user */ "./src/js/user.js");

var _user2 = _interopRequireDefault(_user);

var _subtitle = __webpack_require__(/*! ./subtitle */ "./src/js/subtitle.js");

var _subtitle2 = _interopRequireDefault(_subtitle);

var _bar = __webpack_require__(/*! ./bar */ "./src/js/bar.js");

var _bar2 = _interopRequireDefault(_bar);

var _timer = __webpack_require__(/*! ./timer */ "./src/js/timer.js");

var _timer2 = _interopRequireDefault(_timer);

var _bezel = __webpack_require__(/*! ./bezel */ "./src/js/bezel.js");

var _bezel2 = _interopRequireDefault(_bezel);

var _controller = __webpack_require__(/*! ./controller */ "./src/js/controller.js");

var _controller2 = _interopRequireDefault(_controller);

var _setting = __webpack_require__(/*! ./setting */ "./src/js/setting.js");

var _setting2 = _interopRequireDefault(_setting);

var _comment = __webpack_require__(/*! ./comment */ "./src/js/comment.js");

var _comment2 = _interopRequireDefault(_comment);

var _hotkey = __webpack_require__(/*! ./hotkey */ "./src/js/hotkey.js");

var _hotkey2 = _interopRequireDefault(_hotkey);

var _contextmenu = __webpack_require__(/*! ./contextmenu */ "./src/js/contextmenu.js");

var _contextmenu2 = _interopRequireDefault(_contextmenu);

var _infoPanel = __webpack_require__(/*! ./info-panel */ "./src/js/info-panel.js");

var _infoPanel2 = _interopRequireDefault(_infoPanel);

var _video = __webpack_require__(/*! ../template/video.art */ "./src/template/video.art");

var _video2 = _interopRequireDefault(_video);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var index = 0;
var instances = [];

var DPlayer = function () {

    /**
     * DPlayer constructor function
     *
     * @param {Object} options - See README
     * @constructor
     */
    function DPlayer(options) {
        var _this = this;

        _classCallCheck(this, DPlayer);

        this.options = (0, _options2.default)(options);

        if (this.options.video.quality) {
            this.qualityIndex = this.options.video.defaultQuality;
            this.quality = this.options.video.quality[this.options.video.defaultQuality];
        }
        this.tran = new _i18n2.default(this.options.lang).tran;
        this.events = new _events2.default();
        this.user = new _user2.default(this);
        this.container = this.options.container;

        this.container.classList.add('dplayer');
        if (!this.options.danmaku) {
            this.container.classList.add('dplayer-no-danmaku');
        }
        if (this.options.live) {
            this.container.classList.add('dplayer-live');
        }
        if (_utils2.default.isMobile) {
            this.container.classList.add('dplayer-mobile');
        }
        this.arrow = this.container.offsetWidth <= 500;
        if (this.arrow) {
            this.container.classList.add('dplayer-arrow');
        }

        this.template = new _template2.default({
            container: this.container,
            options: this.options,
            index: index,
            tran: this.tran
        });

        this.video = this.template.video;

        this.bar = new _bar2.default(this.template);

        this.bezel = new _bezel2.default(this.template.bezel);

        this.fullScreen = new _fullscreen2.default(this);

        this.controller = new _controller2.default(this);

        if (this.options.danmaku) {
            this.danmaku = new _danmaku2.default({
                container: this.template.danmaku,
                opacity: this.user.get('opacity'),
                callback: function callback() {
                    setTimeout(function () {
                        _this.template.danmakuLoading.style.display = 'none';

                        // autoplay
                        if (_this.options.autoplay) {
                            _this.play();
                        }
                    }, 0);
                },
                error: function error(msg) {
                    _this.notice(msg);
                },
                apiBackend: this.options.apiBackend,
                borderColor: this.options.theme,
                height: this.arrow ? 24 : 30,
                time: function time() {
                    return _this.video.currentTime;
                },
                unlimited: this.user.get('unlimited'),
                api: {
                    id: this.options.danmaku.id,
                    address: this.options.danmaku.api,
                    token: this.options.danmaku.token,
                    maximum: this.options.danmaku.maximum,
                    addition: this.options.danmaku.addition,
                    user: this.options.danmaku.user
                },
                events: this.events,
                tran: function tran(msg) {
                    return _this.tran(msg);
                }
            });

            this.comment = new _comment2.default(this);
        }

        this.setting = new _setting2.default(this);

        document.addEventListener('click', function () {
            _this.focus = false;
        }, true);
        this.container.addEventListener('click', function () {
            _this.focus = true;
        }, true);

        this.paused = true;

        this.timer = new _timer2.default(this);

        this.hotkey = new _hotkey2.default(this);

        this.contextmenu = new _contextmenu2.default(this);

        this.initVideo(this.video, this.quality && this.quality.type || this.options.video.type);

        this.infoPanel = new _infoPanel2.default(this);

        if (!this.danmaku && this.options.autoplay) {
            this.play();
        }

        index++;
        instances.push(this);
    }

    /**
    * Seek video
    */


    _createClass(DPlayer, [{
        key: 'seek',
        value: function seek(time) {
            time = Math.max(time, 0);
            if (this.video.duration) {
                time = Math.min(time, this.video.duration);
            }
            if (this.video.currentTime < time) {
                this.notice(this.tran('FF') + ' ' + (time - this.video.currentTime).toFixed(0) + ' ' + this.tran('s'));
            } else if (this.video.currentTime > time) {
                this.notice(this.tran('REW') + ' ' + (this.video.currentTime - time).toFixed(0) + ' ' + this.tran('s'));
            }

            this.video.currentTime = time;

            if (this.danmaku) {
                this.danmaku.seek();
            }

            this.bar.set('played', time / this.video.duration, 'width');
            this.template.ptime.innerHTML = _utils2.default.secondToTime(time);
        }

        /**
         * Play video
         */

    }, {
        key: 'play',
        value: function play() {
            var _this2 = this;

            this.paused = false;
            if (this.video.paused) {
                this.bezel.switch(_icons2.default.play);
            }

            this.template.playButton.innerHTML = _icons2.default.pause;

            var playedPromise = _promisePolyfill2.default.resolve(this.video.play());
            playedPromise.catch(function () {
                _this2.pause();
            }).then(function () {});
            this.timer.enable('loading');
            this.container.classList.remove('dplayer-paused');
            this.container.classList.add('dplayer-playing');
            if (this.danmaku) {
                this.danmaku.play();
            }
            if (this.options.mutex) {
                for (var i = 0; i < instances.length; i++) {
                    if (this !== instances[i]) {
                        instances[i].pause();
                    }
                }
            }
        }

        /**
         * Pause video
         */

    }, {
        key: 'pause',
        value: function pause() {
            this.paused = true;
            this.container.classList.remove('dplayer-loading');

            if (!this.video.paused) {
                this.bezel.switch(_icons2.default.pause);
            }

            this.template.playButton.innerHTML = _icons2.default.play;
            this.video.pause();
            this.timer.disable('loading');
            this.container.classList.remove('dplayer-playing');
            this.container.classList.add('dplayer-paused');
            if (this.danmaku) {
                this.danmaku.pause();
            }
        }
    }, {
        key: 'switchVolumeIcon',
        value: function switchVolumeIcon() {
            if (this.volume() >= 0.95) {
                this.template.volumeIcon.innerHTML = _icons2.default.volumeUp;
            } else if (this.volume() > 0) {
                this.template.volumeIcon.innerHTML = _icons2.default.volumeDown;
            } else {
                this.template.volumeIcon.innerHTML = _icons2.default.volumeOff;
            }
        }

        /**
         * Set volume
         */

    }, {
        key: 'volume',
        value: function volume(percentage, nostorage, nonotice) {
            percentage = parseFloat(percentage);
            if (!isNaN(percentage)) {
                percentage = Math.max(percentage, 0);
                percentage = Math.min(percentage, 1);
                this.bar.set('volume', percentage, 'width');
                var formatPercentage = (percentage * 100).toFixed(0) + '%';
                this.template.volumeBarWrapWrap.dataset.balloon = formatPercentage;
                if (!nostorage) {
                    this.user.set('volume', percentage);
                }
                if (!nonotice) {
                    this.notice(this.tran('Volume') + ' ' + (percentage * 100).toFixed(0) + '%');
                }

                this.video.volume = percentage;
                if (this.video.muted) {
                    this.video.muted = false;
                }
                this.switchVolumeIcon();
            }

            return this.video.volume;
        }

        /**
         * Toggle between play and pause
         */

    }, {
        key: 'toggle',
        value: function toggle() {
            if (this.video.paused) {
                this.play();
            } else {
                this.pause();
            }
        }

        /**
         * attach event
         */

    }, {
        key: 'on',
        value: function on(name, callback) {
            this.events.on(name, callback);
        }

        /**
         * Switch to a new video
         *
         * @param {Object} video - new video info
         * @param {Object} danmaku - new danmaku info
         */

    }, {
        key: 'switchVideo',
        value: function switchVideo(video, danmakuAPI) {
            this.pause();
            if (video.quality) {
                this.options.video.quality = video.quality;
                var quality = video.quality[this.qualityIndex];
                this.video.poster = video.pic ? video.pic : '';
                this.video.src = quality.url;
                this.initMSE(this.video, quality.type || 'auto');
            } else {
                this.video.poster = video.pic ? video.pic : '';
                this.video.src = video.url;
                this.initMSE(this.video, video.type || 'auto');
            }

            this.initMSE(this.video, video.type || 'auto');
            if (danmakuAPI) {
                this.template.danmakuLoading.style.display = 'block';
                this.bar.set('played', 0, 'width');
                this.bar.set('loaded', 0, 'width');
                this.template.ptime.innerHTML = '00:00';
                this.template.danmaku.innerHTML = '';
                if (this.danmaku) {
                    this.danmaku.reload({
                        id: danmakuAPI.id,
                        address: danmakuAPI.api,
                        token: danmakuAPI.token,
                        maximum: danmakuAPI.maximum,
                        addition: danmakuAPI.addition,
                        user: danmakuAPI.user
                    });
                }
            }
        }
    }, {
        key: 'initMSE',
        value: function initMSE(video, type) {
            var _this3 = this;

            this.type = type;
            if (this.options.video.customType && this.options.video.customType[type]) {
                if (Object.prototype.toString.call(this.options.video.customType[type]) === '[object Function]') {
                    this.options.video.customType[type](this.video, this);
                } else {
                    console.error('Illegal customType: ' + type);
                }
            } else {
                if (this.type === 'auto') {
                    if (/m3u8(#|\?|$)/i.exec(video.src)) {
                        this.type = 'hls';
                    } else if (/.flv(#|\?|$)/i.exec(video.src)) {
                        this.type = 'flv';
                    } else if (/.mpd(#|\?|$)/i.exec(video.src)) {
                        this.type = 'dash';
                    } else {
                        this.type = 'normal';
                    }
                }

                if (this.type === 'hls' && (video.canPlayType('application/x-mpegURL') || video.canPlayType('application/vnd.apple.mpegURL'))) {
                    this.type = 'normal';
                }

                switch (this.type) {
                    // https://github.com/video-dev/hls.js
                    case 'hls':
                        if (Hls) {
                            if (Hls.isSupported()) {
                                if (!window.DPlayerHlsObj) {
                                    window.DPlayerHlsObj = new Hls();
                                }
                                window.DPlayerHlsObj.loadSource(video.src);
                                window.DPlayerHlsObj.attachMedia(video);
                            } else {
                                this.notice('Error: Hls is not supported.');
                            }
                        } else {
                            this.notice('Error: Can\'t find Hls.');
                        }
                        break;

                    // https://github.com/Bilibili/flv.js
                    case 'flv':
                        if (flvjs && flvjs.isSupported()) {
                            if (flvjs.isSupported()) {
                                var flvPlayer = flvjs.createPlayer({
                                    type: 'flv',
                                    url: video.src
                                });
                                flvPlayer.attachMediaElement(video);
                                flvPlayer.load();
                            } else {
                                this.notice('Error: flvjs is not supported.');
                            }
                        } else {
                            this.notice('Error: Can\'t find flvjs.');
                        }
                        break;

                    // https://github.com/Dash-Industry-Forum/dash.js
                    case 'dash':
                        if (dashjs) {
                            dashjs.MediaPlayer().create().initialize(video, video.src, false);
                        } else {
                            this.notice('Error: Can\'t find dashjs.');
                        }
                        break;

                    // https://github.com/webtorrent/webtorrent
                    case 'webtorrent':
                        if (WebTorrent) {
                            if (WebTorrent.WEBRTC_SUPPORT) {
                                this.container.classList.add('dplayer-loading');
                                var client = new WebTorrent();
                                var torrentId = video.src;
                                client.add(torrentId, function (torrent) {
                                    var file = torrent.files.find(function (file) {
                                        return file.name.endsWith('.mp4');
                                    });
                                    file.renderTo(_this3.video, {
                                        autoplay: _this3.options.autoplay
                                    }, function () {
                                        _this3.container.classList.remove('dplayer-loading');
                                    });
                                });
                            } else {
                                this.notice('Error: Webtorrent is not supported.');
                            }
                        } else {
                            this.notice('Error: Can\'t find Webtorrent.');
                        }
                        break;
                }
            }
        }
    }, {
        key: 'initVideo',
        value: function initVideo(video, type) {
            var _this4 = this;

            this.initMSE(video, type);

            /**
             * video events
             */
            // show video time: the metadata has loaded or changed
            this.on('durationchange', function () {
                // compatibility: Android browsers will output 1 or Infinity at first
                if (video.duration !== 1 && video.duration !== Infinity) {
                    _this4.template.dtime.innerHTML = _utils2.default.secondToTime(video.duration);
                }
            });

            // show video loaded bar: to inform interested parties of progress downloading the media
            this.on('progress', function () {
                var percentage = video.buffered.length ? video.buffered.end(video.buffered.length - 1) / video.duration : 0;
                _this4.bar.set('loaded', percentage, 'width');
            });

            // video download error: an error occurs
            this.on('error', function () {
                if (!_this4.video.error) {
                    // Not a video load error, may be poster load failed, see #307
                    return;
                }
                _this4.tran && _this4.notice && _this4.type !== 'webtorrent' & _this4.notice(_this4.tran('Video load failed'), -1);
            });

            // video end
            this.on('ended', function () {
                _this4.bar.set('played', 1, 'width');
                if (!_this4.setting.loop) {
                    _this4.pause();
                } else {
                    _this4.seek(0);
                    _this4.play();
                }
                if (_this4.danmaku) {
                    _this4.danmaku.danIndex = 0;
                }
            });

            this.on('play', function () {
                if (_this4.paused) {
                    _this4.play();
                }
            });

            this.on('pause', function () {
                if (!_this4.paused) {
                    _this4.pause();
                }
            });

            this.on('timeupdate', function () {
                _this4.bar.set('played', _this4.video.currentTime / _this4.video.duration, 'width');
                var currentTime = _utils2.default.secondToTime(_this4.video.currentTime);
                if (_this4.template.ptime.innerHTML !== currentTime) {
                    _this4.template.ptime.innerHTML = currentTime;
                }
            });

            var _loop = function _loop(i) {
                video.addEventListener(_this4.events.videoEvents[i], function () {
                    _this4.events.trigger(_this4.events.videoEvents[i]);
                });
            };

            for (var i = 0; i < this.events.videoEvents.length; i++) {
                _loop(i);
            }

            this.volume(this.user.get('volume'), true, true);

            if (this.options.subtitle) {
                this.subtitle = new _subtitle2.default(this.template.subtitle, this.video, this.options.subtitle, this.events);
                if (!this.user.get('subtitle')) {
                    this.subtitle.hide();
                }
            }
        }
    }, {
        key: 'switchQuality',
        value: function switchQuality(index) {
            var _this5 = this;

            if (this.qualityIndex === index || this.switchingQuality) {
                return;
            } else {
                this.qualityIndex = index;
            }
            this.switchingQuality = true;
            this.quality = this.options.video.quality[index];
            this.template.qualityButton.innerHTML = this.quality.name;

            var paused = this.video.paused;
            this.video.pause();
            var videoHTML = (0, _video2.default)({
                current: false,
                pic: null,
                screenshot: this.options.screenshot,
                preload: 'auto',
                url: this.quality.url,
                subtitle: this.options.subtitle
            });
            var videoEle = new DOMParser().parseFromString(videoHTML, 'text/html').body.firstChild;
            this.template.videoWrap.insertBefore(videoEle, this.template.videoWrap.getElementsByTagName('div')[0]);
            this.prevVideo = this.video;
            this.video = videoEle;
            this.initVideo(this.video, this.quality.type || this.options.video.type);
            this.seek(this.prevVideo.currentTime);
            this.notice(this.tran('Switching to') + ' ' + this.quality.name + ' ' + this.tran('quality'), -1);
            this.events.trigger('quality_start', this.quality);

            this.on('canplay', function () {
                if (_this5.prevVideo) {
                    if (_this5.video.currentTime !== _this5.prevVideo.currentTime) {
                        _this5.seek(_this5.prevVideo.currentTime);
                        return;
                    }
                    _this5.template.videoWrap.removeChild(_this5.prevVideo);
                    _this5.video.classList.add('dplayer-video-current');
                    if (!paused) {
                        _this5.video.play();
                    }
                    _this5.prevVideo = null;
                    _this5.notice(_this5.tran('Switched to') + ' ' + _this5.quality.name + ' ' + _this5.tran('quality'));
                    _this5.switchingQuality = false;

                    _this5.events.trigger('quality_end');
                }
            });
        }
    }, {
        key: 'notice',
        value: function notice(text) {
            var _this6 = this;

            var time = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2000;
            var opacity = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0.8;

            this.template.notice.innerHTML = text;
            this.template.notice.style.opacity = opacity;
            if (this.noticeTime) {
                clearTimeout(this.noticeTime);
            }
            this.events.trigger('notice_show', text);
            if (time > 0) {
                this.noticeTime = setTimeout(function () {
                    _this6.template.notice.style.opacity = 0;
                    _this6.events.trigger('notice_hide');
                }, time);
            }
        }
    }, {
        key: 'resize',
        value: function resize() {
            if (this.danmaku) {
                this.danmaku.resize();
            }
            this.events.trigger('resize');
        }
    }, {
        key: 'speed',
        value: function speed(rate) {
            this.video.playbackRate = rate;
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            instances.splice(instances.indexOf(this), 1);
            this.pause();
            this.controller.destroy();
            this.timer.destroy();
            this.video.src = '';
            this.container.innerHTML = '';
            this.events.trigger('destroy');
        }
    }], [{
        key: 'version',
        get: function get() {
            /* global DPLAYER_VERSION */
            return "1.25.0";
        }
    }]);

    return DPlayer;
}();

exports.default = DPlayer;

/***/ }),

/***/ "./src/js/setting.js":
/*!***************************!*\
  !*** ./src/js/setting.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = __webpack_require__(/*! ./utils */ "./src/js/utils.js");

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Setting = function () {
    function Setting(player) {
        var _this = this;

        _classCallCheck(this, Setting);

        this.player = player;

        this.player.template.mask.addEventListener('click', function () {
            _this.hide();
        });
        this.player.template.settingButton.addEventListener('click', function () {
            _this.show();
        });

        // loop
        this.loop = this.player.options.loop;
        this.player.template.loopToggle.checked = this.loop;
        this.player.template.loop.addEventListener('click', function () {
            _this.player.template.loopToggle.checked = !_this.player.template.loopToggle.checked;
            if (_this.player.template.loopToggle.checked) {
                _this.loop = true;
            } else {
                _this.loop = false;
            }
            _this.hide();
        });

        // show danmaku
        this.showDanmaku = this.player.user.get('danmaku');
        if (!this.showDanmaku) {
            this.player.danmaku && this.player.danmaku.hide();
        }
        this.player.template.showDanmakuToggle.checked = this.showDanmaku;
        this.player.template.showDanmaku.addEventListener('click', function () {
            _this.player.template.showDanmakuToggle.checked = !_this.player.template.showDanmakuToggle.checked;
            if (_this.player.template.showDanmakuToggle.checked) {
                _this.showDanmaku = true;
                _this.player.danmaku.show();
            } else {
                _this.showDanmaku = false;
                _this.player.danmaku.hide();
            }
            _this.player.user.set('danmaku', _this.showDanmaku ? 1 : 0);
            _this.hide();
        });

        // unlimit danmaku
        this.unlimitDanmaku = this.player.user.get('unlimited');
        this.player.template.unlimitDanmakuToggle.checked = this.unlimitDanmaku;
        this.player.template.unlimitDanmaku.addEventListener('click', function () {
            _this.player.template.unlimitDanmakuToggle.checked = !_this.player.template.unlimitDanmakuToggle.checked;
            if (_this.player.template.unlimitDanmakuToggle.checked) {
                _this.unlimitDanmaku = true;
                _this.player.danmaku.unlimit(true);
            } else {
                _this.unlimitDanmaku = false;
                _this.player.danmaku.unlimit(false);
            }
            _this.player.user.set('unlimited', _this.unlimitDanmaku ? 1 : 0);
            _this.hide();
        });

        // speed
        this.player.template.speed.addEventListener('click', function () {
            _this.player.template.settingBox.classList.add('dplayer-setting-box-narrow');
            _this.player.template.settingBox.classList.add('dplayer-setting-box-speed');
        });

        var _loop = function _loop(i) {
            _this.player.template.speedItem[i].addEventListener('click', function () {
                _this.player.speed(_this.player.template.speedItem[i].dataset.speed);
                _this.hide();
            });
        };

        for (var i = 0; i < this.player.template.speedItem.length; i++) {
            _loop(i);
        }

        // danmaku opacity
        if (this.player.danmaku) {
            var dWidth = 130;
            this.player.on('danmaku_opacity', function (percentage) {
                _this.player.bar.set('danmaku', percentage, 'width');
                _this.player.user.set('opacity', percentage);
            });
            this.player.danmaku.opacity(this.player.user.get('opacity'));

            var danmakuMove = function danmakuMove(event) {
                var e = event || window.event;
                var percentage = ((e.clientX || e.changedTouches[0].clientX) - _utils2.default.getBoundingClientRectViewLeft(_this.player.template.danmakuOpacityBarWrap)) / dWidth;
                percentage = Math.max(percentage, 0);
                percentage = Math.min(percentage, 1);
                _this.player.danmaku.opacity(percentage);
            };
            var danmakuUp = function danmakuUp() {
                document.removeEventListener(_utils2.default.nameMap.dragEnd, danmakuUp);
                document.removeEventListener(_utils2.default.nameMap.dragMove, danmakuMove);
                _this.player.template.danmakuOpacityBox.classList.remove('dplayer-setting-danmaku-active');
            };

            this.player.template.danmakuOpacityBarWrapWrap.addEventListener('click', function (event) {
                var e = event || window.event;
                var percentage = ((e.clientX || e.changedTouches[0].clientX) - _utils2.default.getBoundingClientRectViewLeft(_this.player.template.danmakuOpacityBarWrap)) / dWidth;
                percentage = Math.max(percentage, 0);
                percentage = Math.min(percentage, 1);
                _this.player.danmaku.opacity(percentage);
            });
            this.player.template.danmakuOpacityBarWrapWrap.addEventListener(_utils2.default.nameMap.dragStart, function () {
                document.addEventListener(_utils2.default.nameMap.dragMove, danmakuMove);
                document.addEventListener(_utils2.default.nameMap.dragEnd, danmakuUp);
                _this.player.template.danmakuOpacityBox.classList.add('dplayer-setting-danmaku-active');
            });
        }
    }

    _createClass(Setting, [{
        key: 'hide',
        value: function hide() {
            var _this2 = this;

            this.player.template.settingBox.classList.remove('dplayer-setting-box-open');
            this.player.template.mask.classList.remove('dplayer-mask-show');
            setTimeout(function () {
                _this2.player.template.settingBox.classList.remove('dplayer-setting-box-narrow');
                _this2.player.template.settingBox.classList.remove('dplayer-setting-box-speed');
            }, 300);

            this.player.controller.disableAutoHide = false;
        }
    }, {
        key: 'show',
        value: function show() {
            this.player.template.settingBox.classList.add('dplayer-setting-box-open');
            this.player.template.mask.classList.add('dplayer-mask-show');

            this.player.controller.disableAutoHide = true;
        }
    }]);

    return Setting;
}();

exports.default = Setting;

/***/ }),

/***/ "./src/js/subtitle.js":
/*!****************************!*\
  !*** ./src/js/subtitle.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Subtitle = function () {
    function Subtitle(container, video, options, events) {
        _classCallCheck(this, Subtitle);

        this.container = container;
        this.video = video;
        this.options = options;
        this.events = events;

        this.init();
    }

    _createClass(Subtitle, [{
        key: 'init',
        value: function init() {
            var _this = this;

            this.container.style.fontSize = this.options.fontSize;
            this.container.style.bottom = this.options.bottom;
            this.container.style.color = this.options.color;

            if (this.video.textTracks && this.video.textTracks[0]) {
                var track = this.video.textTracks[0];

                track.oncuechange = function () {
                    var cue = track.activeCues[0];
                    if (cue) {
                        _this.container.innerHTML = '';
                        var p = document.createElement('p');
                        p.appendChild(cue.getCueAsHTML());
                        _this.container.appendChild(p);
                    } else {
                        _this.container.innerHTML = '';
                    }
                    _this.events.trigger('subtitle_change');
                };
            }
        }
    }, {
        key: 'show',
        value: function show() {
            this.container.classList.remove('dplayer-subtitle-hide');
            this.events.trigger('subtitle_show');
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.container.classList.add('dplayer-subtitle-hide');
            this.events.trigger('subtitle_hide');
        }
    }, {
        key: 'toggle',
        value: function toggle() {
            if (this.container.classList.contains('dplayer-subtitle-hide')) {
                this.show();
            } else {
                this.hide();
            }
        }
    }]);

    return Subtitle;
}();

exports.default = Subtitle;

/***/ }),

/***/ "./src/js/template.js":
/*!****************************!*\
  !*** ./src/js/template.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _icons = __webpack_require__(/*! ./icons */ "./src/js/icons.js");

var _icons2 = _interopRequireDefault(_icons);

var _player = __webpack_require__(/*! ../template/player.art */ "./src/template/player.art");

var _player2 = _interopRequireDefault(_player);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Template = function () {
    function Template(options) {
        _classCallCheck(this, Template);

        this.container = options.container;
        this.options = options.options;
        this.index = options.index;
        this.tran = options.tran;
        this.init();
    }

    _createClass(Template, [{
        key: 'init',
        value: function init() {
            this.container.innerHTML = (0, _player2.default)({
                options: this.options,
                index: this.index,
                tran: this.tran,
                icons: _icons2.default,
                video: {
                    current: true,
                    pic: this.options.video.pic,
                    screenshot: this.options.screenshot,
                    preload: this.options.preload,
                    url: this.options.video.url,
                    subtitle: this.options.subtitle
                }
            });

            this.volumeBar = this.container.querySelector('.dplayer-volume-bar-inner');
            this.volumeBarWrap = this.container.querySelector('.dplayer-volume-bar');
            this.volumeBarWrapWrap = this.container.querySelector('.dplayer-volume-bar-wrap');
            this.volumeButton = this.container.querySelector('.dplayer-volume');
            this.volumeButtonIcon = this.container.querySelector('.dplayer-volume-icon');
            this.volumeIcon = this.container.querySelector('.dplayer-volume-icon .dplayer-icon-content');
            this.playedBar = this.container.querySelector('.dplayer-played');
            this.loadedBar = this.container.querySelector('.dplayer-loaded');
            this.playedBarWrap = this.container.querySelector('.dplayer-bar-wrap');
            this.playedBarTime = this.container.querySelector('.dplayer-bar-time');
            this.danmaku = this.container.querySelector('.dplayer-danmaku');
            this.danmakuLoading = this.container.querySelector('.dplayer-danloading');
            this.video = this.container.querySelector('.dplayer-video-current');
            this.bezel = this.container.querySelector('.dplayer-bezel-icon');
            this.playButton = this.container.querySelector('.dplayer-play-icon');
            this.videoWrap = this.container.querySelector('.dplayer-video-wrap');
            this.controllerMask = this.container.querySelector('.dplayer-controller-mask');
            this.ptime = this.container.querySelector('.dplayer-ptime');
            this.settingButton = this.container.querySelector('.dplayer-setting-icon');
            this.settingBox = this.container.querySelector('.dplayer-setting-box');
            this.mask = this.container.querySelector('.dplayer-mask');
            this.loop = this.container.querySelector('.dplayer-setting-loop');
            this.loopToggle = this.container.querySelector('.dplayer-setting-loop .dplayer-toggle-setting-input');
            this.showDanmaku = this.container.querySelector('.dplayer-setting-showdan');
            this.showDanmakuToggle = this.container.querySelector('.dplayer-showdan-setting-input');
            this.unlimitDanmaku = this.container.querySelector('.dplayer-setting-danunlimit');
            this.unlimitDanmakuToggle = this.container.querySelector('.dplayer-danunlimit-setting-input');
            this.speed = this.container.querySelector('.dplayer-setting-speed');
            this.speedItem = this.container.querySelectorAll('.dplayer-setting-speed-item');
            this.danmakuOpacityBar = this.container.querySelector('.dplayer-danmaku-bar-inner');
            this.danmakuOpacityBarWrap = this.container.querySelector('.dplayer-danmaku-bar');
            this.danmakuOpacityBarWrapWrap = this.container.querySelector('.dplayer-danmaku-bar-wrap');
            this.danmakuOpacityBox = this.container.querySelector('.dplayer-setting-danmaku');
            this.dtime = this.container.querySelector('.dplayer-dtime');
            this.controller = this.container.querySelector('.dplayer-controller');
            this.commentInput = this.container.querySelector('.dplayer-comment-input');
            this.commentButton = this.container.querySelector('.dplayer-comment-icon');
            this.commentSettingBox = this.container.querySelector('.dplayer-comment-setting-box');
            this.commentSettingButton = this.container.querySelector('.dplayer-comment-setting-icon');
            this.commentSettingFill = this.container.querySelector('.dplayer-comment-setting-icon path');
            this.commentSendButton = this.container.querySelector('.dplayer-send-icon');
            this.commentSendFill = this.container.querySelector('.dplayer-send-icon path');
            this.commentColorSettingBox = this.container.querySelector('.dplayer-comment-setting-color');
            this.browserFullButton = this.container.querySelector('.dplayer-full-icon');
            this.webFullButton = this.container.querySelector('.dplayer-full-in-icon');
            this.menu = this.container.querySelector('.dplayer-menu');
            this.menuItem = this.container.querySelectorAll('.dplayer-menu-item');
            this.qualityList = this.container.querySelector('.dplayer-quality-list');
            this.camareButton = this.container.querySelector('.dplayer-camera-icon');
            this.subtitleButton = this.container.querySelector('.dplayer-subtitle-icon');
            this.subtitleButtonInner = this.container.querySelector('.dplayer-subtitle-icon .dplayer-icon-content');
            this.subtitle = this.container.querySelector('.dplayer-subtitle');
            this.qualityButton = this.container.querySelector('.dplayer-quality-icon');
            this.barPreview = this.container.querySelector('.dplayer-bar-preview');
            this.barWrap = this.container.querySelector('.dplayer-bar-wrap');
            this.notice = this.container.querySelector('.dplayer-notice');
            this.infoPanel = this.container.querySelector('.dplayer-info-panel');
            this.infoPanelClose = this.container.querySelector('.dplayer-info-panel-close');
            this.infoVersion = this.container.querySelector('.dplayer-info-panel-item-version .dplayer-info-panel-item-data');
            this.infoFPS = this.container.querySelector('.dplayer-info-panel-item-fps .dplayer-info-panel-item-data');
            this.infoType = this.container.querySelector('.dplayer-info-panel-item-type .dplayer-info-panel-item-data');
            this.infoUrl = this.container.querySelector('.dplayer-info-panel-item-url .dplayer-info-panel-item-data');
            this.infoResolution = this.container.querySelector('.dplayer-info-panel-item-resolution .dplayer-info-panel-item-data');
            this.infoDuration = this.container.querySelector('.dplayer-info-panel-item-duration .dplayer-info-panel-item-data');
            this.infoDanmakuId = this.container.querySelector('.dplayer-info-panel-item-danmaku-id .dplayer-info-panel-item-data');
            this.infoDanmakuApi = this.container.querySelector('.dplayer-info-panel-item-danmaku-api .dplayer-info-panel-item-data');
            this.infoDanmakuAmount = this.container.querySelector('.dplayer-info-panel-item-danmaku-amount .dplayer-info-panel-item-data');
        }
    }]);

    return Template;
}();

exports.default = Template;

/***/ }),

/***/ "./src/js/thumbnails.js":
/*!******************************!*\
  !*** ./src/js/thumbnails.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Thumbnails = function () {
    function Thumbnails(options) {
        _classCallCheck(this, Thumbnails);

        this.container = options.container;
        this.barWidth = options.barWidth;
        this.container.style.backgroundImage = 'url(\'' + options.url + '\')';
        this.events = options.events;
    }

    _createClass(Thumbnails, [{
        key: 'resize',
        value: function resize(width, height) {
            this.container.style.width = width + 'px';
            this.container.style.height = height + 'px';
            this.container.style.top = -height + 2 + 'px';
        }
    }, {
        key: 'show',
        value: function show() {
            this.container.style.display = 'block';
            this.events && this.events.trigger('thumbnails_show');
        }
    }, {
        key: 'move',
        value: function move(position) {
            this.container.style.backgroundPosition = '-' + (Math.ceil(position / this.barWidth * 100) - 1) * 160 + 'px 0';
            this.container.style.left = Math.min(Math.max(position - this.container.offsetWidth / 2, -10), this.barWidth - 150) + 'px';
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.container.style.display = 'none';

            this.events && this.events.trigger('thumbnails_hide');
        }
    }]);

    return Thumbnails;
}();

exports.default = Thumbnails;

/***/ }),

/***/ "./src/js/timer.js":
/*!*************************!*\
  !*** ./src/js/timer.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Timer = function () {
    function Timer(player) {
        _classCallCheck(this, Timer);

        this.player = player;

        window.requestAnimationFrame = function () {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
        }();

        this.types = ['loading', 'info', 'fps'];

        this.init();
    }

    _createClass(Timer, [{
        key: 'init',
        value: function init() {
            var _this = this;

            this.types.map(function (item) {
                if (item !== 'fps') {
                    _this['init' + item + 'Checker']();
                }
                return item;
            });
        }
    }, {
        key: 'initloadingChecker',
        value: function initloadingChecker() {
            var _this2 = this;

            var lastPlayPos = 0;
            var currentPlayPos = 0;
            var bufferingDetected = false;
            this.loadingChecker = setInterval(function () {
                if (_this2.enableloadingChecker) {
                    // whether the video is buffering
                    currentPlayPos = _this2.player.video.currentTime;
                    if (!bufferingDetected && currentPlayPos === lastPlayPos && !_this2.player.video.paused) {
                        _this2.player.container.classList.add('dplayer-loading');
                        bufferingDetected = true;
                    }
                    if (bufferingDetected && currentPlayPos > lastPlayPos && !_this2.player.video.paused) {
                        _this2.player.container.classList.remove('dplayer-loading');
                        bufferingDetected = false;
                    }
                    lastPlayPos = currentPlayPos;
                }
            }, 100);
        }
    }, {
        key: 'initfpsChecker',
        value: function initfpsChecker() {
            var _this3 = this;

            window.requestAnimationFrame(function () {
                if (_this3.enablefpsChecker) {
                    _this3.initfpsChecker();
                    if (!_this3.fpsStart) {
                        _this3.fpsStart = new Date();
                        _this3.fpsIndex = 0;
                    } else {
                        _this3.fpsIndex++;
                        var fpsCurrent = new Date();
                        if (fpsCurrent - _this3.fpsStart > 1000) {
                            _this3.player.infoPanel.fps(_this3.fpsIndex / (fpsCurrent - _this3.fpsStart) * 1000);
                            _this3.fpsStart = new Date();
                            _this3.fpsIndex = 0;
                        }
                    }
                } else {
                    _this3.fpsStart = 0;
                    _this3.fpsIndex = 0;
                }
            });
        }
    }, {
        key: 'initinfoChecker',
        value: function initinfoChecker() {
            var _this4 = this;

            this.infoChecker = setInterval(function () {
                if (_this4.enableinfoChecker) {
                    _this4.player.infoPanel.update();
                }
            }, 1000);
        }
    }, {
        key: 'enable',
        value: function enable(type) {
            this['enable' + type + 'Checker'] = true;

            if (type === 'fps') {
                this.initfpsChecker();
            }
        }
    }, {
        key: 'disable',
        value: function disable(type) {
            this['enable' + type + 'Checker'] = false;
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            var _this5 = this;

            this.types.map(function (item) {
                _this5['enable' + item + 'Checker'] = false;
                _this5[item + 'Checker'] && clearInterval(_this5[item + 'Checker']);
                return item;
            });
        }
    }]);

    return Timer;
}();

exports.default = Timer;

/***/ }),

/***/ "./src/js/user.js":
/*!************************!*\
  !*** ./src/js/user.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = __webpack_require__(/*! ./utils */ "./src/js/utils.js");

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var User = function () {
    function User(player) {
        _classCallCheck(this, User);

        this.storageName = {
            opacity: 'dplayer-danmaku-opacity',
            volume: 'dplayer-volume',
            unlimited: 'dplayer-danmaku-unlimited',
            danmaku: 'dplayer-danmaku-show',
            subtitle: 'dplayer-subtitle-show'
        };
        this.default = {
            opacity: 0.7,
            volume: player.options.hasOwnProperty('volume') ? player.options.volume : 0.7,
            unlimited: (player.options.danmaku && player.options.danmaku.unlimited ? 1 : 0) || 0,
            danmaku: 1,
            subtitle: 1
        };
        this.data = {};

        this.init();
    }

    _createClass(User, [{
        key: 'init',
        value: function init() {
            for (var item in this.storageName) {
                var name = this.storageName[item];
                this.data[item] = parseFloat(_utils2.default.storage.get(name) || this.default[item]);
            }
        }
    }, {
        key: 'get',
        value: function get(key) {
            return this.data[key];
        }
    }, {
        key: 'set',
        value: function set(key, value) {
            this.data[key] = value;
            _utils2.default.storage.set(this.storageName[key], value);
        }
    }]);

    return User;
}();

exports.default = User;

/***/ }),

/***/ "./src/js/utils.js":
/*!*************************!*\
  !*** ./src/js/utils.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var isMobile = /mobile/i.test(window.navigator.userAgent);

var utils = {

    /**
    * Parse second to time string
    *
    * @param {Number} second
    * @return {String} 00:00 or 00:00:00
    */
    secondToTime: function secondToTime(second) {
        var add0 = function add0(num) {
            return num < 10 ? '0' + num : '' + num;
        };
        var hour = Math.floor(second / 3600);
        var min = Math.floor((second - hour * 3600) / 60);
        var sec = Math.floor(second - hour * 3600 - min * 60);
        return (hour > 0 ? [hour, min, sec] : [min, sec]).map(add0).join(':');
    },

    /**
     * control play progress
     */
    // get element's view position
    getElementViewLeft: function getElementViewLeft(element) {
        var actualLeft = element.offsetLeft;
        var current = element.offsetParent;
        var elementScrollLeft = document.body.scrollLeft + document.documentElement.scrollLeft;
        if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {
            while (current !== null) {
                actualLeft += current.offsetLeft;
                current = current.offsetParent;
            }
        } else {
            while (current !== null && current !== element) {
                actualLeft += current.offsetLeft;
                current = current.offsetParent;
            }
        }
        return actualLeft - elementScrollLeft;
    },

    /**
    * optimize control play progress
     * optimize get element's view position,for float dialog video player
    * getBoundingClientRect 在 IE8 及以下返回的值缺失 width、height 值
    * getBoundingClientRect 在 Firefox 11 及以下返回的值会把 transform 的值也包含进去
    * getBoundingClientRect 在 Opera 10.5 及以下返回的值缺失 width、height 值
    */
    getBoundingClientRectViewLeft: function getBoundingClientRectViewLeft(element) {
        var scrollTop = document.documentElement.scrollTop;

        if (element.getBoundingClientRect) {
            if (typeof this.getBoundingClientRectViewLeft.offset !== 'number') {
                var temp = document.createElement('div');
                temp.style.cssText = 'position:absolute;top:0;left:0;';
                document.body.appendChild(temp);
                this.getBoundingClientRectViewLeft.offset = -temp.getBoundingClientRect().top - scrollTop;
                document.body.removeChild(temp);
                temp = null;
            }
            var rect = element.getBoundingClientRect();
            var offset = this.getBoundingClientRectViewLeft.offset;

            return rect.left + offset;
        } else {
            // not support getBoundingClientRect
            return this.getElementViewLeft(element);
        }
    },
    getScrollPosition: function getScrollPosition() {
        return {
            left: window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0,
            top: window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0
        };
    },
    setScrollPosition: function setScrollPosition(_ref) {
        var _ref$left = _ref.left,
            left = _ref$left === undefined ? 0 : _ref$left,
            _ref$top = _ref.top,
            top = _ref$top === undefined ? 0 : _ref$top;

        if (this.isFirefox) {
            document.documentElement.scrollLeft = left;
            document.documentElement.scrollTop = top;
        } else {
            window.scrollTo(left, top);
        }
    },


    isMobile: isMobile,

    isFirefox: /firefox/i.test(window.navigator.userAgent),

    isChrome: /chrome/i.test(window.navigator.userAgent),

    storage: {
        set: function set(key, value) {
            localStorage.setItem(key, value);
        },

        get: function get(key) {
            return localStorage.getItem(key);
        }
    },

    cumulativeOffset: function cumulativeOffset(element) {
        var top = 0,
            left = 0;
        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);

        return {
            top: top,
            left: left
        };
    },

    nameMap: {
        dragStart: isMobile ? 'touchstart' : 'mousedown',
        dragMove: isMobile ? 'touchmove' : 'mousemove',
        dragEnd: isMobile ? 'touchend' : 'mouseup'
    },

    color2Number: function color2Number(color) {
        if (color[0] === '#') {
            color = color.substr(1);
        }
        if (color.length === 3) {
            color = '' + color[0] + color[0] + color[1] + color[1] + color[2] + color[2];
        }
        return parseInt(color, 16) + 0x000000 & 0xffffff;
    },

    number2Color: function number2Color(number) {
        return '#' + ('00000' + number.toString(16)).slice(-6);
    },

    number2Type: function number2Type(number) {
        switch (number) {
            case 0:
                return 'right';
            case 1:
                return 'top';
            case 2:
                return 'bottom';
            default:
                return 'right';
        }
    }
};

exports.default = utils;

/***/ }),

/***/ "./src/template/player.art":
/*!*********************************!*\
  !*** ./src/template/player.art ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var $imports = __webpack_require__(/*! ../../node_modules/art-template/lib/runtime.js */ "./node_modules/art-template/lib/runtime.js");
module.exports = function ($data) {
    'use strict';
    $data = $data || {};
    var $$out = '', $$blocks = arguments[1] || {}, include = function (content) {
            $$out += content;
            return $$out;
        }, video = $data.video, options = $data.options, $escape = $imports.$escape, tran = $data.tran, icons = $data.icons, index = $data.index, $each = $imports.$each, $value = $data.$value, $index = $data.$index;
    $$out += '<div class="dplayer-mask"></div>\n<div class="dplayer-video-wrap">\n    ';
    include(__webpack_require__(/*! ./video.art */ "./src/template/video.art")(video));
    $$out += '\n    ';
    if (options.logo) {
        $$out += '\n    <div class="dplayer-logo">\n        <img src="';
        $$out += $escape(options.logo);
        $$out += '">\n    </div>\n    ';
    }
    $$out += '\n    <div class="dplayer-danmaku"';
    if (options.danmaku && options.danmaku.bottm) {
        $$out += ' style="margin-bottom:';
        $$out += $escape(options.danmaku.bottm);
        $$out += '"';
    }
    $$out += '>\n        <div class="dplayer-danmaku-item dplayer-danmaku-item--demo"></div>\n    </div>\n    <div class="dplayer-subtitle"></div>\n    <div class="dplayer-bezel">\n        <span class="dplayer-bezel-icon"></span>\n        ';
    if (options.danmaku) {
        $$out += '\n        <span class="dplayer-danloading">';
        $$out += $escape(tran('Danmaku is loading'));
        $$out += '</span>\n        ';
    }
    $$out += '\n        <span class="diplayer-loading-icon">';
    $$out += icons.loading;
    $$out += '</span>\n    </div>\n</div>\n<div class="dplayer-controller-mask"></div>\n<div class="dplayer-controller">\n    <div class="dplayer-icons dplayer-comment-box">\n        <button class="dplayer-icon dplayer-comment-setting-icon" data-balloon="';
    $$out += $escape(tran('Setting'));
    $$out += '" data-balloon-pos="up">\n            <span class="dplayer-icon-content">';
    $$out += icons.pallette;
    $$out += '</span>\n        </button>\n        <div class="dplayer-comment-setting-box">\n            <div class="dplayer-comment-setting-color">\n                <div class="dplayer-comment-setting-title">';
    $$out += $escape(tran('Set danmaku color'));
    $$out += '</div>\n                <label>\n                    <input type="radio" name="dplayer-danmaku-color-';
    $$out += $escape(index);
    $$out += '" value="#fff" checked>\n                    <span style="background: #fff;"></span>\n                </label>\n                <label>\n                    <input type="radio" name="dplayer-danmaku-color-';
    $$out += $escape(index);
    $$out += '" value="#e54256">\n                    <span style="background: #e54256"></span>\n                </label>\n                <label>\n                    <input type="radio" name="dplayer-danmaku-color-';
    $$out += $escape(index);
    $$out += '" value="#ffe133">\n                    <span style="background: #ffe133"></span>\n                </label>\n                <label>\n                    <input type="radio" name="dplayer-danmaku-color-';
    $$out += $escape(index);
    $$out += '" value="#64DD17">\n                    <span style="background: #64DD17"></span>\n                </label>\n                <label>\n                    <input type="radio" name="dplayer-danmaku-color-';
    $$out += $escape(index);
    $$out += '" value="#39ccff">\n                    <span style="background: #39ccff"></span>\n                </label>\n                <label>\n                    <input type="radio" name="dplayer-danmaku-color-';
    $$out += $escape(index);
    $$out += '" value="#D500F9">\n                    <span style="background: #D500F9"></span>\n                </label>\n            </div>\n            <div class="dplayer-comment-setting-type">\n                <div class="dplayer-comment-setting-title">';
    $$out += $escape(tran('Set danmaku type'));
    $$out += '</div>\n                <label>\n                    <input type="radio" name="dplayer-danmaku-type-';
    $$out += $escape(index);
    $$out += '" value="1">\n                    <span>';
    $$out += $escape(tran('Top'));
    $$out += '</span>\n                </label>\n                <label>\n                    <input type="radio" name="dplayer-danmaku-type-';
    $$out += $escape(index);
    $$out += '" value="0" checked>\n                    <span>';
    $$out += $escape(tran('Rolling'));
    $$out += '</span>\n                </label>\n                <label>\n                    <input type="radio" name="dplayer-danmaku-type-';
    $$out += $escape(index);
    $$out += '" value="2">\n                    <span>';
    $$out += $escape(tran('Bottom'));
    $$out += '</span>\n                </label>\n            </div>\n        </div>\n        <input class="dplayer-comment-input" type="text" placeholder="';
    $$out += $escape(tran('Input danmaku, hit Enter'));
    $$out += '" maxlength="30">\n        <button class="dplayer-icon dplayer-send-icon" data-balloon="';
    $$out += $escape(tran('Send'));
    $$out += '" data-balloon-pos="up">\n            <span class="dplayer-icon-content">';
    $$out += icons.send;
    $$out += '</span>\n        </button>\n    </div>\n    <div class="dplayer-icons dplayer-icons-left">\n        <button class="dplayer-icon dplayer-play-icon">\n            <span class="dplayer-icon-content">';
    $$out += icons.play;
    $$out += '</span>\n        </button>\n        <div class="dplayer-volume">\n            <button class="dplayer-icon dplayer-volume-icon">\n                <span class="dplayer-icon-content">';
    $$out += icons.volumeDown;
    $$out += '</span>\n            </button>\n            <div class="dplayer-volume-bar-wrap" data-balloon-pos="up">\n                <div class="dplayer-volume-bar">\n                    <div class="dplayer-volume-bar-inner" style="background: ';
    $$out += $escape(options.theme);
    $$out += ';">\n                        <span class="dplayer-thumb" style="background: ';
    $$out += $escape(options.theme);
    $$out += '"></span>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <span class="dplayer-time">\n            <span class="dplayer-ptime">0:00</span> /\n            <span class="dplayer-dtime">0:00</span>\n        </span>\n        ';
    if (options.live) {
        $$out += '\n        <span class="dplayer-live-badge"><span class="dplayer-live-dot" style="background: ';
        $$out += $escape(options.theme);
        $$out += ';"></span>';
        $$out += $escape(tran('Live'));
        $$out += '</span>\n        ';
    }
    $$out += '\n    </div>\n    <div class="dplayer-icons dplayer-icons-right">\n        ';
    if (options.video.quality) {
        $$out += '\n        <div class="dplayer-quality">\n            <button class="dplayer-icon dplayer-quality-icon">';
        $$out += $escape(options.video.quality[options.video.defaultQuality].name);
        $$out += '</button>\n            <div class="dplayer-quality-mask">\n                <div class="dplayer-quality-list">\n                ';
        $each(options.video.quality, function ($value, $index) {
            $$out += '\n                    <div class="dplayer-quality-item" data-index="';
            $$out += $escape($index);
            $$out += '">';
            $$out += $escape($value.name);
            $$out += '</div>\n                ';
        });
        $$out += '\n                </div>\n            </div>\n        </div>\n        ';
    }
    $$out += '\n        ';
    if (options.screenshot) {
        $$out += '\n        <div class="dplayer-icon dplayer-camera-icon" data-balloon="';
        $$out += $escape(tran('Screenshot'));
        $$out += '" data-balloon-pos="up">\n            <span class="dplayer-icon-content">';
        $$out += icons.camera;
        $$out += '</span>\n        </div>\n        ';
    }
    $$out += '\n        <div class="dplayer-comment">\n            <button class="dplayer-icon dplayer-comment-icon" data-balloon="';
    $$out += $escape(tran('Send danmaku'));
    $$out += '" data-balloon-pos="up">\n                <span class="dplayer-icon-content">';
    $$out += icons.comment;
    $$out += '</span>\n            </button>\n        </div>\n        ';
    if (options.subtitle) {
        $$out += '\n        <div class="dplayer-subtitle-btn">\n            <button class="dplayer-icon dplayer-subtitle-icon" data-balloon="';
        $$out += $escape(tran('Hide subtitle'));
        $$out += '" data-balloon-pos="up">\n                <span class="dplayer-icon-content">';
        $$out += icons.subtitle;
        $$out += '</span>\n            </button>\n        </div>\n        ';
    }
    $$out += '\n        <div class="dplayer-setting">\n            <button class="dplayer-icon dplayer-setting-icon" data-balloon="';
    $$out += $escape(tran('Setting'));
    $$out += '" data-balloon-pos="up">\n                <span class="dplayer-icon-content">';
    $$out += icons.setting;
    $$out += '</span>\n            </button>\n            <div class="dplayer-setting-box">\n                <div class="dplayer-setting-origin-panel">\n                    <div class="dplayer-setting-item dplayer-setting-speed">\n                        <span class="dplayer-label">';
    $$out += $escape(tran('Speed'));
    $$out += '</span>\n                        <div class="dplayer-toggle">';
    $$out += icons.right;
    $$out += '</div>\n                    </div>\n                    <div class="dplayer-setting-item dplayer-setting-loop">\n                        <span class="dplayer-label">';
    $$out += $escape(tran('Loop'));
    $$out += '</span>\n                        <div class="dplayer-toggle">\n                            <input class="dplayer-toggle-setting-input" type="checkbox" name="dplayer-toggle">\n                            <label for="dplayer-toggle"></label>\n                        </div>\n                    </div>\n                    <div class="dplayer-setting-item dplayer-setting-showdan">\n                        <span class="dplayer-label">';
    $$out += $escape(tran('Show danmaku'));
    $$out += '</span>\n                        <div class="dplayer-toggle">\n                            <input class="dplayer-showdan-setting-input" type="checkbox" name="dplayer-toggle-dan">\n                            <label for="dplayer-toggle-dan"></label>\n                        </div>\n                    </div>\n                    <div class="dplayer-setting-item dplayer-setting-danunlimit">\n                        <span class="dplayer-label">';
    $$out += $escape(tran('Unlimited danmaku'));
    $$out += '</span>\n                        <div class="dplayer-toggle">\n                            <input class="dplayer-danunlimit-setting-input" type="checkbox" name="dplayer-toggle-danunlimit">\n                            <label for="dplayer-toggle-danunlimit"></label>\n                        </div>\n                    </div>\n                    <div class="dplayer-setting-item dplayer-setting-danmaku">\n                        <span class="dplayer-label">';
    $$out += $escape(tran('Opacity for danmaku'));
    $$out += '</span>\n                        <div class="dplayer-danmaku-bar-wrap">\n                            <div class="dplayer-danmaku-bar">\n                                <div class="dplayer-danmaku-bar-inner">\n                                    <span class="dplayer-thumb"></span>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class="dplayer-setting-speed-panel">\n                    <div class="dplayer-setting-speed-item" data-speed="0.5">\n                        <span class="dplayer-label">0.5</span>\n                    </div>\n                    <div class="dplayer-setting-speed-item" data-speed="0.75">\n                        <span class="dplayer-label">0.75</span>\n                    </div>\n                    <div class="dplayer-setting-speed-item" data-speed="1">\n                        <span class="dplayer-label">';
    $$out += $escape(tran('Normal'));
    $$out += '</span>\n                    </div>\n                    <div class="dplayer-setting-speed-item" data-speed="1.25">\n                        <span class="dplayer-label">1.25</span>\n                    </div>\n                    <div class="dplayer-setting-speed-item" data-speed="1.5">\n                        <span class="dplayer-label">1.5</span>\n                    </div>\n                    <div class="dplayer-setting-speed-item" data-speed="2">\n                        <span class="dplayer-label">2</span>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class="dplayer-full">\n            <button class="dplayer-icon dplayer-full-icon" data-balloon="';
    $$out += $escape(tran('Full screen'));
    $$out += '" data-balloon-pos="up">\n                <span class="dplayer-icon-content">';
    $$out += icons.full;
    $$out += '</span>\n            </button>\n        </div>\n    </div>\n    <div class="dplayer-bar-wrap">\n        <div class="dplayer-bar-time hidden">00:00</div>\n        <div class="dplayer-bar-preview"></div>\n        <div class="dplayer-bar">\n            <div class="dplayer-loaded" style="width: 0;"></div>\n            <div class="dplayer-played" style="width: 0; background: ';
    $$out += $escape(options.theme);
    $$out += '">\n                <span class="dplayer-thumb" style="background: ';
    $$out += $escape(options.theme);
    $$out += '"></span>\n            </div>\n        </div>\n    </div>\n</div>\n<div class="dplayer-info-panel dplayer-info-panel-hide">\n    <div class="dplayer-info-panel-close">[x]</div>\n    <div class="dplayer-info-panel-item dplayer-info-panel-item-version">\n        <span class="dplayer-info-panel-item-title">Player version</span>\n        <span class="dplayer-info-panel-item-data"></span>\n    </div>\n    <div class="dplayer-info-panel-item dplayer-info-panel-item-fps">\n        <span class="dplayer-info-panel-item-title">Player FPS</span>\n        <span class="dplayer-info-panel-item-data"></span>\n    </div>\n    <div class="dplayer-info-panel-item dplayer-info-panel-item-type">\n        <span class="dplayer-info-panel-item-title">Video type</span>\n        <span class="dplayer-info-panel-item-data"></span>\n    </div>\n    <div class="dplayer-info-panel-item dplayer-info-panel-item-url">\n        <span class="dplayer-info-panel-item-title">Video url</span>\n        <span class="dplayer-info-panel-item-data"></span>\n    </div>\n    <div class="dplayer-info-panel-item dplayer-info-panel-item-resolution">\n        <span class="dplayer-info-panel-item-title">Video resolution</span>\n        <span class="dplayer-info-panel-item-data"></span>\n    </div>\n    <div class="dplayer-info-panel-item dplayer-info-panel-item-duration">\n        <span class="dplayer-info-panel-item-title">Video duration</span>\n        <span class="dplayer-info-panel-item-data"></span>\n    </div>\n    ';
    if (options.danmaku) {
        $$out += '\n    <div class="dplayer-info-panel-item dplayer-info-panel-item-danmaku-id">\n        <span class="dplayer-info-panel-item-title">Danamku id</span>\n        <span class="dplayer-info-panel-item-data"></span>\n    </div>\n    <div class="dplayer-info-panel-item dplayer-info-panel-item-danmaku-api">\n        <span class="dplayer-info-panel-item-title">Danamku api</span>\n        <span class="dplayer-info-panel-item-data"></span>\n    </div>\n    <div class="dplayer-info-panel-item dplayer-info-panel-item-danmaku-amount">\n        <span class="dplayer-info-panel-item-title">Danamku amount</span>\n        <span class="dplayer-info-panel-item-data"></span>\n    </div>\n    ';
    }
    $$out += '\n</div>\n<div class="dplayer-menu">\n    ';
    $each(options.contextmenu, function ($value, $index) {
        $$out += '\n        <div class="dplayer-menu-item">\n            <a';
        if ($value.link) {
            $$out += ' target="_blank"';
        }
        $$out += ' href="';
        $$out += $escape($value.link || 'javascript:void(0);');
        $$out += '">';
        $$out += $escape(tran($value.text));
        $$out += '</a>\n        </div>\n    ';
    });
    $$out += '\n</div>\n<div class="dplayer-notice"></div>';
    return $$out;
};

/***/ }),

/***/ "./src/template/video.art":
/*!********************************!*\
  !*** ./src/template/video.art ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var $imports = __webpack_require__(/*! ../../node_modules/art-template/lib/runtime.js */ "./node_modules/art-template/lib/runtime.js");
module.exports = function ($data) {
    'use strict';
    $data = $data || {};
    var $$out = '', enableSubtitle = $data.enableSubtitle, subtitle = $data.subtitle, current = $data.current, pic = $data.pic, $escape = $imports.$escape, screenshot = $data.screenshot, preload = $data.preload, url = $data.url;
    var enableSubtitle = subtitle && subtitle.type === 'webvtt';
    $$out += '\n<video\n    class="dplayer-video ';
    if (current) {
        $$out += 'dplayer-video-current';
    }
    $$out += '"\n    webkit-playsinline\n    playsinline\n    ';
    if (pic) {
        $$out += 'poster="';
        $$out += $escape(pic);
        $$out += '"';
    }
    $$out += '\n    ';
    if (screenshot || enableSubtitle) {
        $$out += 'crossorigin="anonymous"';
    }
    $$out += '\n    ';
    if (preload) {
        $$out += 'preload="';
        $$out += $escape(preload);
        $$out += '"';
    }
    $$out += '\n    ';
    if (url) {
        $$out += 'src="';
        $$out += $escape(url);
        $$out += '"';
    }
    $$out += '\n    >\n    ';
    if (enableSubtitle) {
        $$out += '\n    <track kind="metadata" default src="';
        $$out += $escape(subtitle.url);
        $$out += '"></track>\n    ';
    }
    $$out += '\n</video>';
    return $$out;
};

/***/ })

/******/ })["default"];
});
//# sourceMappingURL=DPlayer.js.map